//const assert = require('assert');
const { myPronApp, mypron, TypeEnums, emptySyllable } = require("../../src/assets/js/my-pron")
const _myPronApp_private = myPronApp()._private;

function generateSyllable(newObjectValue) {
    return Object.assign({}, emptySyllable, newObjectValue)
}

describe('Simple Math Test', () => {
    it('should return 2', () => {
        expect(1 + 1).to.equal(2);
    });
    specify('should return 9', () => {
        expect(3 * 3).to.equal(9);
    });
});

describe("mypron object test", () => {
    it("mypron output", () => {
        expect(mypron("abc")).to.equal("abc")
    });

    if (process.env.NODE_ENV == "development") {
        describe('private function tests', function () {
            it("halfVoweltest", () => {
                expect(_myPronApp_private.halfVowelCharacter()).to.equal("ă");
                cy.log("TypeEnums.IPA");
                expect(_myPronApp_private.halfVowelCharacter(TypeEnums.IPA)).to.
                    equal("ə");
            });

            describe("syllable segmentation ", function () {
                it("segment တက္ကသိုလ်", () => {
                    expect(_myPronApp_private.segment("တက္ကသိုလ်")).to.deep.
                        equal(["တ", "က္", "က", "သိုလ်"]);
                })
                it("segment လိမ္မော်ရည်", () => {
                    expect(_myPronApp_private.segment("လိမ္မော်ရည်")).to.deep.
                        equal(["လိ", "မ္", "မော်", "ရည်"]);
                })
                it("segment သ္မီး", () => {
                    expect(_myPronApp_private.segment("သ္မီး")).to.deep.
                        equal(["သ္", "မီး"]);
                })

                it("replaceMistakenInputCharacters example: ် + ျ -> ျ + ် =  ျ်", () => {
                    expect(_myPronApp_private.replaceMistakenInputCharacters("ယောက်ျ")).to.
                        equal("ယောကျ်");
                })

                it("segment ယောက်ျ", () => {
                    cy.log("ျ+် different from ျ+်");
                    expect(_myPronApp_private.segment("ယောကျ်")).to.deep.
                        equal(["ယော", "ကျ်"]);
                })

                it("segment သောကြာနေ့", () => {

                    expect(_myPronApp_private.segment("သောကြာနေ့")).to.deep.
                        equal(["သော", "ကြာ", "နေ့"]);
                })
                it("segment ဘတ်စ်ကား", () => {

                    expect(_myPronApp_private.segment("ဘတ်စ်ကား")).to.deep.
                        equal(["ဘတ်", "စ်", "ကား"]);
                })

                it("segment ပိန္နဲသီး", () => {

                    expect(_myPronApp_private.segment("ပိန္နဲသီး")).to.deep.
                        equal(["ပိ", "န္", "နဲ", "သီး"]);
                })
            })


        })
        describe("can merge nextConsonant", () => {
            it('canMergeToNewEndOfSyllable "ပိ", "န်"', () => {
                const syllable = generateSyllable({ consonant: "ပ", vowel: "ိ" })
                //Object.assign({},emptySyllable,{consonant:"ပ",vowel:"ိ"})
                expect(_myPronApp_private.canMergeToNewEndOfSyllable(syllable, "န်")).to.be.true
                expect(_myPronApp_private.canMergeToNewEndOfSyllable(syllable, "န")).to.be.false
                console.log(emptySyllable)
                // syllable = Object.assign(emptySyllable,{})
            })
            it('canMergeToNewEndOfSyllable "တ", "က်"', () => {
                const syllable = generateSyllable({ consonant: "တ" })
                expect(_myPronApp_private.canMergeToNewEndOfSyllable(syllable, "က်")).to.be.true
                expect(_myPronApp_private.canMergeToNewEndOfSyllable(syllable, "က")).to.be.false


            })
            it('canMergeToNewEndOfSyllable "ယော", "က်"', () => {
                const syllable = generateSyllable({ consonant: "ယ", vowel: "ော" })
                expect(_myPronApp_private.canMergeToNewEndOfSyllable(syllable, "က်")).to.be.true
                expect(_myPronApp_private.canMergeToNewEndOfSyllable(syllable, "က")).to.be.false


            })
        });

        describe("array pos can be combined with next syllable ForSimplifiedStacked", () => {
            it("segment ပိန္နဲသီး", () => {
                expect(_myPronApp_private.segment("ပိန္နဲသီး")).to.deep.
                    equal(["ပိ", "န္", "နဲ", "သီး"]);
            })
            it("cannot combine with next syllable", () => {
                const array = _myPronApp_private.segment("ပိန္နဲသီး");
                const syllableObjectArray = _myPronApp_private.createPhonemObjectArray(array);
                expect(_myPronApp_private.canBeCombinedWithNextSyllableForSimplifiedStacked(syllableObjectArray, 0, TypeEnums.BGN)).to.be.false

            })
            it("segment ယောက်ျ", () => {
                const array = _myPronApp_private.segment("ယောကျ်");
                expect(array).to.deep.
                    equal(["ယော", "ကျ်"]);
                console.log(array)
            })
            it("combine with next syllable ယောကျ် -> ယောက်ကျ", () => {
                const array = _myPronApp_private.segment("ယောကျ်");
                const syllableObjectArray = _myPronApp_private.createPhonemObjectArray(array);
                console.log(array)
                console.log(syllableObjectArray)
                expect(_myPronApp_private.canBeCombinedWithNextSyllableForSimplifiedStacked(syllableObjectArray, 0, TypeEnums.BGN)).to.be.true
            })
            it("combine with next syllable သောကြာနေ့ -> သောက်ကြာနေ့", () => {
                const array = _myPronApp_private.segment("သောကြာနေ့");
                const syllableObjectArray = _myPronApp_private.createPhonemObjectArray(array);
                console.log(array)
                console.log(syllableObjectArray)
                expect(_myPronApp_private.canBeCombinedWithNextSyllableForSimplifiedStacked(syllableObjectArray, 0, TypeEnums.BGN)).to.be.true
            })
        })

        describe("insertSpecialStackedCharacter ", () => {
            it("ပိဿာ -> ပိသ်သာ", () => {
                const array = _myPronApp_private.segment("ပိဿာ");
                const syllableObjectArray = _myPronApp_private.createPhonemObjectArray(array);
                const syllableUnchangedObjectArray = _myPronApp_private.createPhonemObjectArray(array);
                const hasInsertedStackedCharacter = _myPronApp_private.insertSpecialStackedCharacter(syllableObjectArray, 0, syllableUnchangedObjectArray, 0)
                expect(hasInsertedStackedCharacter).to.be.true;
            })
        
        })
    }


})
