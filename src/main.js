// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api
import "./assets/css/global.css";
import DefaultLayout from '~/layouts/Default.vue'
import VueDisqus from 'vue-disqus'

export default function (Vue, { router, head, isClient }) {
  // Set default layout as a global component
  Vue.component('Layout', DefaultLayout);
  Vue.use(VueDisqus, {
    shortname: 'myanmartolatin'
  })


  /*
  // Add attributes to BODY tag
  head.bodyAttrs = { style: 'height:100%' }
  */
 /*
   // Add a meta tag
   head.meta.push({
    //name: 'Content-Security-Policy',
    "http-equiv":"Content-Security-Policy",
    content: "default-src 'self' http://localhost:8080; img-src https://* http://localhost:8080 ; child-src 'none'; worker-src 'self' http://localhost:8080"
  })
*/

}