//https://en.wiktionary.org/wiki/Wiktionary:Burmese_transliteration //pcgn 

const CONSONANT_TYPE = {// romanization+a when not followed by vowel character?
    "က": "k",
    "ခ": "hk",
    "ဂ": "g",
    "ဃ": "g",
    "င": "ng",
    "စ": "s",
    "ဆ": "hs",
    "ဇ": "z",	//can also be j? ဝိဇ္ဇာ (wijja)[mlcts][knowledge] //https://en.wikipedia.org/wiki/Burmese_alphabet
    "ဈ": "z",
    "ည": "ny",
    "ဉ": "ny",
    "တ": "t",
    "ထ": "ht",//changed to pcgn
    "ဋ": "t",
    "ဌ": "ht", //changed to pcgn
    "ဒ": "d",
    "ဍ": "d",
    "ဓ": "d",
    "ဎ": "d",
    "န": "n",
    "ဏ": "n",
    "ပ": "p",
    "ဖ": "hp",
    "ဗ": "b",
    "ဘ": "b",
    "မ": "m",
    "ယ": "y",
    "ရ": "|y|",	//r substitute for names not rooted of burmese: example paris, karaweik(pali "karavika")
    "လ": "l",
    "ဠ": "l",
    "ဝ": "w",
    "သ": "th",
    "ဟ": "h",
    "အ": "a",

    //https://en.wikipedia.org/wiki/Burmese_alphabet 
    //https://r12a.github.io/scripts/myanmar/
    //consonant repetition appears within words, whereas သ်သ is used across word boundaries
    "ဿ": "ss", //alternative "ss" or stacked သ္သ tth
};
const CONSONANT_CHARACTER_COMBINATIONS_TYPE = { // "-" instead of C for Consonant position

    "ြ": "-y",	//Ya yit (ရရစ်)
    "ျ": "-y",	//Ya pin (ယပင့်)
    "ွ": "-w",	//Wa hswe (ဝဆွဲ)
    "ြွ": "-yw",
    "ျွ": "-yw",
    "ှ": "h-",	//Ha hto (ဟထိုး) 
    //needs to be done better // same error like with EndOfSyllable လွှတ် (hlut)[to release]
    "ြှ": "h-y",// not BGN correct should be hCy needs to be done in 
    //returnTargetRomanConsonant -consonantCombinationCount
    //vv page64 more consonant clusters vv
    "ျှ": "h-y",
    "ွှ": "h-w",
    //^more consonant clusters ^
};

/* //unusal signs p.216(p.238)
	သြ	= အော aw
	သျှ = ရှ	sha
    */

const CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR_TYPE = {
    //multichar extra check
    "ခြ": "ch",	//not ky
    "ချ": "ch",	//not ky
    "ရှ": "sh",	//not hy
    "ရွှ": "shw",		//for gold ရွှေ //exception better pronounciation
    //"ရှွ":"shw", // for gold ရွှေ //exception better pronounciation //after unicode processing
    //colloqial burmese p.210(p.187) လျှ can be (hly-) and (sh-)
    "လျှ": "|sh|",	//exception? alternative C-yin/hly- possible
    "သျှ": "sh",	//exception for example british ဗြိတိသျှ //colloquial burmese p.240(p.217)
    //C+ျှ -> "sh" ?
    "မြွ":"hmy",
    "ယှ":"sh",
}

//exchange consonant after vowel
const CONSONANT_AFTER_ROMAN_VOWEL_TYPE = {	//change consonant after roman vowel
    "က": "g",
    "ခ": "g",
    "စ": "z",
    "ဆ": "z",
    "တ": "d",
    "ထ": "d",
    "ဋ": "d",
    "ဌ": "d",
    "ပ": "b",
    "ဖ": "b",
}
const CONSONANT_AFTER_ROMAN_VOWEL_MULTICHAR_TYPE = {
    "ခြ": "gy",	//not cha+y (chy)
    "ချ": "gy",	//not cha+y (chy)
}

const VOWEL_CHARACTERS_TYPE = {
    Independent: {
        "ဧ": "e",
        "၏": "e",
        "ဣ": "i",
        "ဤ": "i",
        "ဥ": "u",
        "ဦ": "u",
        "ဩ": "aw",
        "ဩော": "aw",
        "ဪ": "aw",

    },
    Dependent: {
        "ာ": "a",
        "ါ": "a",
        "ေ": "e",
        "ဲ": "è:",
        "ိ": "i",
        "ီ": "i",
        "ို": "o",
        "ု": "u",
        "ူ": "u",
        "ော": "aw:",
        "ော်": "aw",
        "ေါ": "aw",
        "ေါ်": "aw",
        "္": "",
    }
}
const CONSONANT_END_OF_SYLLABLE_TYPE = {
    //vv below vv different from BGN? additional for readability
    "ေါက်": "au",
    "ေါင်": "aun",
    "ိုဒ်": "aik", //example cases ဂိုဒ်, ပိုဒ်
    "ဉ္စ": "yinsa",//exception stacked character: example case ပဉ္စမ (pyin); not real final character

    //unicode "hack"/quickfix needs to be done differently
    //"ွှတ်":"hut", // ွှ needs to be written circle comma rest endofsyllable -> should be h-C-ut not C-hut
    //^^ atop ^^  different from BGN? additional for readability

    "က်": "et",
    "ိုက်": "aik",
    "ောက်": "auk",

    "င်": "in",
    "ိင်": "ein", //missing in BGN? example name [Theingi] သိင်္ဂီ  - သ +  ိင် + ဂ +ီ found in https://att-astrec.nict.go.jp/member/ding/PACLING2017-MY.pdf
    "ိုင်": "aing",
    "ောင်": "aung",

    "စ်": "it",	//numbers sway -> (a) instead of (it) for တစ်(tit)/နှစ်(hnit) + CL
    "ိစ်": "eit", //not in BGN

    //more often e?
    "ည်": "|i|", //!note 10: ည် is romanized i, in or e, depending on pronunciation.  A reference source should be consulted in case of uncertainty. 

    //5 / 7
    "တ်": "at",
    "ိတ်": "eik",
    "ုတ်": "ôk",
    "ွတ်": "ut",
    "ဝတ်": "wut",// maybe "false" endOfSyllable? vocab if same handling like "ဝမ်"; search for example vocab
    "ေတ်": "it",

    "ပ်": "at",
    "ိပ်": "eik",
    "ုပ်": "ôk",
    "ွပ်": "ut",
    "ဝပ်":"wut",

    //6 / 8
    "န်": "an",
    "ိန်": "ein",
    "ုန်": "ôn",
    "ွန်": "un",
    "ဝန်": "wun",// "false" endOfSyllable? C+EndSyllable combination

    "မ်": "an",
    "ိမ်": "ein",
    "ုမ်": "ôn",
    "ွမ်": "un",
    "ဝမ်": "wun",// "false" endOfSyllable? C+EndSyllable combination


    "ယ်": "è",
    "ဥ်": "in",	//ဥ (u)+်
    "ဉ်": "in", //ဉ (nya)+်	//added. one or the other is needed and not in BGN table

    "ဝံ": "wun",
    "ံ": "an",
    "ိံ": "ein",
    "ုံ": "ôn",

    "ွံ": "uɴ",
    "်": "",	//end of syllable final mark	//needed for example baNdaik "ဘဏ်တိုက်"

    /* Colloquial Burmese p.178(p.156)
    The ending -လ် does not affect the pronunciation in any way. For example သို and သိုလ် are both pronounced as (to); ဗို and ဗိုလ် are both pronounced as (bo)
    */
    "လ်": "",
}

const TONE_SENTENCE_MARK_TYPE = {	//original BGN without tone mark transliteration
    // https://en.wiktionary.org/wiki/Wiktionary:Burmese_transliteration
    "း": ":",	//needs other marker because of sentence endpoint maybe ´
    "့": ".",	//needs other marker because of sentence endpoint maybe  
    /*
    ့	- "t" https://att-astrec.nict.go.jp/member/ding/PACLING2017-MY.pdf
    or backtrack search first vowel (case -at/-on/-un etc) or beginning of vowel (aung) to put on "´" "`"
    */
    "၊": ", ",
    "။": ". ",
    "၏": ". ",	//https://en.wikipedia.org/wiki/Burmese_alphabet "used as a full stop if the sentence immediately ends with a verb."
}

const endSyllableKeyArray = Object.keys(CONSONANT_END_OF_SYLLABLE_TYPE);
const toneSentenceMarkArray = Object.keys(TONE_SENTENCE_MARK_TYPE);
const syllableAndTonemarkCriteria = '(' + endSyllableKeyArray.sort((a, b) => b.length - a.length).join("|") + ")([" + toneSentenceMarkArray.join("") + "]*)";//tripple split result [no match, first group(syllable), second group(toneMark)]
const REGEX_ENDOFSYLLABLE_AND_TONEMARKS_TYPE = new RegExp(syllableAndTonemarkCriteria, "g");

const REGEX_STARTSWITH_CONSONANT_TYPE = new RegExp("^(" + Object.keys(CONSONANT_TYPE).join("|") + ")");
const REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATIONS_TYPE = new RegExp("^(" + Object.keys(CONSONANT_CHARACTER_COMBINATIONS_TYPE).sort((a, b) => b.length - a.length).join("|") + ")");
const REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR_TYPE = new RegExp("^(" + Object.keys(CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR_TYPE).sort((a, b) => b.length - a.length).join("|") + ")");
const REGEX_STARTSWITH_VOWELINDEPENDENT_TYPE = new RegExp("^(" + Object.keys(VOWEL_CHARACTERS_TYPE["Independent"]).sort((a, b) => b.length - a.length).join("|") + ")");
const REGEX_STARTSWITH_VOWELDEPENDENT_TYPE = new RegExp("^(" + Object.keys(VOWEL_CHARACTERS_TYPE["Dependent"]).sort((a, b) => b.length - a.length).join("|") + ")");
const REGEX_CONSONANT_END_OF_SYLLABLE_TYPE = new RegExp(Object.keys(CONSONANT_END_OF_SYLLABLE_TYPE).sort((a, b) => b.length - a.length).join("|"));
const REGEX_STARTSWITH_CONSONANT_END_OF_SYLLABLE_TYPE = new RegExp("^(" + Object.keys(CONSONANT_END_OF_SYLLABLE_TYPE).sort((a, b) => b.length - a.length).join("|") + ")");
const REGEX_STARTSWITH_TONE_SENTENCE_MARK_TYPE = new RegExp("^([" + toneSentenceMarkArray.join("") + "]*)");
//const REGEX_TONE_SENTENCE_MARK = new RegExp("([" + toneSentenceMarkArray.join("") + "]*)");
//TODO how to define regex without expanding variant



export const TRANSLITERATEOBJECT_BGNPCGN = {
    CONSONANT: CONSONANT_TYPE,
    CONSONANT_CHARACTER_COMBINATIONS: CONSONANT_CHARACTER_COMBINATIONS_TYPE,
    CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR: CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR_TYPE,
    CONSONANT_AFTER_ROMAN_VOWEL: CONSONANT_AFTER_ROMAN_VOWEL_TYPE,
    CONSONANT_AFTER_ROMAN_VOWEL_MULTICHAR: CONSONANT_AFTER_ROMAN_VOWEL_MULTICHAR_TYPE,
    VOWEL_CHARACTERS: VOWEL_CHARACTERS_TYPE,
    CONSONANT_END_OF_SYLLABLE: CONSONANT_END_OF_SYLLABLE_TYPE,
    TONE_SENTENCE_MARK: TONE_SENTENCE_MARK_TYPE,
    REGEX_ENDOFSYLLABLE_AND_TONEMARKS: REGEX_ENDOFSYLLABLE_AND_TONEMARKS_TYPE,
    REGEX_STARTSWITH_CONSONANT: REGEX_STARTSWITH_CONSONANT_TYPE,
    REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATIONS: REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATIONS_TYPE,
    REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR: REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR_TYPE,
    REGEX_STARTSWITH_VOWELINDEPENDENT: REGEX_STARTSWITH_VOWELINDEPENDENT_TYPE,
    REGEX_STARTSWITH_VOWELDEPENDENT: REGEX_STARTSWITH_VOWELDEPENDENT_TYPE,
    REGEX_CONSONANT_END_OF_SYLLABLE: REGEX_CONSONANT_END_OF_SYLLABLE_TYPE,
    REGEX_STARTSWITH_CONSONANT_END_OF_SYLLABLE: REGEX_STARTSWITH_CONSONANT_END_OF_SYLLABLE_TYPE,
    REGEX_STARTSWITH_TONE_SENTENCE_MARK: REGEX_STARTSWITH_TONE_SENTENCE_MARK_TYPE,
};