/* to improve
https://en.wikipedia.org/wiki/Burmese_phonology ipa description
https://en.wiktionary.org/wiki/Wiktionary:Burmese_transliteration
Minor syllables in Burmese contain only the vowel /ə/ in the rhyme and do not bear tone. They are always followed by a major syllable (any other kind of syllable). In the orthography, they are most commonly spelled as if their rhyme was /a̰/
*/
//ipa association of https://en.wikipedia.org/wiki/Burmese_alphabet#Letters
const CONSONANT_TYPE = {// romanization+a when not followed by vowel character?
    "က": "k",
    "ခ": "kʰ",
    "ဂ": "g",
    "ဃ": "g?",
    "င": "ŋ",
    "စ": "s",
    "ဆ": "sʰ",
    "ဇ": "z",	// j MLCTS ဝိဇ္ဇာ (wijja)[mlcts][knowledge] //https://en.wikipedia.org/wiki/Burmese_alphabet
    "ဈ": "zˀ",
    "ည": "ɲ",
    "ဉ": "ɲ",
    "တ": "t",
    "ထ": "tʰ",
    "ဋ": "t",
    "ဌ": "tʰ",
    "ဒ": "d",
    "ဍ": "d",
    "ဓ": "dˀ",
    "ဎ": "dˀ",
    "န": "n",
    "ဏ": "n",
    "ပ": "p",
    "ဖ": "pʰ",
    "ဗ": "b",
    "ဘ": "bˀ",
    "မ": "m",
    "ယ": "j",
    "ရ": "|j|",	//r substitute for names not rooted of burmese: example paris, karaweik(pali "karavika")
    "လ": "l",
    "ဠ": "l",
    "ဝ": "w",
    "သ": "θ",
    "ဟ": "h",
    "အ": "ʔ",

    //https://en.wikipedia.org/wiki/Burmese_alphabet 
    //https://r12a.github.io/scripts/myanmar/
    //consonant repetition appears within words, whereas သ်သ is used across word boundaries
    "ဿ": "ʔθ",
};
//ျ | ya1 pin1 //ြ | ya1 yit 
const CONSONANT_MODIFICATION_YAPIN_YAYIT_TYPE = {
    "က": "tɕ",
    "ခ": "tɕʰ",
    
    // "င":"n", //ငြ wiktionary error?  copy from BGN https://en.wiktionary.org/wiki/Wiktionary:Burmese_transliteration
}
const CONSONANT_CHARACTER_COMBINATIONS_TYPE = { // "-" instead of C for Consonant position

    "ြ": "-j",	//Ya yit (ရရစ်)
    "ျ": "-j",	//Ya pin (ယပင့်)
    "ွ": "-w",	//Wa hswe (ဝဆွဲ)
    "ြွ": "-jw",
    "ျွ": "-jw",
    "ှ": "-̥", //Ha hto (ဟထိုး)  //https://westonruter.github.io/ipa-chart/keyboard/

    "ြှ": "-̥j",// not BGN correct should be hCy needs to be done in 
    //returnTargetRomanConsonant -consonantCombinationCount
    //vv page64 more consonant clusters vv
    "ျှ": "-̥j",
    "ွှ": "-̥w",
    //^more consonant clusters ^
};

/* //unusal signs p.216(p.238)
	သြ	= အော aw
	သျှ = ရှ	sha
    */

const CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR_TYPE = {
    //multichar extra check
    "ခြ": "tɕʰ",	//not ky
    "ချ": "tɕʰ",
    "ရှ": "ʃ",	// hy is okell, sh is BGN
    "ရွှ": "ʃw",		//for gold ရွှေ //exception better pronounciation
    //"ရှွ":"shw", // for gold ရွှေ //exception better pronounciation //after unicode processing
    //colloqial burmese p.210(p.187) လျှ can be (hly-) and (sh-)
    //"လျှ": "|hly|",	//hly/hy
    "သျှ": "ʃ",	//exception for example british ဗြိတိသျှ //colloquial burmese p.240(p.217)
    //C+ျှ -> "sh" ?
    "ကြ": "tɕ",
    "ကျ": "tɕ",
    "ငြ": "ny",
    "ဂျ": "j",
    "ပြွ": "pw",
    "မြွ": "mw",
    "ဂျ": "dʑ",
    "ငှ": "ŋ̊",
    "ငြ": "ɲ",
    "ငြှ":"ɲ̊",
    "ငွှ":"ŋ̊w",
    "ဉှ":"ɲ̊",
    "ညှ":"ɲ̊",
    "ညွှ":"ɲ̊w", //endOfSyllabe Unicode hack or additional var
    "ယှ":"ʃ",
    "ဝှ":"ʍ",
}

//needs to be checked with other ipa sources 
//https://en.wiktionary.org/wiki/Wiktionary:Burmese_transliteration
//exchange consonant after vowel
const CONSONANT_AFTER_ROMAN_VOWEL_TYPE = {	//change consonant after roman vowel   
    "သ": "ð",
    "ပ": "b",
    //"ဆ": "z",
    /*  "က": "g",
      "ခ": "g",
      "စ": "z",
        "တ": "ð",
      "ထ": "d",
      "ဋ": "d",
      "ဌ": "d",      
      "ဖ": "b",*/
}
const CONSONANT_AFTER_ROMAN_VOWEL_MULTICHAR_TYPE = {
    "ကြ": "ʥ",
    /*
    "ခြ": "gy",	//not cha+y (chy)
    "ချ": "gy",	//not cha+y (chy)*/
}

const VOWEL_CHARACTERS_TYPE = {
    Independent: {
        "ဧ": "ʔè",
        "၏": "e",
        "ဣ": "ʔḭ",
        "ဤ": "ʔì",
        "ဥ": "ʔṵ",
        "ဦ": "ʔù",
        "ဩ": "ʔɔ́",
        "ဩော": "ʔɔ̀",
        "ဪ": "ʔɔ̀",

    },
    Dependent: {
        "ာ": "à",
        "ါ": "ə",
        "ေ": "è",
        "ဲ": "ɛ́",
        "ိ": "ḭ",
        "ီ": "ì",
        "ို": "ò",
        "ု": "ṵ",
        "ူ": "ù",
        "ော": "ɔ́",
        "ော်": "ɔ̀",
        "ေါ": "aw",
        "ေါ်": "aw",
        "္": "",
        //extra from basic bgn
     //   "ား": "à",
        "ဲ့": "ɛ̰",
    }
}
const CONSONANT_END_OF_SYLLABLE_TYPE = {
    //vv below vv different from BGN? additional for readability
    "ေါက်": "au",
    "ေါင်": "aun",
    "ိုဒ်": "aik", //example cases ဂိုဒ်, ပိုဒ်
    "ဉ္စ": "yinsa",//exception stacked character: example case ပဉ္စမ (pyin); not real final character

    //unicode "hack"/quickfix needs to be done differently
    //"ွှတ်":"hut", // ွှ needs to be written circle comma rest endofsyllable -> should be h-C-ut not C-hut
    //^^ atop ^^  different from BGN? additional for readability

    "က်": "ɛʔ",
    "ိုက်": "aɪʔ",
    "ောက်": "aʊʔ",

    "င်": "ɪ̀ɴ",
    "ိင်": "ein", //missing in BGN? example name [Theingi] သိင်္ဂီ  - သ +  ိင် + ဂ +ီ found in https://att-astrec.nict.go.jp/member/ding/PACLING2017-MY.pdf
    "ိုင်": "àɪɴ",
    "ောင်": "àʊɴ",

    "စ်": "iʔ",	//numbers sway -> (a) instead of (it) for တစ်(tit)/နှစ်(hnit) + CL
    "ိစ်": "eit", //not in BGN //maybe "eiʔ" more correct

    //more often e?
    "ည်": "|ì|", //!note 10: ည် is romanized i, in or e, depending on pronunciation.  A reference source should be consulted in case of uncertainty. 

    //5 / 7
    "တ်": "aʔ",
    "ိတ်": "eiʔ",
    "ုတ်": "oʊʔ",
    "ွတ်": "ʊʔ",
    "ဝတ်": "wʊʔ",// maybe "false" endOfSyllable? vocab if same handling like "ဝမ်"; search for example vocab
    "ေတ်": "it",//maybe "iʔ" more correct?

    "ပ်": "aʔ",
    "ိပ်": "eiʔ",
    "ုပ်": "oʊʔ",
    "ွပ်": "ʊʔ",
    "ဝပ်":"wʊʔ",

    //6 / 8
    "န်": "àɴ",
    "ိန်": "èɪɴ",
    "ုန်": "òʊɴ",
    "ွန်": "ʊ̀ɴ",
    "ဝန်": "wʊ̀ɴ",// "false" endOfSyllable? C+EndSyllable combination

    "မ်": "àɴ",
    "ိမ်": "èɪɴ",
    "ုမ်": "òʊɴ",
    "ွမ်": "ʊ̀ɴ",
    "ဝမ်": "wʊ̀ɴ",// "false" endOfSyllable? C+EndSyllable combination


    "ယ်": "ɛ̀",
    "ဥ်": "ɪ̀ɴ",	//ဥ (u)+်
    "ဉ်": "ɪ̀ɴ", //ဉ (nya)+်	//added. one or the other is needed and not in BGN table

    "ဝံ": "wʊ̀ɴ",
    "ံ": "àɴ",
    "ိံ": "èɪɴ",
    "ုံ": "òʊɴ",

    //"false" EndOfSyllable
    //https://en.wiktionary.org/wiki/Wiktionary:Burmese_transliteration
    //In sequences with ွ or ဝ before တ်, န်, ပ်, မ် or together with ံ, the vowel pronounced is /u/ rather than the expected /(w)a/
    "ွံ": "ʊ̀ɴ",

    "်": "",	//end of syllable final mark	//needed for example baNdaik "ဘဏ်တိုက်"

    /* Colloquial Burmese p.178(p.156)
    The ending -လ် does not affect the pronunciation in any way. For example သို and သိုလ် are both pronounced as (to); ဗို and ဗိုလ် are both pronounced as (bo)
    */
    "လ်": "",

    //Tones not in vowel list
    "န်း": "áɴ",
    "န့်": "a̰ɴ",
   // "ါး":"əʔ", //double check

   //special combination need to check BGN and others example အဘိဓာန် [dictionary] /əbídæɴ/
    "ာန်":"æɴ",
}

const TONE_SENTENCE_MARK_TYPE = {	//original BGN without tone mark transliteration
    // https://en.wiktionary.org/wiki/Wiktionary:Burmese_transliteration
    "း": ":",	//needs other marker because of sentence endpoint maybe ´
    "့": ".",	//needs other marker because of sentence endpoint maybe  
    /*
    ့	- "t" https://att-astrec.nict.go.jp/member/ding/PACLING2017-MY.pdf
    or backtrack search first vowel (case -at/-on/-un etc) or beginning of vowel (aung) to put on "´" "`"
    */
    "၊": ", ",
    "။": ". ",
    "၏": ". ",	//https://en.wikipedia.org/wiki/Burmese_alphabet "used as a full stop if the sentence immediately ends with a verb."
}

const endSyllableKeyArray = Object.keys(CONSONANT_END_OF_SYLLABLE_TYPE);
const toneSentenceMarkArray = Object.keys(TONE_SENTENCE_MARK_TYPE);
const syllableAndTonemarkCriteria = '(' + endSyllableKeyArray.sort((a, b) => b.length - a.length).join("|") + ")([" + toneSentenceMarkArray.join("") + "]*)";//tripple split result [no match, first group(syllable), second group(toneMark)]
const REGEX_ENDOFSYLLABLE_AND_TONEMARKS_TYPE = new RegExp(syllableAndTonemarkCriteria, "g");

const REGEX_STARTSWITH_CONSONANT_TYPE = new RegExp("^(" + Object.keys(CONSONANT_TYPE).join("|") + ")");
const REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATION_TYPE = new RegExp("^(" + Object.keys(CONSONANT_CHARACTER_COMBINATIONS_TYPE).sort((a, b) => b.length - a.length).join("|") + ")");
const REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATIONS_TYPE = new RegExp("^([" + Object.keys(CONSONANT_CHARACTER_COMBINATIONS_TYPE).sort((a, b) => b.length - a.length).join("|") + "]*)");
const REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR_TYPE = new RegExp("^(" + Object.keys(CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR_TYPE).sort((a, b) => b.length - a.length).join("|") + ")");
const REGEX_STARTSWITH_VOWELINDEPENDENT_TYPE = new RegExp("^(" + Object.keys(VOWEL_CHARACTERS_TYPE["Independent"]).sort((a, b) => b.length - a.length).join("|") + ")");
const REGEX_STARTSWITH_VOWELDEPENDENT_TYPE = new RegExp("^(" + Object.keys(VOWEL_CHARACTERS_TYPE["Dependent"]).sort((a, b) => b.length - a.length).join("|") + ")");
const REGEX_CONSONANT_END_OF_SYLLABLE_TYPE = new RegExp(Object.keys(CONSONANT_END_OF_SYLLABLE_TYPE).sort((a, b) => b.length - a.length).join("|"));
const REGEX_STARTSWITH_CONSONANT_END_OF_SYLLABLE_TYPE = new RegExp("^(" + Object.keys(CONSONANT_END_OF_SYLLABLE_TYPE).sort((a, b) => b.length - a.length).join("|") + ")");
const REGEX_STARTSWITH_TONE_SENTENCE_MARK_TYPE = new RegExp("^([" + toneSentenceMarkArray.join("") + "]*)");
//const REGEX_TONE_SENTENCE_MARK = new RegExp("([" + toneSentenceMarkArray.join("") + "]*)");
//TODO how to define regex without expanding variant



export const TRANSLITERATEOBJECT_IPA = {
    CONSONANT: CONSONANT_TYPE,
    CONSONANT_MODIFICATION_YAPIN_YAYIT: CONSONANT_MODIFICATION_YAPIN_YAYIT_TYPE,
    CONSONANT_CHARACTER_COMBINATIONS: CONSONANT_CHARACTER_COMBINATIONS_TYPE,
    CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR: CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR_TYPE,
    CONSONANT_AFTER_ROMAN_VOWEL: CONSONANT_AFTER_ROMAN_VOWEL_TYPE,
    CONSONANT_AFTER_ROMAN_VOWEL_MULTICHAR: CONSONANT_AFTER_ROMAN_VOWEL_MULTICHAR_TYPE,
    VOWEL_CHARACTERS: VOWEL_CHARACTERS_TYPE,
    CONSONANT_END_OF_SYLLABLE: CONSONANT_END_OF_SYLLABLE_TYPE,
    TONE_SENTENCE_MARK: TONE_SENTENCE_MARK_TYPE,
    REGEX_ENDOFSYLLABLE_AND_TONEMARKS: REGEX_ENDOFSYLLABLE_AND_TONEMARKS_TYPE,
    REGEX_STARTSWITH_CONSONANT: REGEX_STARTSWITH_CONSONANT_TYPE,
    REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATION: REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATION_TYPE,
    REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATIONS: REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATIONS_TYPE,
    REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR: REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR_TYPE,
    REGEX_STARTSWITH_VOWELINDEPENDENT: REGEX_STARTSWITH_VOWELINDEPENDENT_TYPE,
    REGEX_STARTSWITH_VOWELDEPENDENT: REGEX_STARTSWITH_VOWELDEPENDENT_TYPE,
    REGEX_CONSONANT_END_OF_SYLLABLE: REGEX_CONSONANT_END_OF_SYLLABLE_TYPE,
    REGEX_STARTSWITH_CONSONANT_END_OF_SYLLABLE: REGEX_STARTSWITH_CONSONANT_END_OF_SYLLABLE_TYPE,
    REGEX_STARTSWITH_TONE_SENTENCE_MARK: REGEX_STARTSWITH_TONE_SENTENCE_MARK_TYPE,
};