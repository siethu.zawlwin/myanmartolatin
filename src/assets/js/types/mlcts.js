//https://en.wikipedia.org/wiki/MLC_Transcription_System
const CONSONANT_TYPE = {// romanization+a when not followed by vowel character?
    "က": "k",
    "ခ": "kh",
    "ဂ": "g",
    "ဃ": "gh",
    "င": "ng",
    "စ": "c",
    "ဆ": "ch",
    "ဇ": "j",	//can also be j? ဝိဇ္ဇာ (wijja)[knowledge] //https://en.wikipedia.org/wiki/Burmese_alphabet
    "ဈ": "jh",
    "ည": "ny",
    "ဉ": "ny",
    "တ": "t",
    "ထ": "th",
    "ဋ": "t",
    "ဌ": "th",
    "ဒ": "d",
    "ဍ": "d",
    "ဓ": "dh",
    "ဎ": "dh",
    "န": "n",
    "ဏ": "n",
    "ပ": "p",
    "ဖ": "ph",
    "ဗ": "b",
    "ဘ": "bh",
    "မ": "m",
    "ယ": "y",
    "ရ": "r",	//r substitute for names not rooted of burmese: example paris, karaweik(pali "karavika")
    "လ": "l",
    "ဠ": "l",
    "ဝ": "w",
    "သ": "s",
    "ဟ": "h",
    "အ": "a",

    //https://en.wikipedia.org/wiki/Burmese_alphabet 
    //https://r12a.github.io/scripts/myanmar/
    //consonant repetition appears within words, whereas သ်သ is used across word boundaries
    "ဿ": "ss",
};
const CONSONANT_CHARACTER_COMBINATIONS_TYPE = { // "-" instead of C for Consonant position
//https://en.wikipedia.org/wiki/MLC_Transcription_System#Medial_consonants
//They follow the following order in transcription: h-, -y- or -r-, and -w-.
    "ြ": "-r",	//Ya yit (ရရစ်)
    "ျ": "-y",	//Ya pin (ယပင့်)
    "ွ": "-w",	//Wa hswe (ဝဆွဲ) 
    "ှ": "h-",	//Ha hto (ဟထိုး) 

    "ြွ": "-rw",
    "ျွ": "-yw",
    //needs to be done better // same error like with EndOfSyllable လွှတ် (hlut)[to release]
    "ြှ": "h-r",// not BGN correct should be hCy needs to be done in 
    //returnTargetRomanConsonant -consonantCombinationCount
    //vv page64 more consonant clusters vv
    "ျှ": "h-y",
    "ွှ": "h-w",
    //^more consonant clusters ^
};

/* //unusal signs p.216(p.238)
	သြ	= အော aw
	သျှ = ရှ	sha
    */

const CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR_TYPE = {
    //multichar extra check
   // "ခြ": "hkr",	//not ky
  //  "ချ": "hky",	//not ky
    "ရှ": "hr",	//not hy
   // "ရွှ": "hrw",		//for gold ရွှေ //exception better pronounciation
    //"ရှွ":"shw", // for gold ရွှေ //exception better pronounciation //after unicode processing
    //colloqial burmese p.210(p.187) လျှ can be (hly-) and (sh-)
  //  "လျှ": "hly",	//exception? alternative C-yin/hly- possible
  //  "သျှ": "hsy",	//exception for example british ဗြိတိသျှ //colloquial burmese p.240(p.217)
    //C+ျှ -> "sh" ?
    //"ယှ":"hy" //MLCTS
}

//exchange consonant after vowel
const CONSONANT_AFTER_ROMAN_VOWEL_TYPE = {	//change consonant after roman vowel
 /*   "က": "g",
    "ခ": "g",
    "စ": "z",
    "ဆ": "z",
    "တ": "d",
    "ထ": "d",
    "ဋ": "d",
    "ဌ": "d",
    "ပ": "b",
    "ဖ": "b",*/
}
const CONSONANT_AFTER_ROMAN_VOWEL_MULTICHAR_TYPE = {
 /*   "ခြ": "gy",	//not cha+y (chy)
    "ချ": "gy",	//not cha+y (chy)*/
}

const VOWEL_CHARACTERS_TYPE = {
    Independent: {
        "ဧ": "e",
        "၏": "e",
        "ဣ": "i",
        "ဤ": "i",
        "ဥ": "u",
        "ဦ": "u",
        "ဩ": "au:",
        "ဩော": "au:",
        "ဪ": "au",

    },
    Dependent: {
        "ာ": "a",
        "ါ": "a",
        "ေ": "e",
        "ဲ": "ai:",
        "ိ": "i.",
        "ီ": "i",
        "ို": "ui",
        "ု": "u.",
        "ူ": "u",
        "ော": "au:",
        "ော်": "au",
        "ေါ": "aw",
        "ေါ်": "aw",
        "္": "",
        
        "ဲ့": "ai.",
        "ော့": "au.",
    }
}
const CONSONANT_END_OF_SYLLABLE_TYPE = {
    //vv below vv different from BGN? additional for readability
    "ေါက်": "au",
    "ေါင်": "aun",
    "ိုဒ်": "aik", //example cases ဂိုဒ်, ပိုဒ်
    "ဉ္စ": "yinsa",//exception stacked character: example case ပဉ္စမ (pyin); not real final character

    //unicode "hack"/quickfix needs to be done differently
    //"ွှတ်":"hut", // ွှ needs to be written circle comma rest endofsyllable -> should be h-C-ut not C-hut
    //^^ atop ^^  different from BGN? additional for readability

    //Consonant k
    "က်": "ak",
    "ွက်": "wak", //not needed? combination of medial + က်
    "ိုက်": "uik",
    "ောက်": "auk",

    //Consonant c
    "စ်": "ac",	//numbers sway -> (a) instead of (it) for တစ်(tit)/နှစ်(hnit) + CL

    //"ိစ်": "eit", //not in BGN    not listed in wiki page

    //Consonant t
    //5 / 7
    "တ်": "at",
    "ိတ်": "it",
    "ုတ်": "ut",
    "ွတ်": "wat",
    //"ဝတ်": "wat",// maybe "false" endOfSyllable? vocab if same handling like "ဝမ်"; search for example vocab
    "ေတ်": "it", //BGN/IPA adjustment

     //Consonant p
    "ပ်": "p",
    "ိပ်": "ip",
    "ုပ်": "up",
    "ွပ်": "wap",

     //nasalised Finals
     //more often e?
    "ည်": "any", //!note 10: ည် is romanized i, in or e, depending on pronunciation.  A reference source should be consulted in case of uncertainty. 

    "င်": "ang",
    "ိင်": "ein", //missing in BGN? example name [Theingi] သိင်္ဂီ  - သ +  ိင် + ဂ +ီ found in https://att-astrec.nict.go.jp/member/ding/PACLING2017-MY.pdf
    "ိုင်": "uing",
    "ောင်": "aung",

    //6 / 8
    "န်": "an",
    "ိန်": "in",
    "ုန်": "un",
    "ွန်": "wan",
    //"ဝန်": "wan",// "false" endOfSyllable? C+EndSyllable combination

    "မ်": "am",
    "ိမ်": "im",
    "ုမ်": "um",
    "ွမ်": "wam",
   // "ဝမ်": "wam",// "false" endOfSyllable? C+EndSyllable combination


    "ယ်": "ai", //vowel? monophthongs
    "ဥ်": "any",	//ဥ (u)+်
    "ဉ်": "any", //ဉ (nya)+်	//added. one or the other is needed and not in BGN table

    //"ဝံ": "wam",
    "ံ": "am",
    "ိံ": "im", //BGN/IPA adjustment
    "ုံ": "um",

    "်": "",	//end of syllable final mark	//needed for example baNdaik "ဘဏ်တိုက်"

    /* Colloquial Burmese p.178(p.156)
    The ending -လ် does not affect the pronunciation in any way. For example သို and သိုလ် are both pronounced as (to); ဗို and ဗိုလ် are both pronounced as (bo)
    */
   // "လ်": "",
}

const TONE_SENTENCE_MARK_TYPE = {	//original BGN without tone mark transliteration
    // https://en.wiktionary.org/wiki/Wiktionary:Burmese_transliteration
    "း": ":",	//needs other marker because of sentence endpoint maybe ´
    "့": ".",	//needs other marker because of sentence endpoint maybe  
    /*
    ့	- "t" https://att-astrec.nict.go.jp/member/ding/PACLING2017-MY.pdf
    or backtrack search first vowel (case -at/-on/-un etc) or beginning of vowel (aung) to put on "´" "`"
    */
    "၊": ", ",
    "။": ". ",
    "၏": ". ",	//https://en.wikipedia.org/wiki/Burmese_alphabet "used as a full stop if the sentence immediately ends with a verb."
}

const endSyllableKeyArray = Object.keys(CONSONANT_END_OF_SYLLABLE_TYPE);
const toneSentenceMarkArray = Object.keys(TONE_SENTENCE_MARK_TYPE);
const syllableAndTonemarkCriteria = '(' + endSyllableKeyArray.sort((a, b) => b.length - a.length).join("|") + ")([" + toneSentenceMarkArray.join("") + "]*)";//tripple split result [no match, first group(syllable), second group(toneMark)]
const REGEX_ENDOFSYLLABLE_AND_TONEMARKS_TYPE = new RegExp(syllableAndTonemarkCriteria, "g");

const REGEX_STARTSWITH_CONSONANT_TYPE = new RegExp("^(" + Object.keys(CONSONANT_TYPE).join("|") + ")");
const REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATION_TYPE = new RegExp("^(" + Object.keys(CONSONANT_CHARACTER_COMBINATIONS_TYPE).sort((a, b) => b.length - a.length).join("|") + ")");
const REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATIONS_TYPE = new RegExp("^([" + Object.keys(CONSONANT_CHARACTER_COMBINATIONS_TYPE).sort((a, b) => b.length - a.length).join("|") + "]*)");
const REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR_TYPE = new RegExp("^(" + Object.keys(CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR_TYPE).sort((a, b) => b.length - a.length).join("|") + ")");
const REGEX_STARTSWITH_VOWELINDEPENDENT_TYPE = new RegExp("^(" + Object.keys(VOWEL_CHARACTERS_TYPE["Independent"]).sort((a, b) => b.length - a.length).join("|") + ")");
const REGEX_STARTSWITH_VOWELDEPENDENT_TYPE = new RegExp("^(" + Object.keys(VOWEL_CHARACTERS_TYPE["Dependent"]).sort((a, b) => b.length - a.length).join("|") + ")");
const REGEX_CONSONANT_END_OF_SYLLABLE_TYPE = new RegExp(Object.keys(CONSONANT_END_OF_SYLLABLE_TYPE).sort((a, b) => b.length - a.length).join("|"));
const REGEX_STARTSWITH_CONSONANT_END_OF_SYLLABLE_TYPE = new RegExp("^(" + Object.keys(CONSONANT_END_OF_SYLLABLE_TYPE).sort((a, b) => b.length - a.length).join("|") + ")");
const REGEX_STARTSWITH_TONE_SENTENCE_MARK_TYPE = new RegExp("^([" + toneSentenceMarkArray.join("") + "]*)");
//const REGEX_TONE_SENTENCE_MARK = new RegExp("([" + toneSentenceMarkArray.join("") + "]*)");
//TODO how to define regex without expanding variant



export const TRANSLITERATEOBJECT_MLCTS = {
    CONSONANT: CONSONANT_TYPE,
    CONSONANT_CHARACTER_COMBINATIONS: CONSONANT_CHARACTER_COMBINATIONS_TYPE,
    CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR: CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR_TYPE,
    CONSONANT_AFTER_ROMAN_VOWEL: CONSONANT_AFTER_ROMAN_VOWEL_TYPE,
    CONSONANT_AFTER_ROMAN_VOWEL_MULTICHAR: CONSONANT_AFTER_ROMAN_VOWEL_MULTICHAR_TYPE,
    VOWEL_CHARACTERS: VOWEL_CHARACTERS_TYPE,
    CONSONANT_END_OF_SYLLABLE: CONSONANT_END_OF_SYLLABLE_TYPE,
    TONE_SENTENCE_MARK: TONE_SENTENCE_MARK_TYPE,
    REGEX_ENDOFSYLLABLE_AND_TONEMARKS: REGEX_ENDOFSYLLABLE_AND_TONEMARKS_TYPE,
    REGEX_STARTSWITH_CONSONANT: REGEX_STARTSWITH_CONSONANT_TYPE,
    REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATION: REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATION_TYPE,
    REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATIONS: REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATIONS_TYPE,
    REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR: REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR_TYPE,
    REGEX_STARTSWITH_VOWELINDEPENDENT: REGEX_STARTSWITH_VOWELINDEPENDENT_TYPE,
    REGEX_STARTSWITH_VOWELDEPENDENT: REGEX_STARTSWITH_VOWELDEPENDENT_TYPE,
    REGEX_CONSONANT_END_OF_SYLLABLE: REGEX_CONSONANT_END_OF_SYLLABLE_TYPE,
    REGEX_STARTSWITH_CONSONANT_END_OF_SYLLABLE: REGEX_STARTSWITH_CONSONANT_END_OF_SYLLABLE_TYPE,
    REGEX_STARTSWITH_TONE_SENTENCE_MARK: REGEX_STARTSWITH_TONE_SENTENCE_MARK_TYPE,
};