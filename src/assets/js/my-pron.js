//import { expose } from "threads/worker"
//better syllable breaker https://github.com/ye-kyaw-thu/sylbreak
/*
	loosely based on script from https://en.wiktionary.org/wiki/Module:my-pron
	table content copy

	
	ROMANIZATION OF BURMESE @https://en.wikipedia.org/wiki/BGN/PCGN_romanization_of_Burmese
	BGN/PCGN 1970 Agreement. Retrieved 18 July 2015. (Used pdf version was checked for validity and accuracy - October 2017 )
		following BGN/PCGN 1970 Agreement (This system is an amplified restatement of the 1907 version of the Tables for the 
		Transliteration  of  Burmese  into  English,  published  in  1908  by  the  Office  of  the 
		Superintendent, Government Printing, Rangoon, Burma. )

	-missing Consonant added from from https://en.wiktionary.org/wiki/Wiktionary:Burmese_transliteration

	BGN romanization extended with stacked deconstruction of double stacked words
	https://www.asiapearltravels.com/language/lesson33.php	//double stacked words
	https://www.asiapearltravels.com/language/myanmar-language-pdf-download-page.php
		https://www.asiapearltravels.com/language/myanmar_script.pdf
	added rules to BGN for double stacked characters:
			- if stacked -> put middle character in front with added final
				- ဗန္ဓုလ read as ဗန်ဓုလ
				- ပိန္နဲပင် read as ပိန်နဲပင်
			- if C has ေ + double stacked follows -> add ြ+middle C+် ?
				- မေတ္တာ -> မြေတ်တာ
			Simplified stacked words
			- if syllable(C+V) has no final (+ next is not double stacked and has ြ yayit ) use next consonant as double character with final?
				- (သောကြာနေ့ - Friday is read as သောက် + ကြာ + နေ့ သောကြာနေ့ - Friday is read as သောက် + ကြာ + နေ့)
			- double stacked if C+final(်)without next consonant()
				- ယောက်ျား -> ယောက်ကျား (YaukKya) [husband,man]
				- ကျွန်ုပ် -> ကျွန်နုပ် (KyunNôk)

	second BGN transliteration version
	https://www.eki.ee/wgrs/rom2_my.pdf


	current transliteration state not working for stacked characters  are there other functions available?
		ဝတ္ထု
		မိတ္တူကူး	(mittugu) instead of /MeikTuGu:/ /meiʔtukù/
	https://www.ushuaia.pl/transliterate/?ln=en
*/
'use strict';

import { transliterate as tr } from 'transliteration';
import { TRANSLITERATEOBJECT_BGN } from "./types/bgn";
import { TRANSLITERATEOBJECT_BGNPCGN } from "./types/bgnpcgn";
import { TRANSLITERATEOBJECT_MLCTS } from "./types/mlcts";
import { TRANSLITERATEOBJECT_OKELL } from "./types/okell";
import { TRANSLITERATEOBJECT_IPA } from "./types/ipa";
import { NOUNS } from "./myanmarLinguistics/nouns"
import { VERBS } from "./myanmarLinguistics/verbs"
import { ADJECTIVES } from "./myanmarLinguistics/adjectives"
import { ADVERBS } from "./myanmarLinguistics/adverbs"
//https://blog.appsignal.com/2020/05/06/avoiding-memory-leaks-in-nodejs-best-practices-for-performance.html
//https://www.javascripttutorial.net/javascript-immediately-invoked-function-expression-iife/
//changed to iife, avoid global vars
/*
const testApp = (function () {
const testVar ="test content";
function testFunction(textToOutput){
	console.log("testVar: " + testVar);
	console.log("textToOutput: " + textToOutput)
}

return { testFunction:testFunction,
};
})

testApp().testFunction("custom variable output");
*/
//console.log(process.env.NODE_ENV) //vue https://vuejs.org/v2/guide/deployment.html
var GLOBALDEBUG = true && process.env.NODE_ENV == "development";
if (!GLOBALDEBUG && typeof(window) !== 'undefined') {
	if (!window.console) window.console = {};
	let methods = ["log", "debug", "warn", "info", "group", "groupEnd", "groupCollapsed"];
	for (let i = 0; i < methods.length; i++) {
		console[methods[i]] = function () { };
	}
}

const compoundWordTest = [
	...NOUNS,
	...VERBS
]

//used together with question word လား la
const primaryTenses = [

]

const possibleTensesAfterVerb = [ //Everyday spoken burmese /currently up to page 22
	"တယ်", //present
	"မယ်", //near future
	"မလား", // Try to understand  မ  | ma1 before the question mark  လား  | la3 as a short spoken sound of  မယ်  | meare2 in the question
	"လိမ့်မယ်", // happen in the near future
	"တော့မယ်", // verb-suffix particle to indicate that the action is imminent or about to happen in the near future
	"တော့", //without   မယ်  |  meare2 is a persuasion or an advice to do something immediately
	//how to handle တော့  | dau1 can also be used as an emphasis.
	//တေ ာ်တော တေ ာ်ပါတယ် (tau2 dau1 tau2 ba2 deare2)[She is bright alright. ]
	//differentiation noun / adjective?
	"တော့မလား", //question: [verb] is about to happen?
	"ခဲ့တယ်", //past tense

	"ဦးမလား", //still want to [verb]? - The person asking the question expects somewhat a negative answer.
	"ဦးမယ်",  //still want to do [verb]
	"ပါ", //Making a suggestion
	"ပါဦး",//softer more polite suggestion

	"ပြီ", //already
	"သေးတယ်", //still something
	"သေးလား", // still [verb]?

	/*
	have to properly check for negative ma in verb 
	example နဲ့ would be no tense after verb but a "postpositional marker" [with] for a noun
	*/
	"ဘူး", //
	"တော့ဘူး", //Immediate plan has changed if immediate action before တေ ာ
	"နဲ့", //Someone is telling you not to do something.
	"ပါနဲ့", // more polite - Someone is telling you not to do something.
	"သေးဘူး", //something still has not happend, not ready or done yet

]
console.log("compoundWordTest.length: " + compoundWordTest.length)
const REGEX_ENDSWITH_NOUN = new RegExp("(" + compoundWordTest.join("|") + ")$");

export const TypeEnums = {
	"BGN": "BGN",
	"BGNPCGN": "BGNPCGN",
	"IPA": "IPA",
	"MLCTS": "MLCTS",
	"ALA-LC": "ALA-LC",
	"Okell": "Okell"
}
Object.freeze(TypeEnums);

export const emptySyllable = { fullPhonem: "", consonant: "", hasConsonantCombination: "", hasConsonantMultichar: "", vowel: "", endOfSyllable: "", toneSentenceMark: "", hasToneSentenceMarkAfterEndOfSyllable: "" }
var myPronApp = (function () {

	//const nonMyanmarCharacters = /([^ဆတနမအပကငသစဟဩေျိ်ါ့ြုူး၏ဖထခလဘညာဈဝဣ၎ဤ၌ဥ၍ဿဏဧဪဗှီ္ွံဲဒဓဂၑဇဌဃဠယဉဦ၊။၁၂၃၄၅၆၇၈၉၀ၐဎဍၒဋၓၔၕရ])+/; //include empty space [number + " "+ CL]
	const MyanmarCharacters = "ဆတနမအပကငသစဟဩေျိ်ါ့ြုူး၏ဖထခလဘညာဈဝဣ၎ဤ၌ဥ၍ဿဏဧဪဗှီ္ွံဲဒဓဂၑဇဌဃဠယဉဦ၊။၁၂၃၄၅၆၇၈၉၀ၐဎဍၒဋၓၔၕရ";//include empty space [number + " "+ CL]
	const REGEXMYANMARCHARACTERS = new RegExp("([" + MyanmarCharacters + "]+)");


	const CONSONANT = {
		[TypeEnums.BGN]: TRANSLITERATEOBJECT_BGN.CONSONANT,
		[TypeEnums.BGNPCGN]: TRANSLITERATEOBJECT_BGNPCGN.CONSONANT,
		[TypeEnums.MLCTS]: TRANSLITERATEOBJECT_MLCTS.CONSONANT,
		[TypeEnums.Okell]: TRANSLITERATEOBJECT_OKELL.CONSONANT,
		[TypeEnums.IPA]: TRANSLITERATEOBJECT_IPA.CONSONANT,
	}
	const CONSONANT_MODIFICATION_YAPIN_YAYIT = {
		[TypeEnums.BGN]: {},
		[TypeEnums.BGNPCGN]: {},
		[TypeEnums.MLCTS]: {},
		[TypeEnums.Okell]: TRANSLITERATEOBJECT_OKELL.CONSONANT_MODIFICATION_YAPIN_YAYIT,
		[TypeEnums.IPA]: TRANSLITERATEOBJECT_IPA.CONSONANT_MODIFICATION_YAPIN_YAYIT,
	}
	const CONSONANT_CHARACTER_COMBINATIONS = {
		[TypeEnums.BGN]: TRANSLITERATEOBJECT_BGN.CONSONANT_CHARACTER_COMBINATIONS,
		[TypeEnums.BGNPCGN]: TRANSLITERATEOBJECT_BGNPCGN.CONSONANT_CHARACTER_COMBINATIONS,
		[TypeEnums.MLCTS]: TRANSLITERATEOBJECT_MLCTS.CONSONANT_CHARACTER_COMBINATIONS,
		[TypeEnums.Okell]: TRANSLITERATEOBJECT_OKELL.CONSONANT_CHARACTER_COMBINATIONS,
		[TypeEnums.IPA]: TRANSLITERATEOBJECT_IPA.CONSONANT_CHARACTER_COMBINATIONS,
	}
	const CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR = {
		[TypeEnums.BGN]: TRANSLITERATEOBJECT_BGN.CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR,
		[TypeEnums.BGNPCGN]: TRANSLITERATEOBJECT_BGNPCGN.CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR,
		[TypeEnums.MLCTS]: TRANSLITERATEOBJECT_MLCTS.CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR,
		[TypeEnums.Okell]: TRANSLITERATEOBJECT_OKELL.CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR,
		[TypeEnums.IPA]: TRANSLITERATEOBJECT_IPA.CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR,
	}

	const CONSONANT_AFTER_ROMAN_VOWEL = {
		[TypeEnums.BGN]: TRANSLITERATEOBJECT_BGN.CONSONANT_AFTER_ROMAN_VOWEL,
		[TypeEnums.BGNPCGN]: TRANSLITERATEOBJECT_BGNPCGN.CONSONANT_AFTER_ROMAN_VOWEL,
		[TypeEnums.MLCTS]: TRANSLITERATEOBJECT_MLCTS.CONSONANT_AFTER_ROMAN_VOWEL,
		[TypeEnums.Okell]: TRANSLITERATEOBJECT_OKELL.CONSONANT_AFTER_ROMAN_VOWEL,
		[TypeEnums.IPA]: TRANSLITERATEOBJECT_IPA.CONSONANT_AFTER_ROMAN_VOWEL,
	}
	const CONSONANT_AFTER_ROMAN_VOWEL_MULTICHAR = {
		[TypeEnums.BGN]: TRANSLITERATEOBJECT_BGN.CONSONANT_AFTER_ROMAN_VOWEL_MULTICHAR,
		[TypeEnums.BGNPCGN]: TRANSLITERATEOBJECT_BGNPCGN.CONSONANT_AFTER_ROMAN_VOWEL_MULTICHAR,
		[TypeEnums.MLCTS]: TRANSLITERATEOBJECT_MLCTS.CONSONANT_AFTER_ROMAN_VOWEL_MULTICHAR,
		[TypeEnums.Okell]: TRANSLITERATEOBJECT_OKELL.CONSONANT_AFTER_ROMAN_VOWEL_MULTICHAR,
		[TypeEnums.IPA]: TRANSLITERATEOBJECT_IPA.CONSONANT_AFTER_ROMAN_VOWEL_MULTICHAR,
	}
	const VOWEL_CHARACTERS = {
		[TypeEnums.BGN]: TRANSLITERATEOBJECT_BGN.VOWEL_CHARACTERS,
		[TypeEnums.BGNPCGN]: TRANSLITERATEOBJECT_BGNPCGN.VOWEL_CHARACTERS,
		[TypeEnums.MLCTS]: TRANSLITERATEOBJECT_MLCTS.VOWEL_CHARACTERS,
		[TypeEnums.Okell]: TRANSLITERATEOBJECT_OKELL.VOWEL_CHARACTERS,
		[TypeEnums.IPA]: TRANSLITERATEOBJECT_IPA.VOWEL_CHARACTERS,
	}

	const CONSONANT_END_OF_SYLLABLE = {
		[TypeEnums.BGN]: TRANSLITERATEOBJECT_BGN.CONSONANT_END_OF_SYLLABLE,
		[TypeEnums.BGNPCGN]: TRANSLITERATEOBJECT_BGNPCGN.CONSONANT_END_OF_SYLLABLE,
		[TypeEnums.MLCTS]: TRANSLITERATEOBJECT_MLCTS.CONSONANT_END_OF_SYLLABLE,
		[TypeEnums.Okell]: TRANSLITERATEOBJECT_OKELL.CONSONANT_END_OF_SYLLABLE,
		[TypeEnums.IPA]: TRANSLITERATEOBJECT_IPA.CONSONANT_END_OF_SYLLABLE,
	}
	const TONE_SENTENCE_MARK = {
		[TypeEnums.BGN]: TRANSLITERATEOBJECT_BGN.TONE_SENTENCE_MARK,
		[TypeEnums.BGNPCGN]: TRANSLITERATEOBJECT_BGNPCGN.TONE_SENTENCE_MARK,
		[TypeEnums.MLCTS]: TRANSLITERATEOBJECT_MLCTS.TONE_SENTENCE_MARK,
		[TypeEnums.Okell]: TRANSLITERATEOBJECT_OKELL.TONE_SENTENCE_MARK,
		[TypeEnums.IPA]: TRANSLITERATEOBJECT_IPA.TONE_SENTENCE_MARK,
	};


	const REGEX_STARTSWITH_CONSONANT = {
		[TypeEnums.BGN]: TRANSLITERATEOBJECT_BGN.REGEX_STARTSWITH_CONSONANT,
		[TypeEnums.BGNPCGN]: TRANSLITERATEOBJECT_BGNPCGN.REGEX_STARTSWITH_CONSONANT,
		[TypeEnums.MLCTS]: TRANSLITERATEOBJECT_MLCTS.REGEX_STARTSWITH_CONSONANT,
		[TypeEnums.Okell]: TRANSLITERATEOBJECT_OKELL.REGEX_STARTSWITH_CONSONANT,
		[TypeEnums.IPA]: TRANSLITERATEOBJECT_IPA.REGEX_STARTSWITH_CONSONANT,
	}
	const REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATION = {
		[TypeEnums.BGN]: TRANSLITERATEOBJECT_BGN.REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATION,
		[TypeEnums.BGNPCGN]: TRANSLITERATEOBJECT_BGNPCGN.REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATION,
		[TypeEnums.MLCTS]: TRANSLITERATEOBJECT_MLCTS.REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATION,
		[TypeEnums.Okell]: TRANSLITERATEOBJECT_OKELL.REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATION,
		[TypeEnums.IPA]: TRANSLITERATEOBJECT_IPA.REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATION,
	}
	const REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATIONS = {
		[TypeEnums.BGN]: TRANSLITERATEOBJECT_BGN.REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATIONS,
		[TypeEnums.BGNPCGN]: TRANSLITERATEOBJECT_BGNPCGN.REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATIONS,
		[TypeEnums.MLCTS]: TRANSLITERATEOBJECT_MLCTS.REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATIONS,
		[TypeEnums.Okell]: TRANSLITERATEOBJECT_OKELL.REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATIONS,
		[TypeEnums.IPA]: TRANSLITERATEOBJECT_IPA.REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATIONS,
	}
	const REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR = {
		[TypeEnums.BGN]: TRANSLITERATEOBJECT_BGN.REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR,
		[TypeEnums.BGNPCGN]: TRANSLITERATEOBJECT_BGNPCGN.REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR,
		[TypeEnums.MLCTS]: TRANSLITERATEOBJECT_MLCTS.REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR,
		[TypeEnums.Okell]: TRANSLITERATEOBJECT_OKELL.REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR,
		[TypeEnums.IPA]: TRANSLITERATEOBJECT_IPA.REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR,
	}
	const REGEX_STARTSWITH_VOWELINDEPENDENT = {
		[TypeEnums.BGN]: TRANSLITERATEOBJECT_BGN.REGEX_STARTSWITH_VOWELINDEPENDENT,
		[TypeEnums.BGNPCGN]: TRANSLITERATEOBJECT_BGNPCGN.REGEX_STARTSWITH_VOWELINDEPENDENT,
		[TypeEnums.MLCTS]: TRANSLITERATEOBJECT_MLCTS.REGEX_STARTSWITH_VOWELINDEPENDENT,
		[TypeEnums.Okell]: TRANSLITERATEOBJECT_OKELL.REGEX_STARTSWITH_VOWELINDEPENDENT,
		[TypeEnums.IPA]: TRANSLITERATEOBJECT_IPA.REGEX_STARTSWITH_VOWELINDEPENDENT,
	}
	const REGEX_STARTSWITH_VOWELDEPENDENT = {
		[TypeEnums.BGN]: TRANSLITERATEOBJECT_BGN.REGEX_STARTSWITH_VOWELDEPENDENT,
		[TypeEnums.BGNPCGN]: TRANSLITERATEOBJECT_BGNPCGN.REGEX_STARTSWITH_VOWELDEPENDENT,
		[TypeEnums.MLCTS]: TRANSLITERATEOBJECT_MLCTS.REGEX_STARTSWITH_VOWELDEPENDENT,
		[TypeEnums.Okell]: TRANSLITERATEOBJECT_OKELL.REGEX_STARTSWITH_VOWELDEPENDENT,
		[TypeEnums.IPA]: TRANSLITERATEOBJECT_IPA.REGEX_STARTSWITH_VOWELDEPENDENT,
	}
	const REGEX_CONSONANT_END_OF_SYLLABLE = {
		[TypeEnums.BGN]: TRANSLITERATEOBJECT_BGN.REGEX_CONSONANT_END_OF_SYLLABLE,
		[TypeEnums.BGNPCGN]: TRANSLITERATEOBJECT_BGNPCGN.REGEX_CONSONANT_END_OF_SYLLABLE,
		[TypeEnums.MLCTS]: TRANSLITERATEOBJECT_MLCTS.REGEX_CONSONANT_END_OF_SYLLABLE,
		[TypeEnums.Okell]: TRANSLITERATEOBJECT_OKELL.REGEX_CONSONANT_END_OF_SYLLABLE,
		[TypeEnums.IPA]: TRANSLITERATEOBJECT_IPA.REGEX_CONSONANT_END_OF_SYLLABLE,
	}
	const REGEX_STARTSWITH_CONSONANT_END_OF_SYLLABLE = {
		[TypeEnums.BGN]: TRANSLITERATEOBJECT_BGN.REGEX_STARTSWITH_CONSONANT_END_OF_SYLLABLE,
		[TypeEnums.BGNPCGN]: TRANSLITERATEOBJECT_BGNPCGN.REGEX_STARTSWITH_CONSONANT_END_OF_SYLLABLE,
		[TypeEnums.MLCTS]: TRANSLITERATEOBJECT_MLCTS.REGEX_STARTSWITH_CONSONANT_END_OF_SYLLABLE,
		[TypeEnums.Okell]: TRANSLITERATEOBJECT_OKELL.REGEX_STARTSWITH_CONSONANT_END_OF_SYLLABLE,
		[TypeEnums.IPA]: TRANSLITERATEOBJECT_IPA.REGEX_STARTSWITH_CONSONANT_END_OF_SYLLABLE,
	}
	const REGEX_STARTSWITH_TONE_SENTENCE_MARK = {
		[TypeEnums.BGN]: TRANSLITERATEOBJECT_BGN.REGEX_STARTSWITH_TONE_SENTENCE_MARK,
		[TypeEnums.BGNPCGN]: TRANSLITERATEOBJECT_BGNPCGN.REGEX_STARTSWITH_TONE_SENTENCE_MARK,
		[TypeEnums.MLCTS]: TRANSLITERATEOBJECT_MLCTS.REGEX_STARTSWITH_TONE_SENTENCE_MARK,
		[TypeEnums.Okell]: TRANSLITERATEOBJECT_OKELL.REGEX_STARTSWITH_TONE_SENTENCE_MARK,
		[TypeEnums.IPA]: TRANSLITERATEOBJECT_IPA.REGEX_STARTSWITH_TONE_SENTENCE_MARK,
	}
	const REGEX_ENDOFSYLLABLE_AND_TONEMARKS = {
		[TypeEnums.BGN]: TRANSLITERATEOBJECT_BGN.REGEX_ENDOFSYLLABLE_AND_TONEMARKS,
		[TypeEnums.BGNPCGN]: TRANSLITERATEOBJECT_BGNPCGN.REGEX_ENDOFSYLLABLE_AND_TONEMARKS,
		[TypeEnums.MLCTS]: TRANSLITERATEOBJECT_MLCTS.REGEX_ENDOFSYLLABLE_AND_TONEMARKS,
		[TypeEnums.Okell]: TRANSLITERATEOBJECT_OKELL.REGEX_ENDOFSYLLABLE_AND_TONEMARKS,
		[TypeEnums.IPA]: TRANSLITERATEOBJECT_IPA.REGEX_ENDOFSYLLABLE_AND_TONEMARKS,
	}

	//https://en.wikipedia.org/wiki/Burmese_phonology#Burmese_voicing_sandhi
	//https://en.wikipedia.org/wiki/Burmese_alphabet#Letters
	/*
	needed for minor syllable consonant changes example (kinbya) -> kə-mya
	same for consonant after vowel?

	*/
	//Unaspirated/Aspirated -> Voiced, Nasal 	
	/*
	Sandhi can occur in two environments. In the first environment, consonants become voiced between vowels or after nasals. 
	"hot water": [jèbù] ရေပူ ← /jè/ + /pù/
	When a syllable becomes reduced, if both the consonant preceding and following the schwa – i.e. the consonant of the reduced syllable and the consonant of the following syllable – are stops, then both will be voiced
	"promise": [ɡədḭ] ကတိ ← /ka̰/ + /tḭ/
	*/
	//https://en.wikipedia.org/wiki/Burmese_alphabet and https://en.wikipedia.org/wiki/Burmese_language#Consonants
	const groupedStopConsonants = {
		//unaspirated -> voiced , aspirated/nasal
		"က": { voiced: "ဂ", nasal: "င" },
		"စ": { voiced: "ဇ", nasal: "ဉ" }, //nasal ဉ or ည(when not a medial syllable)
		"ဋ": { voiced: "ဍ", nasal: "ဏ" },
		"တ": { voiced: "ဒ", nasal: "န" },
		"ပ": { voiced: "ဗ", nasal: "မ" },
		//aspirated
		"ခ": { voiced: "ဃ", nasal: "င" },
		"ဆ": { voiced: "ဈ", nasal: "ဉ" }, //nasal ဉ or ည(when not a medial syllable)
		"ဌ": { voiced: "ဎ", nasal: "ဏ" },
		"ထ": { voiced: "ဓ", nasal: "န" },
		"ဖ": { voiced: "ဘ", nasal: "မ" },

		//exception
		"သ": { voiced: "ဒ" }, //https://en.wikipedia.org/wiki/Burmese_phonology#Burmese_voicing_sandhi //, nasal: "န"  //?doublecheck has nasalise consonant exception?
		//Additionally ⟨သ⟩ can become voiced under the same conditions, however this is purely allophonic since the voiced [ɾ̪~ð̆~d̪̆] phone does not exist in any other context.
	}
	//CONSONANT_AFTER_ROMAN_VOWEL_TYPE stop -> voiced
	const groupedStopConsonantsValue = Object.values(groupedStopConsonants);
	//The phonemes /p, pʰ, b, t, tʰ, d/, when following the nasalized final /ɰ̃/, can become /m/ in compound words: 
	//ခင် ဗျာ။  compound CONSONANT_END_OF_SYLLABLE_N_TYPE(nasalized final) /ɰ̃/ + next groupedStopConsonants(voiced) "ဗ" -> minor syllable + consonant change voiced -> /m/ nasal

	/*
	ပ 	/p/; ဖ 	/pʰ/; ဗ 	/b/; တ 	/t/;ဋ 	/t/, ထ 	/tʰ/, ဌ 	/tʰ/
	nasalized Final = all end of syllable ending on ɴ /ɰ̃/ -> Consonant can become /m/ 
	to double check "ေါင်": "aun",
	*/
	/*
	မ ... ဘူး enclosure. မ is not associated syllable for vowel front check
	မ ပူ ဘူး လား။ (Ma-pu-bu-la) not (ma bu bu la)
	*/
	//ဘုရား buya -> phaya
	const voicedStopWithoutGroup = [
		"ရ",
	]

	//Copy from BGN //only keys for consonant after vowel check needed
	const VOWEL_CHARACTERS_CONSONANT_AFTER_VOWEL = {
		Independent: {
			"ဧ": "e",
			"၏": "e",
			"ဣ": "i",
			"ဤ": "i",
			"ဥ": "u",
			"ဦ": "u",
			"ဩ": "aw",
			"ဩော": "aw",
			"ဪ": "aw",

		},
		Dependent: {
			"ာ": "a",
			"ါ": "a",
			"ေ": "e",
			"ဲ": "è",
			"ိ": "i",
			"ီ": "i",
			"ို": "o",
			"ု": "u",
			"ူ": "u",
			"ော": "aw",
			"ော်": "aw",
			"ေါ": "aw",
			"ေါ်": "aw",
			"္": "",//hack special character stacked character can only follow after a consonant
		}
	}

	const CONSONANT_END_OF_SYLLABLE_CONSONANTAFTERVOWEL_CHANGE = {
		//vv below vv different from BGN? additional for readability
		"ေါက်": "au",
		"ေါင်": "aun",
		"ဉ္စ": "yinsa",//exception stacked character: example case ပဉ္စမ (pyin); not real final character

		//unicode "hack"/quickfix needs to be done differently
		//"ွှတ်":"hut", // ွှ needs to be written circle comma rest endofsyllable -> should be h-C-ut not C-hut
		//^^ atop ^^  different from BGN? additional for readability


		"င်": "in",
		"ိင်": "ein", //missing in BGN? example name [Theingi] သိင်္ဂီ  - သ +  ိင် + ဂ +ီ found in https://att-astrec.nict.go.jp/member/ding/PACLING2017-MY.pdf
		"ိုင်": "aing",
		"ောင်": "aung",


		//more often e?
		"ည်": "|i|", //!note 10: ည် is romanized i, in or e, depending on pronunciation.  A reference source should be consulted in case of uncertainty. 



		//6 / 8
		"န်": "an",
		"ိန်": "ein",
		"ုန်": "ôn",
		"ွန်": "un",
		"ဝန်": "wun",// "false" endOfSyllable? C+EndSyllable combination

		"မ်": "an",
		"ိမ်": "ein",
		"ုမ်": "ôn",
		"ွမ်": "un",
		"ဝမ်": "wun",// "false" endOfSyllable? C+EndSyllable combination


		"ယ်": "è",
		// "ဥ်": "in",	//ဥ (u)+်
		"ဉ်": "in", //ဉ (nya)+်	//added. one or the other is needed and not in BGN table

		"ဝံ": "wun",
		"ံ": "an",
		"ိံ": "ein",
		"ုံ": "ôn",

	}

	const CONSONANT_END_OF_SYLLABLE_N_TYPE = {
		"င်": "ɪ̀ɴ", //kinza special case? ခင်ဗျာ။ 
		"ိင်": "ein", //missing in BGN? example name [Theingi] သိင်္ဂီ  - သ +  ိင် + ဂ +ီ found in https://att-astrec.nict.go.jp/member/ding/PACLING2017-MY.pdf
		"ိုင်": "àɪɴ",
		"ောင်": "àʊɴ",
		//6 / 8
		"န်": "àɴ",
		"ိန်": "èɪɴ",
		"ုန်": "òʊɴ",
		"ွန်": "ʊ̀ɴ",
		"ဝန်": "wʊ̀ɴ",// "false" endOfSyllable? C+EndSyllable combination

		"မ်": "àɴ",
		"ိမ်": "èɪɴ",
		"ုမ်": "òʊɴ",
		"ွမ်": "ʊ̀ɴ",
		"ဝမ်": "wʊ̀ɴ",// "false" endOfSyllable? C+EndSyllable combination

		//	"ဥ်": "ɪ̀ɴ",	//ဥ (u)+်
		"ဉ်": "ɪ̀ɴ", //ဉ (nya)+်	//added. one or the other is needed and not in BGN table

		"ဝံ": "wʊ̀ɴ",
		"ံ": "àɴ",
		"ိံ": "èɪɴ",
		"ုံ": "òʊɴ",

		//"false" EndOfSyllable
		//https://en.wiktionary.org/wiki/Wiktionary:Burmese_transliteration
		//In sequences with ွ or ဝ before တ်, န်, ပ်, မ် or together with ံ, the vowel pronounced is /u/ rather than the expected /(w)a/
		"ွံ": "ʊ̀ɴ",

		//Tones not in vowel list
		"န်း": "áɴ",
		"န့်": "a̰ɴ",
		// "ါး":"əʔ", //double check

		//special combination need to check BGN and others example အဘိဓာန် [dictionary] /əbídæɴ/
		"ာန်": "æɴ",
	}

	//const REGEX_CONSONANT = new RegExp(Object.keys(CONSONANT).join("|"));	
	//const REGEX_CONSONANT_CHARACTER_COMBINATIONS = new RegExp(Object.keys(CONSONANT_CHARACTER_COMBINATIONS).sort((a, b) => b.length - a.length).join("|"));
	//const REGEX_CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR = new RegExp(Object.keys(CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR).sort((a, b) => b.length - a.length).join("|"));

	//https://en.wikipedia.org/wiki/Burmese_alphabet
	//Diacritics and symbols

	//.sort((a, b) => b.length - a.length) -> sorts array longest to shortest string ိ , ို
	//const REGEX_VOWELINDEPENDENT = new RegExp(Object.keys(VOWEL_CHARACTERS["Independent"]).sort((a, b) => b.length - a.length).join("|"));
	//const REGEX_VOWELDEPENDENT = new RegExp(Object.keys(VOWEL_CHARACTERS["Dependent"]).sort((a, b) => b.length - a.length).join("|"));

	//CONSONANT CHARACTERS WITH END-OF-SYLLABLE MARKS IN SYLLABLES 
	//CONTAINING A MEDIAL VOWEL AND A FINAL CONSONANT 
	//https://en.wikipedia.org/wiki/Burmese_alphabet
	//	Syllable rhymes

	//const REGEX_CONSONANT_END_OF_SYLLABLE_ALL = new RegExp(Object.keys(CONSONANT_END_OF_SYLLABLE).sort((a, b) => b.length - a.length).join("|"),"g");

	//"ည်":	"|i|", //!note 10: ည် is romanized i, in or e, depending on pronunciation.  A reference source should be consulted in case of uncertainty. 
	const CONSONANT_END_OF_SYLLABLE_APPROXIMATION = {
		"လ, န, ရှ, ဖ, တ, မ, ထ, ခ, ချ": "e",
		"ပ, ဗ, က, စ": "i",
		//"သ, ရ": "|i|",
	}

	const falseEndOfSyllable = ["ဝန်", "ဝမ်", "ဝတ်", "ဝံ"];
	const openSyllableList = ["ါ", "ါး", "ါး", //https://en.wikipedia.org/wiki/Burmese_phonology#Vowels_in_open_syllables
		"ဲ့", "ယ်", "ဲ",
		"ေါ့", "ေါ်", "ေါ",
		"ွ", "ွါ", "ွါး",
		"ွဲ့", "ွယ်", "ွဲ",
		"ိ", "ီ", "ီး",
		"ေ့", "ေ", "ေး",
		"ု", "ူ", "ူး",
		"ွေ့", "ွေ", "ွေး",
		"ို့", "ို", "ိုး"];

	//https://www.wikiwand.com/en/Burmese_language#/Syllable_structure
	//"It has only a simple (C) onset (no glide after the consonant)"
	const openSyllableWithoutGlideList = ["ါ", "ါး", "ါး", //https://en.wikipedia.org/wiki/Burmese_phonology#Vowels_in_open_syllables
		"ဲ့", "ယ်", "ဲ",
		"ေါ့", "ေါ်", "ေါ",
		"ိ", "ီ", "ီး",
		"ေ့", "ေ", "ေး",
		"ု", "ူ", "ူး",
		"ို့", "ို", "ိုး"];

	//for minor syllable test https://en.wiktionary.org/wiki/Wiktionary:Burmese_transliteration#Minor_syllables
	//assumption all ending on ʔ are ending without tone?
	const endOfSyllableWithoutTone = [
		"က်", "စ်", "တ်", "ပ်",
		" ိတ်", "ိပ်", "ုတ်", "ုပ်",
		"ောက်", "ိာက်", "ိုက်",
		"ွတ်", "ွပ်",
		"ဝတ်", "ဝပ်"
	]

	const NUMERALS = {
		"၀": "0",
		"၁": "1",
		"၂": "2",
		"၃": "3",
		"၄": "4",
		"၅": "5",
		"၆": "6",
		"၇": "7",
		"၈": "8",
		"၉": "9",
	}
	/*
	100	ရာ
	1 000	ထောင်
	10 000		သောင်း
	100 000		သိန်း
	1 000 000		သန်း
	10 000 000		ကုဋေ
	11 တစ်ဆယ် တစ်	thit.se.thit or se.thit first 1 omitable
	25000	hna thoun nga taun
	600500 chaut thein nga ya
	
	
	*/
	const NUMBER_ROMANIZATION = {
		"0": "သုည",
		"1": "တစ်",
		"2": "နှစ်",
		"3": "သုံး",
		"4": "လေး",
		"5": "ငါး",
		"6": "ခြောက်",
		"7": "ခုနှစ်",
		"8": "ရှစ်",
		"9": "ကိုး"
	}
	const NUMBER_ROMANIZATION_VALUES = Object.values(NUMBER_ROMANIZATION);

	const NUMBER_POSITION_ROMANIZATION = {
		"2": "ဆယ်",
		"3": "ရာ",
		"4": "ထောင်",
		"5": "သောင်း",
		"6": "သိန်း",
		"7": "သန်း",
		"8": "ကုဋေ",
	}
	const NUMBER_POSITION_ROMANIZATION_VALUES = Object.values(NUMBER_POSITION_ROMANIZATION);

	//how to add from table sheet NOUNS
	const NOUNS_old = [
		"မနက်", //(manet)morning
		"ည", //(nya)night
		"ရွာ", //village
		"မြို့", //town
		"အုပ်", //(ôk) [book]
		"ပန်းကန်", //(/bəɡæɴ/) [plate]
		"စေ့", //(sé)[seed]
		//"တောင့်", (Taung) [bar of]?
		"ပုဒ်", //(/pouʔ/) [phrase / speech acts]
	]
	//additional common/possible classifiers[nouns]?
	//Nouns+လုံး(lôn) whole noun
	const UNITOFMEASUREMENTS = [
		"ကျပ်",	//(kyat)
		"ဒေါ်လာ",	//(dollar)
		"ယူရို", //(euro)
		"နာရီ", //(nayi) [hour/time]
		"မိနစ်", //minit [minute]
		"ပတ်", //(pat) [week]
		"လ", //(la) [month]
		"နှစ်", //(hnit) [year]
		"နေ့", //(ne) [day]
		"နေရာ", //(neya) [place/situation]	
		"မျိုး", //(Myo) [type/kind of]
		"ဂါလန်", //gallons
		"မိုင်", //miles
		"ဘူး", //packet (of plasters, cigarettes), jar (of coffee), tube (of toothpaste)
		"လိပ်", // roll (of film, toilet paper)
		"ပိဿာ", //viss  (equivalent to 3.6 lb or 1.6kg)
		"ပုလင်း", //(/pəlìɴ/) [bottle]
		//TODO CL detection combination of tablesheet isNOUN + isCLASSIFIERWORDS
		//common nounCL combination
		/*
		"ညလုံး", //(nyaLôn) [allnight]
		"မနက်လုံး", //manetLôn [allMorning]
		"ရွာလုံး", //(ywaLôn) [whole village]
		"မြို့လုံး" //(myoLôn) [whole town]
		*/
	]
	//https://en.wikipedia.org/wiki/Burmese_numerical_classifiers
	const CLASSIFIERWORDS = [
		"ကောင်",	//animal
		"ပါး",		//sacred persons (such as Buddhist monks and nuns) 
		"ယောက်",	//persons (general classifier) 
		"ရှဉ်း",		//pairs of draught cattle 
		"ဦး",			// 	people, monks and nuns 
		"ကုံး",			// 	garlands, necklaces, stringed items 
		"ကျိုက်",		// 	draughts gulped down 
		"ကျိပ်",		// 	items in 'tens' 
		"ခါ",			// 	number of times 
		"ခု",			// 	items (general classifier) 
		"ခက်",			// 	branches, sprays of flowers 
		"ခင်",			//skeins of wool or cotton 
		"ခေါင်း",		// 	skeins of yarn 
		"ခိုင်",	//bunch of flowers, fruits
		"ခေါက်",		// 	trips 
		"ခွေ",			// 	rings, coils 
		"ခွန်း",		//words 
		"ချီ",			// performances or shows 
		"ချိုး",		//components of a ratio 
		"ချက်",
		"ချောင်း",	//stick, rod like object(pencil, finger etc)
		"ချပ်",	//flat items (like tables, slice of bread)
		"ခြည်",
		"စီး",
		"စည်း",
		"စင်း",
		"စောင်",
		"စုံ",	//a set/pair
		"ဆ",
		"ဆူ",
		"ဆောင်",
		"ဆိုင်း",
		"တန်",
		"တန့်",
		"တုတ်",
		"တွဲ",
		"တွက်",
		"ထပ်",
		"ထမ်း",
		"ထောက်",
		"ထည်",	//clothing items
		"ထုပ်",	//pack/package
		"ထုံး",
		"ထွာ",
		"ထွေ",
		"နပ်",
		"ပါး",
		"ပေါ",
		"ပေါက်",
		"ပိုင်",
		"ပိုင်း",
		"ပတ်",
		"ပင်",
		"ပိုဒ်",
		"ပုဒ်",
		"ပုံ",
		"ပျစ်",
		"ပြူး",
		"ပြိုက်",
		"ပြင်",
		"ပြာ",
		"ပွဲ",
		"ဖီး",	// hands of bananas or plantains
		"ဖောင်",
		"ဖုံ",
		"ဖန်",
		"ဖြာ",
		"ဖွာ",
		"မည်",
		"မြူ",
		"မျက်",
		"မှုတ်",
		"မြွှာ",
		"မြှောင့်",
		"ယာ",
		"ယှက်",
		"ရန်",
		"ရပ်",
		"ရေး",
		"ရေစီး",
		"ရိုက်",
		"ရစ်",
		"ရွက်",	//leaf
		"လား",
		"လီ",
		"လေး",
		"လော",
		"လက်",
		"လုတ်",
		"လုံး",
		"လှိုင်း",
		"လွှာ",
		"ဝါး",
		"သား",
		"သုတ်",
		"သွယ်",
		"ခွက်",	//cup/glass
		//additional common/possible classifiers[nouns]?
		...UNITOFMEASUREMENTS,
		//...NOUNS,//better detection would be NOUNS + CL
	]
	//const REGEX_CLASSIFIERWORDS = new RegExp(Object.keys(CLASSIFIERWORDS).sort((a, b) => b.length - a.length).join("|"));

	/* စ် it -> a why not with 8?
	*/
	const END_OF_SYLLABLE_NUMBER_EXCEPTIONS = {
		/* 1,2,7 စ် it -> a
		 */
		"စ်": "a",
	}






	//const syllableAndTonemarkCriteria = '(['+'"'+endSyllableKeyArray.join('"|"')+'"'+"]+["+toneSentenceMarkArray.join("")+"]*"+")";


	/*
	- is no final + has no top/bottom vowel/endofSyllable
		+ has previous Consonant without vowel/endofsyllable
		+ has following Consonant(word boundary)?
	https://r12a.github.io/scripts/myanmar/ consonant-repetition
	- A repeated သ [U+101E MYANMAR LETTER SA] can be represented using ဿ [U+103F MYANMAR LETTER GREAT SA].
		In modern Burmese, ဿ appears within words, whereas သ်သ is used across word boundaries.
	*/
	const SIMPLIFIED_DOUBLE_STACKED = {
		"ည": "ြဉ်ည", //ပညာရေး -> ပြဥ်ညာရေး
		"ဿ": "သ်သ"
	}

	//possible mistaken inputs
	const mistakenCharacter = { //bigger to smaller matches
		"ဦ": "ဦ",		//shiftU shiftD -> shiftM
		//"စျ":"ဈ", 		// ps -> shiftQ can exists for stacked characters
		"သြော်": "ဪ", // ojamf -> shift+
		"သြ": "ဩ", 		//oj -> +
		"ပာ": "ဟ", 		//zm -> ü
		//"ဝာ": "တ", 		//shiftW m -> w	 //maybe possible as stacked character. မဇ္စျိမ[center] properly written မဇ္ဈိမ
		"၁ာ": "ဘ",		//1m -> b
		"ဒခ": "အ", 	  //shiftK c -> t
		//"သ၁": "ဿ", 	//o1 -> shiftO //stacked character possibility
		"ဌ်": "၌", 		//shiftX f -> shiftZ
		//"ှု":"ူ",		//can be possible ရှုပ် (shôk) [be messy, confused]
		"့်": "့်", 		//vowel tonemark sequencing
		"်ျ": "ျ်",	//consecutive sequencing	် last character
		//"ခာ":"ဆ",		//cm -> q  //can be possible when used as stacked character
		"ဥ်": "ဉ်",	// shiftU(U)+f ->  shiftN(Nya)+f  //Vowel not legal final/double consonant character
		//https://en.wikipedia.org/wiki/MLC_Transcription_System - Abbreviated syllables
		"၍": "ရုယ်",
		"၌": "နှိုက်",
		"၎င်း": "လေကောင်",
		//https://en.wikipedia.org/wiki/Burmese_alphabet
		//	"သ္သ":"ဿ",
		"ွှ": "ှွ",//UnicodeHack for easier endOfSyllable processing
		//"ညာ":"ဉ်ဉာ" //stacked character hack only if no previous consonant
		//"၏":"ဧအ်", //in BGN vowels
	}


	function processSpecialStackedCharacters(text, type = TypeEnums.BGN) {
		//let result = text;
		//example ပိဿာ -> ပိသ္သာ
		//In modern Burmese, ဿ appears within words, whereas သ်သ is used across word boundaries
		//const regex = /ဿ/gi
		const replaceCharacter = "သ္သ"
		const regex = new RegExp("ဿ", 'gi');
		const matches = text.matchAll(regex)
		let newText = "";
		console.group("processSpecialStackedCharacters")
		let previousIndex = 0;
		for (const match of matches) {
			console.log(match.index)
			const textUntilSpecialCharacter = text.slice(previousIndex, match.index + 1)
			newText += text.slice(previousIndex, match.index);
			let syllableRest = "";
			const textAfterCharacter = text.slice(match.index + 1);
			console.log("textUntilSpecialCharacter: " + textUntilSpecialCharacter)
			console.log("textAfterCharacter: " + textAfterCharacter)
			const vowelMatchAfterCharacter = textAfterCharacter.match(REGEX_STARTSWITH_VOWELDEPENDENT[type]);
			let vowelLength = 0;
			const hasVowelMatch = vowelMatchAfterCharacter && vowelMatchAfterCharacter[0];
			if (hasVowelMatch) {
				syllableRest += hasVowelMatch;
				vowelLength = hasVowelMatch.length;
			}
			console.log("vowelMatchAfterCharacter: " + vowelMatchAfterCharacter + ", hasVowelMatch: " + hasVowelMatch + ", vowelLength: " + vowelLength)
			const textAfterVowel = textAfterCharacter.slice(vowelLength)
			const endOfSyllable = textAfterVowel.match(REGEX_STARTSWITH_CONSONANT_END_OF_SYLLABLE[type]);
			const hasEndOfSyllableMatch = endOfSyllable && endOfSyllable[0];

			let textAfterEndOfSyllable = textAfterVowel;
			if (hasEndOfSyllableMatch) {
				syllableRest += hasEndOfSyllableMatch;
				textAfterEndOfSyllable = textAfterVowel.slice(hasEndOfSyllableMatch.length)
			}
			console.log("hasEndOfSyllableMatch: " + hasEndOfSyllableMatch)
			const toneSentenceMarkMatch = textAfterEndOfSyllable.match(REGEX_STARTSWITH_TONE_SENTENCE_MARK[type]);
			const hasToneSentenceMark = toneSentenceMarkMatch && toneSentenceMarkMatch[0]
			if (hasToneSentenceMark) {
				syllableRest += hasToneSentenceMark;
			}
			console.log("hasToneSentenceMark: " + hasToneSentenceMark + ", syllableRest: " + syllableRest + ", syllableRestLength: " + syllableRest.length)
			//const endOfSyllable  REGEX_ENDOFSYLLABLE_AND_TONEMARKS_TYPE[type]


			//const hasVowelAfterSpecialCharacter = REGEX_STARTSWITH_VOWELDEPENDENT
			const index = 1;
			const testNext = true;
			let hasMatch = false;
			//for (let i = 1; i < textUntilSpecialCharacter.length; i++) {

			while (testNext) {
				const wordToCheck = textUntilSpecialCharacter.slice(-index) + syllableRest;
				console.log("wordToCheck: " + wordToCheck)
				const endsWithSpecialCharacter = matchWordVariations(wordToCheck, NOUNS, -1)
				if (endsWithSpecialCharacter) {
					console.log("endsWithSpecialCharacter - wordToCheck: " + wordToCheck)
					if (index < textUntilSpecialCharacter.length)
						index++;
					else
						testNext = false;
				} else testNext = false;
			}
			const wordToCheck = textUntilSpecialCharacter.slice(-index) + syllableRest;
			const endsWithSpecialCharacterExactMatch = matchWordVariations(wordToCheck, NOUNS, 0) //NOUNS.includes(wordToCheck); 
			console.log("hasExact special character word match?: " + endsWithSpecialCharacterExactMatch + ", wordToCheck: " + wordToCheck)

			if (endsWithSpecialCharacterExactMatch)
				newText += "သ္သ";
			else
				newText += "ဿ";
			previousIndex = match.index + 1;
		}
		newText += text.slice(previousIndex);
		console.log("orig text: " + text)
		console.log("newText: " + newText)
		console.groupEnd("processSpecialStackedCharacters")
		return newText;
	}

	/**
	 * next has stackedCharacter symbol or next is specialStackedCharacter (ဿ)
	 * check for endOfSyllableMerge
	 *
	 * @param {*} currentSyllable
	 * @param {*} nextSyllable
	 * @param {*} [type=TypeEnums.BGN]
	 * @param {boolean} [spoken=true]
	 */
	function getConsonantOfStackedCharacter(currentSyllable, nextSyllable, type = TypeEnums.BGN, spoken = true) {
		//ပိဿာ  spoken ပိသ်သာ
		//ပိန္နဲသီး spoken ပိန်နဲသီ


	}

	/**
	 *
	 * @param {string} myanmartext
	 * @returns {string} myanmarText without stackedCharacters
	 */
	function preprocessStackedCharacters(text, type = TypeEnums.BGN, spoken = true) {
		//const REGEX_ENDSWITH_VOWELINDEPENDENT_TYPE = new RegExp("(" + Object.keys(VOWEL_CHARACTERS_TYPE["Independent"]).sort((a, b) => b.length - a.length).join("|") + ")$");
		const REGEX_ENDSWITH_VOWELDEPENDENT = new RegExp("(" + Object.keys(VOWEL_CHARACTERS[type]["Dependent"]).sort((a, b) => b.length - a.length).join("|") + ")$");

		//colloquial burmese "stacked consonants" p.178(p.155)
		const DEBUG = true;
		let foundStackedIndexes = [];
		DEBUG && console.groupCollapsed("preprocessStackedCharacters")
		//const specialStackedCharacters = processSpecialStackedCharacters(text, type);
		const textToProcess = text;
		const stackedMatches = returnStackedCharacterMatches(textToProcess);
		/*
			let stackedConsonant = (targetConsonantIndex-1 >=0)?syllableArray[s][targetConsonantIndex-1]:null;
						let stackedLowerConsonant = (targetConsonantIndex+1 >=0)?syllableArray[s][targetConsonantIndex+1]:null;
						let stackedIndexOnConsonant = (stackedConsonant == consonant);
						let stackedVowel = (targetConsonantIndex+2 <syllableArray[s].length) ?syllableArray[s][targetConsonantIndex+2]:null;
		*/
		let processedIndex = 0;
		let newText = "";
		let result = { newText: "", stackedIndexes: [] };
		DEBUG && console.log("stackedCharacterText: " + textToProcess)
		const upperVowelList = ["ဲ", "ိ", "ီ"]
		if (stackedMatches) {
			DEBUG && console.log(stackedMatches);
			let previousMatch = -1;
			let addFinalCharacterToIndex = 0;
			for (const match of stackedMatches) {

				DEBUG && console.log("match: '" + match[0] + "', returnStackedCharacterMatche Index: " + match.index);
				let stackedConsonantKinzi = textToProcess[match.index - 2]
				let stackedConsonant = textToProcess[match.index - 1];
				let stackedLowerConsonant = textToProcess[match.index + 1];
				let stackedVowel = textToProcess[match.index + 2];
				let isStackedVowel = upperVowelList.includes(stackedVowel);
				let isVowelCharacter = Object.keys(VOWEL_CHARACTERS[type].Dependent).includes(stackedVowel); //isVowel(stackedVowel, type);


				let latinStackedLowerConsonant = CONSONANT[type][stackedLowerConsonant];
				let potentialEndOfSyllable = stackedConsonant + "်";
				//potentialEndOfSyllable = potentialEndOfSyllable.replace("္","")
				if (stackedConsonantKinzi + stackedConsonant == "င်") //kinzi
				{
					potentialEndOfSyllable = stackedConsonantKinzi + stackedConsonant;
					stackedConsonant = "";
				}
				let latinStackedConsonant = CONSONANT[type][stackedConsonant] ? CONSONANT[type][stackedConsonant] : "";
				DEBUG && console.log("latinStackedConsonant: " + latinStackedConsonant +
					", stackedConsonant: " + stackedConsonant +
					",latinStackedLowerConsonant: " + latinStackedLowerConsonant +
					", stackedLowerConsonant:" + stackedLowerConsonant)
				//console.log(stackedConsonantKinzistackedConsonantKinzi+"");
				let isEndOfSyllable = false;
				DEBUG && console.log("potentialEndOfSyllable to test: " + potentialEndOfSyllable)
				/*
				for(let end_syllable in CONSONANT_END_OF_SYLLABLE){				
					if(potentialEndOfSyllable == end_syllable)
					{
						isEndOfSyllable = true;
						console.log("end_syllable: " + end_syllable);
						break;
					}
				}
				*/
				//TODO check previous Vowel + new final new EndOfSyllable?
				const textAfterStackedIndex = previousMatch >= 0 ? previousMatch : 0;

				console.log("characters before processedIndex: " + textToProcess.slice(textAfterStackedIndex, processedIndex))
				console.log("match.index: " + match.index)
				const textBeforeMatchIndex = textToProcess.slice(textAfterStackedIndex, match.index - 1);
				console.log("characters before match.index: " + textBeforeMatchIndex)
				const matchVowelBeforeStacked = textBeforeMatchIndex.match(REGEX_ENDSWITH_VOWELDEPENDENT);
				console.log("matchVowelBeforeStacked: " + matchVowelBeforeStacked);

				let previousVowel = "";
				if (matchVowelBeforeStacked) {
					previousVowel = matchVowelBeforeStacked[0];
				} else {

				}
				const potentialEndOfSyllableToTest = previousVowel + potentialEndOfSyllable
				const matchPotentialEndSyllable = potentialEndOfSyllableToTest.match(REGEX_STARTSWITH_CONSONANT_END_OF_SYLLABLE[type]);
				let isKinza = false;
				console.log("potentialEndOfSyllableToTest: " + potentialEndOfSyllableToTest)
				console.log("matchPotentialEndSyllable: " + matchPotentialEndSyllable)
				if (matchPotentialEndSyllable) {
					isEndOfSyllable = true;
					DEBUG && console.log("starts with matchPotentialEndSyllable: " + matchPotentialEndSyllable[0]);
					if (matchPotentialEndSyllable[0] == "င်")
						isKinza = true;
				}

				let exceptionTest = stackedConsonant + "္" + stackedLowerConsonant;
				const exceptionList = ["ဉ္စ", "လ်"]
				let isException = ("ဉ္စ" == exceptionTest);
				DEBUG && console.log("exceptionTest: " + exceptionTest + ', is "ဉ္စ": ' + isException)
				DEBUG && console.log("isEndOfSyllable: " + isEndOfSyllable + ", isStackedVowel:" + isStackedVowel)
				//if((isEndOfSyllable || isStackedVowel) && !isException)
				let nonMuteConsonant = (stackedConsonant == "လ") //လ်


				let isFinalException = isException //|| (!isEndOfSyllable )
				const isFinalSyllable = ((isEndOfSyllable && !nonMuteConsonant)) //&& isStackedVowel
				const isWithoutVowel = (stackedLowerConsonant && !nonMuteConsonant) // && !isStackedVowel

				const couldBeConsonantDouble = (stackedConsonant != "" && stackedLowerConsonant != "")
				//example မဇ္ဈိမ consonantDouble vs သ္မီး abbreviation
				//const isLatinDouble = (latinStackedConsonant == latinStackedLowerConsonant) //example မဇ္ဈိမ
				let isFinal = (isFinalSyllable)// || isWithoutVowel || isLatinDouble //vs သ္မီး  //
				//||(stackedConsonant==stackedLowerConsonant)  //လ္လ ?

				DEBUG && console.log("isFinal: " + isFinal +
					", isFinalException: " + isFinalException +
					", isFinalSyllable: " + isFinalSyllable +
					", isWithoutVowel: " + isWithoutVowel +
					", couldBeConsonantDouble: " + couldBeConsonantDouble +
					", isKinza: " + isKinza)
				DEBUG && console.log("isConsonantDouble= stackedConsonant: " + stackedConsonant + ", stackedLowerConsonant: " + stackedLowerConsonant +
					", latinStackedConsonant: " + latinStackedConsonant + ", latinStackedLowerConsonant: " + latinStackedLowerConsonant)
				DEBUG && console.log("isFinalSyllable= isEndOfSyllable: " + isEndOfSyllable + ", nonMuteConsonant: " + nonMuteConsonant + ", isStackedVowel: " + isStackedVowel);

				//if ((isFinal && !isFinalException && !isKinza )|| !isEndOfSyllable) //stacked Character
				const firstTest = (isFinalSyllable || isFinalException);

				const abbreviation = (!isFinalSyllable &&
					(!isEndOfSyllable)// no previous Consonant  //ပိဿာသ္မီး  //textBeforeMatchIndex.length == 0||
				)
				console.log("firstTest final: " + firstTest + ", abbreviation: " + abbreviation)
				console.log("processedIndex: " + processedIndex + ", text.length: " + textToProcess.length + ", match.index: " + match.index)
				const textSliceStackedIndexBracked = textToProcess.slice(processedIndex, match.index);
				console.log("textSliceStackedIndexBracked: " + textSliceStackedIndexBracked)

				if (!isKinza && (firstTest || !abbreviation))//&& previousVowel != ""
				{
					//ဥယ္ယာဉ် becomes ဥယျာဉ်
					//လက္ယာ becomes လက်ျာ
					if (!spoken ||
						(spoken && stackedLowerConsonant == "ယ" && stackedConsonant != "ယ") || (spoken && !abbreviation)
					)	//ဒုက္ခ not abbreviation
					{
						newText += textSliceStackedIndexBracked + "်";
						addFinalCharacterToIndex++;
					}

					else
						newText += textSliceStackedIndexBracked;

					//foundStackedIndexes.push(newText.length);

				}
				else //vs stacked abbreviation
					newText += textSliceStackedIndexBracked; //သ္မီး decompresses to သမီး abbreviation  //no need to mark stacked Position
				//if (!abbreviation)
				foundStackedIndexes.push(match.index + addFinalCharacterToIndex);  //မင်္ဂလာပါ။ 

				console.log("stackedLowerConsonant: " + stackedLowerConsonant)
				DEBUG && console.log("newText before LowerConsonant: " + newText)
				if (stackedLowerConsonant) {
					if (isException)
						newText += "္";
					console.log("spoken: " + spoken + ", stackedLowerConsonant: " + stackedLowerConsonant);

					if (spoken && stackedLowerConsonant == "ယ") {
						console.log("Simplified Double Stacked word exception -ယ, isEndOfSyllable: " + isEndOfSyllable)
						if (newText[newText.length - 1] == "်") {
							newText = newText.slice(0, newText.length - 1) + "ျ"
						}
						else
							newText += "ျ";
						console.log("newText ယ: " + newText)
						if (isEndOfSyllable && stackedLowerConsonant != stackedConsonant) //not ယ္ယ
							newText += "်"
					}
					else
						newText += stackedLowerConsonant;//text[match.index + 1];
					processedIndex = match.index + 2;
				}
				DEBUG && console.log("newText: " + newText)
				if (stackedVowel) {
					newText += stackedVowel//text[match.index + 2];
					processedIndex = match.index + 3;
				}
				if (!stackedLowerConsonant && !stackedVowel) {
					processedIndex = match.index + 1;
				}

				DEBUG && console.log("latinConsonant: " + latinStackedConsonant +
					" -stackedConsonant: " + stackedConsonant +
					", isEndOfSyllable: " + isEndOfSyllable +
					", latinStackedLowerConsonant: " + latinStackedLowerConsonant + " -stackedLowerConsonant: " + stackedLowerConsonant +
					", stackedVowel: " + stackedVowel + " isUpperVowel: " + isStackedVowel + ", isVowel: " + isVowelCharacter);
				previousMatch = match.index;
			}
			newText += textToProcess.slice(processedIndex)

			DEBUG && console.log("foundStackedIndexes: " + foundStackedIndexes +
				", indexLength: " + foundStackedIndexes.length);
		}
		else
			newText = textToProcess;

		result.newText = newText;
		result.stackedIndexes = foundStackedIndexes;
		DEBUG && console.log(newText)
		DEBUG && console.groupEnd("preprocessStackedCharacters")
		return result; //newText;
	}
	/**
	 * - Split MyanmarNumer and reprounce as myanmar text
	 * - romanize myanmarText
	 * @param {*} myanmarText
	 * @param {boolean} [isNumber=false]
	 * @param {boolean} [nextIsCL=false]
	 * @returns
	 */
	function processMyanmarText(myanmarText, nextIsCL = false, type = TypeEnums.BGN, spoken = true, sandhi = true) {
		const DEBUG = true;
		const MYANMARNUMBERS = /([၁၂၃၄၅၆၇၈၉၀]+)/g
		const BRACKETSANDDIGITS = /([\(\)\[\]\d]+)/g
		let processedText = "";

		const matches = myanmarText.split(MYANMARNUMBERS);
		let isMyanmarNumber = false;
		DEBUG && console.group("processMyanmarText: " + myanmarText + ", nextIsCL: " + nextIsCL + ", spoken: " + spoken + ", sandhi: " + sandhi)
		DEBUG && console.log(matches)
		let hasPreviousNumberText = "";
		for (let i = 0; i < matches.length; i++) {
			isMyanmarNumber = (i % 2 == 1);
			const selectedText = matches[i];
			if (selectedText && selectedText.length > 0) {
				const hasNextMyanmarText = matches[i + 1];
				let nextMyanmarTextIsCL = false;
				if (hasNextMyanmarText) {

					nextMyanmarTextIsCL = infrontOfClassifierword(hasNextMyanmarText, type)
					DEBUG && console.log("hasNextMyanmarText: " + hasNextMyanmarText + ", nextMyanmarTextIsCL: " + nextMyanmarTextIsCL)
				}
				DEBUG && console.log("isMyanmarNumber: " + isMyanmarNumber + ", selectedText: " + selectedText + ", nextArrayWordIsCL: " + nextIsCL +
					", nextMyanmarTextIsCL: " + nextMyanmarTextIsCL)
				const nextTextOrSyllableIsCL = nextIsCL || nextMyanmarTextIsCL;
				if (isMyanmarNumber)// || isNumber
				{
					const pronouncedNumber = pronounceMyanmarNumberArray(selectedText, type);
					const pronouncedNumberText = pronouncedNumber.join("");
					console.log("pronouncedNumber: " + pronouncedNumber +
						", pronouncedNumberText: " + pronouncedNumberText);
					//recheck each syllable of pronouncedNumberText for infrontOfClassifierword
					//(text, isNumber = false, isNextWordCL = false, debug = false, type = TypeEnums.BGN) 
					hasPreviousNumberText = processTextBGN(pronouncedNumberText, nextTextOrSyllableIsCL, type, spoken, sandhi);
					processedText += hasPreviousNumberText;
					//processedText += romanizeText(pronouncedNumberText, false, false, true, nextTextOrSyllableIsCL);
				}
				else {
					if (selectedText && selectedText.length > 0) {

						//processedText += processTextBGN(pronouncedNumber.join(""));
						console.log("processMyanmarText: " + selectedText + ", nextIsCL: " + nextIsCL + ", nextMyanmarTextIsCL: " + nextMyanmarTextIsCL)
						//function romanizeText(text, isNextCL = false, type = TypeEnums.BGN, spoken=true) {
						processedText += romanizeText(selectedText, nextTextOrSyllableIsCL, type, spoken, sandhi, hasPreviousNumberText);
						hasPreviousNumberText = "";
					}
				}
			}

		}
		DEBUG && console.groupEnd("processMyanmarText")
		return processedText;
	}

	function processTextBGN(text, isNextWordCL = false, type = TypeEnums.BGN, spoken = true, sandhi = true) {
		let DEBUG = true;//debug;
		let processedText = "";

		if (text && text.length > 0) {
			text = replaceMistakenInputCharacters(text);
			const BRACKETENDORSEPERATOR = /([\]):-]+)/g //
			const matches = text.split(REGEXMYANMARCHARACTERS);//array (i%2==1) -> match[non match, match, ...]
			DEBUG && console.log(matches);
			for (let i = 0; i < matches.length; i++) {
				const isMyanmarText = (i % 2 == 1)
				const selectedText = matches[i];
				let isNextCL = false;

				console.log("isMyanmarText: " + isMyanmarText + ", selectedText to process: '" + selectedText + "'");
				if (isMyanmarText) {
					const hasNextLatinMatch = matches[i + 1];
					const hasNextMyanmarMatch = matches[i + 2];
					//if(hasNextLatinMatch && hasNextLatinMatch.trim()=="") isCL = true;
					console.log("hasNextLatinMatch: " + hasNextLatinMatch + ", hasNextMyanmarMatch: " + hasNextMyanmarMatch)
					if (hasNextLatinMatch) //look at next MyanmarWord and Limit inbetween Latin word to seperator
					{
						const nextLatinMatchOnlySeperatorOrBracketEnd = hasNextLatinMatch.split(BRACKETENDORSEPERATOR);
						console.group("nextLatinMatchOnlySeperatorOrBracketEnd split length:" + nextLatinMatchOnlySeperatorOrBracketEnd.length)
						console.log(nextLatinMatchOnlySeperatorOrBracketEnd)
						console.groupEnd("nextLatinMatchOnlySeperatorOrBracketEnd split")
						const onlyOneSeperator = nextLatinMatchOnlySeperatorOrBracketEnd.length == 3; //0- bracket content, 1 bracketEnd, 2 empty
						const nextLatinMatchTrimmed = hasNextLatinMatch.trim();
						const onlySpacesBetweenMyanmarWords = (nextLatinMatchTrimmed == "") || (nextLatinMatchTrimmed.length == 1);//empty or single"Seperator"Character open Bracket char [Classifier]
						console.log("onlySpacesBetweenMyanmarWords: " + onlySpacesBetweenMyanmarWords + ", onlyOneSeperator: " + onlyOneSeperator)
						if (onlySpacesBetweenMyanmarWords || onlyOneSeperator) {
							let hasSecondOperator = nextLatinMatchOnlySeperatorOrBracketEnd[2];
							if (hasSecondOperator)
								console.log("hasSecondOperator.length: " + hasSecondOperator.length)
							let emptyOrOnlySpacesAfterSeperator = true;
							if (hasSecondOperator) //can be undefined
								emptyOrOnlySpacesAfterSeperator = (hasSecondOperator.trim() == "");

							console.log(typeof hasSecondOperator + ", hasSecondOperator: '" + hasSecondOperator + "', emptyOrOnlySpacesAfterSeperator: " + emptyOrOnlySpacesAfterSeperator)
							if (onlySpacesBetweenMyanmarWords || emptyOrOnlySpacesAfterSeperator) {
								isNextCL = infrontOfClassifierword(hasNextMyanmarMatch, type);

							}
						}

					}
					console.log("processTextBGN: " + selectedText + ", isNextCL: " + isNextCL + ", hasNextMyanmarMatch: " + hasNextMyanmarMatch)
				}


				if (!isMyanmarText)
					processedText += tr(selectedText);
				else {
					console.log("processMyanmarText '" + selectedText + "' with isNextCL: " + isNextCL +
						", isNextWordCL: " + isNextWordCL);
					//function processMyanmarText(myanmarText, nextIsCL = false, type = TypeEnums.BGN, spoken = true) {
					processedText += processMyanmarText(selectedText, isNextCL || isNextWordCL, type, spoken, sandhi);
					//processedText += processMyanmarTextInefficient(selectedText, isNumber, isNextCL);
				}
			}
		}
		return processedText;
	}

	function replaceMistakenInputCharacters(text) {
		const DEBUG = false;
		DEBUG && console.group("replaceMistakenCharacters " + text)
		let result = text;
		let countReplaceables = 0;
		for (let check in mistakenCharacter) {
			//DEBUG && console.log("check for: " + check);		
			const reCheck = new RegExp(check, 'g');
			let matches = result.match(reCheck);
			if (matches)
				countReplaceables += matches.length;
			let hasReplaceableCharacter = mistakenCharacter[check];
			if (hasReplaceableCharacter) {
				//	DEBUG && console.log("hasReplaceableCharacter: "+hasReplaceableCharacter);*/
				result = result.replace(reCheck, hasReplaceableCharacter);
			}
		}
		/*
		let regexTest = "textcontentcont".replace(/cont/g,"konta");
		console.log("regexTest: " + regexTest);*/
		DEBUG && console.log("countReplaceables: " + countReplaceables + ", replaceMistakenCharacters " + result);
		DEBUG && console.groupEnd("replaceMistakenCharacters " + text);
		DEBUG && console.log("result before return of replaceMistakenInputCharacters: " + result + ", resultlength: " + result.length);
		return result;
	}

	function returnStackedCharacterMatches(text) {
		//lookup match vs exec
		const reStacked = new RegExp(/([\u1039])/, "g");//Unicode char "္"
		const matches = text.matchAll(reStacked);
		/*
		let index = -1;		
		if(matches)
		{
			index = matches.index;
			console.log("returnStackedCharacterIndex: " + matches[0] +", index: "+index);
		}*/

		return matches;
	}

	/*
	 https://github.com/myanmaropenwordnet/mya2rom/blob/master/MYA%20Syllable%20Regex.txt
	/const re = /([ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအဿ][ျြွှ]*[ံ့းွာါါိီုူေဲ ှ]*)([ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအဿ]?[့]?[့်္််း]+)?/g
	[့်း]
	
	ဘတ်စ်ကား
	သောကြာနေ့ 
	သ္မီ
	ပိန္နဲသီ
	*/
	function segment(text) {
		const stacked = "([ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအဿ][့]?([္]))"
		const consonant = "([ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအဿ][ျြွှ]*[ံ့းွာါါိီုူေဲ ှ]*)";
		const finals = "([ကခဂဃငစဆဇဈဉညဋဌဍဎဏတထဒဓနပဖဗဘမယရလဝသဟဠအဿ]?[့]?([့်း])+)?";

		const regex_str = stacked + "|" + consonant + finals;
		const re = new RegExp(regex_str, "g");

		console.group("segmentation")
		const syllables = text.replace(re, "$1$3$4 ").trim().split(" ");//$1 = stacked ,$3 = consonant, $4=finals
		/*
		const RE_Syllables = stacked +"|"+ consonant+finals
		const re2 = new RegExp(RE_Syllables, "g");
		let matchedSyllables =[];
		const matches = text.matchAll(re2);
		for(const match of matches){
			console.log(match)
			matchedSyllables.push(match[0]);
		}
		console.log(matchedSyllables);
		*/
		console.log(syllables)
		console.groupEnd("segmentation")
		//const seperatedStacked = syllables.foreach()
		return syllables;//matchedSyllables;
	}

	//isMyanmarText = false,
	function romanizeText(text, isNextCL = false, type = TypeEnums.BGN, spoken = true, sandhi = true, hasPreviousNumberText = "") {
		const DEBUG = true;
		let syllables = [];
		let romanization = [];
		//+", isMyanmarText: " + isMyanmarText
		DEBUG && console.group("romanizeText: " + text + ", type: " + type + ", keyName: " + Object.keys(TypeEnums)[type]);
		let preprocessedText = text;
		let stackedIndexes = [];

		const preprocessedTextObject = preprocessStackedCharacters(text, type, spoken);
		console.log(preprocessedTextObject);
		preprocessedText = preprocessedTextObject.newText;
		stackedIndexes = preprocessedTextObject.stackedIndexes;
		/* prepocess syllable
			vowel + tonemark switch
				can be unintentional switched: ့ ် instead of ် ့
			match and replace (give comment to text/add paragraph to note)
				စျ[ps]	ဈ[q]
				ပာ[zm]	ဟ[ü]
				ဥ[shiftU] + ီ[d] [exact match: C+V]-> ဦ[m]
		*/

		//Seperate into latin + myanmar numbers + myanmar characters
		//DEBUG && console.log(syllables);

		//syllables = seperateIntoSyllables(preprocessedText);
		const myanmarSegmentation = segment(text);
		syllables = seperateIntoSyllablesRegex(preprocessedText, type);
		let unchangedSyllables = seperateIntoSyllablesRegex(text, type);
		//const test = seperateIntoSyllablesRegex(preprocessedText);	
		DEBUG && console.log("seperateIntoSyllables: " + syllables);
		DEBUG && console.log("seperateInto unchangedSyllables: " + unchangedSyllables);
		DEBUG && console.log("myanmarSegmentation segment: " + myanmarSegmentation);
		//DEBUG && console.log("seperateIntoSyllablesRegex: " +test);


		DEBUG && console.log("romanizeText: '" + text + ", preprocessedText(without stackedCharacters): " + preprocessedText)

		console.log("romanize syllables: " + syllables + ", isNextCL: " + isNextCL + ", sandhi: " + sandhi);
		//function romanizeSyllableRegex(syllableArray, isNextSyllableCL = false, stackedIndexes = [], type = TypeEnums.BGN, spoken = true, sandhi = true) {
		romanization = romanizeSyllableRegex(syllables, unchangedSyllables, (isNextCL), stackedIndexes, type, spoken, sandhi, hasPreviousNumberText);
		//const oldNonRegexVersion = romanizeSyllable(syllables, isNumber, isSingleNumber, (isNextCL));	
		DEBUG && console.log("romanizeSyllable: " + romanization);
		//DEBUG && console.log("romanizeSyllableRegex: " + oldNonRegexVersion);
		DEBUG && console.groupEnd("romanizeText: " + text);
		return romanization.toString();
	}

	/**
	 * 
	 *
	 * @param {*} text
	 * @returns {index, value} false or object {index, character}
	 */
	function nextEndOfSyllableRegex(text, type = TypeEnums.BGN) {
		const DEBUG = false;
		let result = false;
		DEBUG && console.group("nextEndOfSyllableRegex inside: " + text)
		const match = text.match(REGEX_CONSONANT_END_OF_SYLLABLE[type]);
		if (match) {
			DEBUG && console.log(match);
			result = { index: match.index, character: match[0] };
		}
		DEBUG && console.groupEnd("nextEndOfSyllableRegex")
		return result;
	}


	/**
	 * first character after BRACKETORVARIABLE uppercase
	 *
	 * @param {*} text
	 * @returns Text uppercased first letter
	 */
	function firstCharacterToUpperCase(text) {
		if (typeof (text) == "string") {
			if (text.length > 1) {
				let i = 0;
				let char = text[i];
				//console.log("char: " + char +", of text: " + text);
				const BRACKETORVARIABLE = /([\(\|)-])/
				if (!char) console.log("undefined text: " + text);
				if (char !== undefined)
					while (char.match(BRACKETORVARIABLE) && i < text.length) {
						i++;
						char = text[i];
					}
				text = text.slice(0, i) + text[i].toUpperCase() + text.slice(i + 1);
			}
			else
				text = text.toUpperCase();
		}
		else text = "";
		return text;
	}

	/**
	 * Seperates MyanmarText at EndOfsyllables
	 *
	 * @param {*} text
	 * @returns {Array} syllables including EndOfSyllables+toneSentenceMark
	 */
	function seperateIntoSyllablesRegex(text, type = TypeEnums.BGN) {
		const DEBUG = true;
		let syllableArray = [];
		DEBUG && console.log("text to seperate at endOfSyllable + tonesentencemark: " + text)
		//console.log('"' + endSyllableKeyArray.join('"|"') + '"')
		//console.log(syllableAndTonemarkCriteria);
		//console.log(regexSearch)
		//REGEX_ENDOFSYLLABLE_AND_TONEMARKS
		//REGEX_TONE_SENTENCE_MARK
		const matches = text.split(REGEX_ENDOFSYLLABLE_AND_TONEMARKS[type]); //tripple split result [no match, first group(syllable), second group(toneMark)]
		DEBUG && console.log(matches);
		DEBUG && console.log("matches.length: " + matches.length + ", tripple: " + (matches.length / 3) + ", rest: " + (matches.length % 3))
		for (let i = 0; i < matches.length / 3; i++) {
			const beforeSplitMatch = matches[i * 3];
			const splitMatchSyllable = matches[i * 3 + 1];
			const splitMatchToneSentence = matches[i * 3 + 2];
			let syllable = "";
			if (beforeSplitMatch) syllable += beforeSplitMatch;
			if (splitMatchSyllable) syllable += splitMatchSyllable;
			if (splitMatchToneSentence) syllable += splitMatchToneSentence;
			//DEBUG && console.log("beforeSplitMatch: " + beforeSplitMatch+", splitMatch: " +splitMatch)
			if (syllable != "") //
			{
				DEBUG && console.log("text.split(REGEX_ENDOFSYLLABLE_AND_TONEMARKS) ending on endOfSyllable -> produces additional empty match")
				syllableArray.push(syllable)
			}
		}
		DEBUG && console.log(syllableArray)
		return syllableArray;
	}

	function hasPreviousChar(previousRomanChar, charSearch = ["n", "ng"], type) {
		const DEBUG = false;
		//const previousCharacterObject = getPreviousRomanCharactersRegex(romanizedArray, type);
		let previousCharacter = previousRomanChar;//previousCharacterObject;//romanizedArray[previousCharacterObject.previousArrayPos]; //for ng check

		DEBUG && console.group("hasPreviousChar: '" + previousCharacter + "'");
		DEBUG && console.log("current romanizedArray: " + romanizedArray.toString());

		if (previousCharacter && previousCharacter != "") {
			//let toneSentenceMarkCount =0;// previousCharacterObject["toneSentenceMarkCount"];
			DEBUG && console.log("hasPreviousChar previousCharacter: '" + previousCharacter + "'");
			/*
			let lastTwoCharacters = previousCharacter.slice(-2);
			DEBUG && console.log("lastTwoCharacters: '"+lastTwoCharacters+"'");
			*/
			DEBUG && console.log("charSearch.length:" + charSearch.length)
			//https://mathiasbynens.be/notes/javascript-unicode#accounting-for-lookalikes
			if (previousCharacter.charAt(previousCharacter.length - 1) == halfVowelCharacter(type)) {
				previousCharacter = "a";
				console.log("previous character is halfVowelCharacter")
			}

			const previousCharacterWithoutAccents = tr(previousCharacter.toLowerCase());// previousCharacter.toLowerCase().normalize("NFKD").replace(/[\u0300-\u036f]/g, ""); //https://stackoverflow.com/questions/990904/remove-accents-diacritics-in-a-string-in-javascript/37511463
			console.log("previousCharacterWithoutAccents: " + previousCharacterWithoutAccents);

			for (let i = 0; i < charSearch.length; i++) {
				let charSearchLength = charSearch[i].length;
				DEBUG && console.log(charSearch[i] + ", charSearchLength:" + charSearchLength)
				if (previousCharacterWithoutAccents.length > (charSearchLength - 1) && previousCharacterWithoutAccents.slice(-charSearchLength).toLowerCase() == charSearch[i]) {
					DEBUG && console.groupEnd("hasPreviousChar: '" + previousCharacter + "', withoutAccents: " + previousCharacterWithoutAccents);
					return true;
				}

			}
		}
		DEBUG && console.log("has not hasPreviousChar: false");
		DEBUG && console.groupEnd("hasPreviousChar: '" + previousCharacter + "'");
		return false;
	}

	function getPreviousRomanCharactersRegex(romanizedArray, type, previousNumberTransliteration = "") {
		const DEBUG = true;
		DEBUG && console.group("getPreviousRomanCharactersRegex")
		let previousSyllable = romanizedArray[romanizedArray.length - 1];
		if (previousNumberTransliteration.length > 0)
			previousSyllable = previousNumberTransliteration;
		let characterWithoutAccents = "";
		if (previousSyllable) {
			let previousLatinCharacters = "";
			DEBUG && console.log(TONE_SENTENCE_MARK[type]);
			const REGEX_ENDSWITH_TONE_SENTENCE_MARK = new RegExp("([" + Object.values(TONE_SENTENCE_MARK[type]).sort((a, b) => b.length - a.length).join("") + "]*)$");
			const match = previousSyllable.match(REGEX_ENDSWITH_TONE_SENTENCE_MARK);
			DEBUG && console.log(match);
			let lastIndex = 0;
			if (match.length > 1)//has found match
			{
				const ToneSentenceMarkAtEnd = match[0];
				lastIndex = ToneSentenceMarkAtEnd.length;
			}
			if (previousSyllable.length - lastIndex >= 0)
				previousLatinCharacters = previousSyllable.slice(0, previousSyllable.length - lastIndex);
			if (previousLatinCharacters.charAt(previousLatinCharacters.length - 1) == halfVowelCharacter(type)) {
				previousLatinCharacters = "a";
				//console.log("previous character is halfVowelCharacter")
			}
			DEBUG && console.log("previousLatinCharacters: " + previousLatinCharacters);
			characterWithoutAccents = previousLatinCharacters.toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, ""); //https://stackoverflow.com/questions/990904/remove-accents-diacritics-in-a-string-in-javascript/37511463
			DEBUG && console.log("characterWithoutAccents: " + characterWithoutAccents);
		}
		DEBUG && console.groupEnd();
		return characterWithoutAccents
	}
	function hasPreviousRomanVowelCharacterRegEx(previousRomanChar = "", type) {
		const DEBUG = true;
		let hasPreviousVowel = false;
		//let previousSyllable = romanizedArray[romanizedArray.length - 1];

		console.group("hasPreviousRomanVowelCharacterRegEx")
		//console.log(previousSyllable);


		//if (previousSyllable) {
		if (previousRomanChar && previousRomanChar.length > 0) {
			let charactersWithoutAccents = previousRomanChar//getPreviousRomanCharactersRegex(romanizedArray, type);
			const lastCharacter = tr(charactersWithoutAccents[charactersWithoutAccents.length - 1]);
			//transliterate tr() to remove Diacritics from vowel characters.
			//alternative? .normalize('NFC') https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/normalize
			const romanVowel = ["ə", "a", "e", "i", "o", "u"]; //endofsyllable ["ယ်":	"è",] //half vowel + vowels

			//console.log("strWithoutAccents: " + strWithoutAccents);
			if (romanVowel.includes(lastCharacter)) {
				hasPreviousVowel = true;
				DEBUG && console.log("previousCharacter is vowel: " + lastCharacter);
			}
		}

		console.groupEnd("hasPreviousRomanVowelCharacterRegEx")
		return hasPreviousVowel;
	}

	function myanmarToLatinStringNumber(myanmarNumber, type = TypeEnums.BGN) {
		let romanizedNumeral = "";
		for (let i = 0; i < myanmarNumber.length; i++) {
			let hasNumeral = NUMERALS[myanmarNumber[i]];

			let hasVOWEL_CHARACTERS;
			if (i + 1 <= myanmarNumber.length)
				hasVOWEL_CHARACTERS = VOWEL_CHARACTERS[type].Dependent[NUMERALS[myanmarNumber[i + 1]]];
			if (hasNumeral && !hasVOWEL_CHARACTERS)
				romanizedNumeral += hasNumeral;
		}
		//let insertIndex=0;
		romanizedNumeral = romanizedNumeral.toString();
		return romanizedNumeral;
	}
	function pronounceMyanmarNumber(myanmarNumber, type = TypeEnums.BGN) {
		return pronounceNumber(myanmarToLatinStringNumber(myanmarNumber, type));
	}
	function pronounceMyanmarNumberArray(myanmarNumber, type = TypeEnums.BGN) {
		return pronounceNumberArray(myanmarToLatinStringNumber(myanmarNumber, type));
	}

	function pronounceNumberArray(latinNumber) {
		const DEBUG = true;
		let pronouncedNumber = [];
		DEBUG && console.group("pronounceNumberArray: " + latinNumber)
		let numberString = parseInt(latinNumber, 10).toString(); //remove leading Zeros	//latinNumber.toString();
		let numberIndexLeft = numberString.length;
		let isSingleDigit = (numberString.length == 1)

		if (isSingleDigit) {
			pronouncedNumber.push(NUMBER_ROMANIZATION[numberString[0]]);
			console.log("isSingleDigit: " + isSingleDigit)
		}
		else if (numberString.length > 8)
			pronouncedNumber.push(latinNumber);
		else {
			DEBUG && console.log("isSingleDigit: " + isSingleDigit + ", numberString: " + numberString + ", numberString.length: " + numberString.length);
			for (let i = 0; i < numberString.length; i++) {
				numberIndexLeft = numberString.length - i;
				let latinNumber = numberString[i];
				DEBUG && console.log("numberIndexLeft: " + numberIndexLeft + " of maxLength: " + numberString.length);
				let romanNumberAtIndex;
				if (latinNumber != "0")
					romanNumberAtIndex = NUMBER_ROMANIZATION[latinNumber];
				DEBUG && console.log("romanNumberAtIndex: " + romanNumberAtIndex + ", [numberString[i]: " + latinNumber);
				if (romanNumberAtIndex) {


					if (i == 0 && latinNumber == 1) {
						pronouncedNumber.push("(");
						pronouncedNumber.push(romanNumberAtIndex);
						pronouncedNumber.push(")");
						//	pronouncedNumber.push(romanNumberAtIndex);
						//pronouncedNumber += ("("+romanNumberAtIndex+")");				
					}
					else
						pronouncedNumber.push(romanNumberAtIndex);
					/*
				if(i==0)
				//pronouncedNumber += ("("+romanNumberAtIndex+")");
				pronouncedNumber.push(romanNumberAtIndex);
				else
				pronouncedNumber.push(romanNumberAtIndex);
				*/
				}

				let hasPositionRomanization = NUMBER_POSITION_ROMANIZATION[numberIndexLeft];
				DEBUG && console.log(pronouncedNumber + ", hasPositionRomanization: " + hasPositionRomanization);
				if (hasPositionRomanization) {
					if (romanNumberAtIndex || i == 0)
						pronouncedNumber.push(hasPositionRomanization);
				}
			}

		}
		DEBUG && console.log("pronouncedNumber " + pronouncedNumber);
		DEBUG && console.groupEnd("pronounceNumberArray: ")
		return pronouncedNumber;
	}
	function pronounceNumber(latinNumber) {
		const DEBUG = true;
		DEBUG && console.group("pronounceNumber: " + latinNumber)
		let pronouncedNumber = "";
		let numberString = parseInt(latinNumber, 10).toString(); //latinNumber.toString();
		let numberIndexLeft = numberString.length;
		let isSingleDigit = (numberString.length == 1);
		//console.log(pronounceNumberArray(number))
		if (isSingleDigit)
			pronouncedNumber = NUMBER_ROMANIZATION[numberString[0]];
		else if (numberString.length > 8)
			return latinNumber;
		else {
			DEBUG && console.log("isSingleDigit: " + isSingleDigit)
			for (let i = 0; i < numberString.length; i++) {
				numberIndexLeft = numberString.length - i;
				DEBUG && console.log("numberIndexLeft: " + numberIndexLeft + " of maxLength: " + numberString.length)
				let romanNumberAtIndex;
				if (numberString[i] != "0")
					romanNumberAtIndex = NUMBER_ROMANIZATION[numberString[i]];
				if (romanNumberAtIndex) {
					DEBUG && console.log("romanNumberAtIndex: " + romanNumberAtIndex)
					if (i == 0)
						//pronouncedNumber += ("("+romanNumberAtIndex+")");
						pronouncedNumber += romanNumberAtIndex;
					else
						pronouncedNumber += romanNumberAtIndex;
				}

				let hasPositionRomanization = NUMBER_POSITION_ROMANIZATION[numberIndexLeft];
				DEBUG && console.log(pronouncedNumber + ", hasPositionRomanization: " + hasPositionRomanization);
				if (hasPositionRomanization) {
					if (romanNumberAtIndex || i == 0)
						pronouncedNumber += (hasPositionRomanization);
				}
			}
		}

		DEBUG && console.log("pronouncedNumber: " + pronouncedNumber);
		DEBUG && console.groupEnd("pronounceNumber: ");
		return pronouncedNumber;
	}
	/**
	 * reverse after endOfSyllable detection to allow correct consonantCharacterCombination ရွှေ(shwe)[gold]
	 *
	 * @param {*} text
	 * @returns text with exchanged placement of ှွ -> ွှ
	 */
	function reverseUnicodeHack(text) {
		const DEBUG = false;
		let result = text.replace("ှွ", "ွှ");

		DEBUG && console.group("reverseUnicodeHack");
		DEBUG && console.log(text + " tryReverseHack: " + result);
		DEBUG && console.groupEnd("reverseUnicodeHack");
		return result;
	}

	//TODO regex version checkClassifier CL without additionalEndOfSyllable,ToneSentenceMark
	/**
	 * checkClassifier
	 *
	 * @param {string} text
	 * @param {Object} "Array or Object Type"
	 * @returns {boolean} "has Classifier without followingVowel or EndOfSyllable"
	 */
	function checkClassifier(text, clType, type = TypeEnums.BGN) {
		const DEBUG = true;
		let result = false;
		console.group("checkClassifier: " + text);
		if (typeof (text) == "string") {
			const typeIsArray = Array.isArray(clType);
			let typeToSearch = clType;

			if (!typeIsArray)
				typeToSearch = Object.values(clType);
			for (let cl of typeToSearch) {
				//DEBUG && console.log("test for classifier: " + cl)
				//let clIndex = text.indexOf(cl);
				let startWithCL = text.startsWith(cl); //should be faster since there is no need to look at later string positions on false
				/*
				if(clIndex>=0)
					DEBUG && console.log("found Classifier is: " + cl +" at index: " + clIndex);
				if(clIndex===0)//faster than startsWith()?
				*/
				if (startWithCL) {
					//DEBUG && console.log("found Classifier is at start: " + cl +" at index: " + clIndex);
					DEBUG && console.log("found Classifier at start: " + cl);
					/* hack next character not endOfSyllable
						search for better way to detect whole words
					*/
					const nextIndex = cl.length; //index 0 = 1 length
					console.log("text.length: " + text.length + ", nextIndex: " + nextIndex + ", classifier to test: " + cl)
					const textAfterCL = text.slice(cl.length).trim();

					//let latinGroup = returnNextLatinCharacterGroup(textAfterCL);

					let nextCharAfterCL = "";
					if (textAfterCL.length > 0)
						nextCharAfterCL = textAfterCL[0];
					console.log("cl: " + cl + ", text after CL: " + textAfterCL + ", nextCharAfterCL: " + nextCharAfterCL)

					let isVowelIndependent = false;
					if (VOWEL_CHARACTERS[type].Independent[nextCharAfterCL])
						isVowelIndependent = true;


					//const hasVowelCount = testFollowingCharactersFor(isVowel, textAfterCL, true);
					//DEBUG && console.log("Vowel on rest: " + textAfterCL + ", targetVowelCount:#" + hasVowelCount);
					const matchesVowel = textAfterCL.match(REGEX_STARTSWITH_VOWELDEPENDENT[type]); //Are there cases for Vowel Independend?
					let hasDependendVowel = 0;
					if (matchesVowel) {
						DEBUG && console.log("matchesVowel: " + matchesVowel);
						hasDependendVowel = matchesVowel[0].length;
						DEBUG && console.log("dependend Vowel on rest: " + textAfterCL + ", targetVowelCount:#" + hasDependendVowel);
					}
					//const hasEndOfSyllable = nextEndOfSyllable(textAfterCL);  //result = false when no nextEndOfSyllable
					const hasEndOfSyllable = nextEndOfSyllableRegex(textAfterCL, type);  //result = false when no nextEndOfSyllable

					//result = {index:indexFound, character:smallestIndexSyllable};
					console.log(hasEndOfSyllable);


					const hasEndOfSyllableAfterCL = (hasEndOfSyllable && hasEndOfSyllable.index == 0)
					const isIncompleteSyllable = hasDependendVowel || hasEndOfSyllableAfterCL;

					console.log("CL without Vowel, EndOfsyllable: " + isIncompleteSyllable +
						", isVowelIndependent: " + isVowelIndependent
					)
					//if character after CL is nonMyanmar or Consonant
					//if(text.length > nextIndex)
					if (!isIncompleteSyllable) {
						//console.log("text[nextIndex]: " +text[nextIndex])
						console.log("checkClassifier has found Classifier '" + cl + "' without following Vowel/EndOfSyllable")
						//if(text[nextIndex]!="်")		
						result = true;
					}
					//break;
				}
			}
		}
		console.groupEnd("checkClassifier: " + text);
		return result;
	}
	/**
	 *
	 *
	 * @param {string} text to check
	 * @returns {boolean} textStartsWithClassifier
	 */
	function infrontOfClassifierword(text, type = TypeEnums.BGN) {
		const DEBUG = true;
		let result = false;
		//getFullPhonem(nextMyanmarPhonem);
		DEBUG && console.group("infrontOfClassifierword test")
		DEBUG && console.log("check classifier for text: " + text)
		//longestMatchingNoun -> endIndex -> from index startsWithClassifier
		let matchNoun = false;
		let longestMatch = "";
		let foundNoun = "";
		let textToCheck = text;
		let textAfterNoun = "";
		if (textToCheck && textToCheck.length > 0) {
			let tryNext = true;
			let i = 0;
			while (tryNext && i < textToCheck.length) {
				longestMatch += textToCheck[i];
				if (NOUNS.find(item => item.startsWith(longestMatch))) {
					if (NOUNS.indexOf(longestMatch) > 0) {
						foundNoun = longestMatch;
					}
					i++;
				}
				else
					tryNext = false;
			}
			console.log("foundNoun: " + foundNoun);
		}
		if (foundNoun.length > 0) {
			textAfterNoun = textToCheck.slice(foundNoun.length);
		}

		if (checkClassifier(textAfterNoun, CLASSIFIERWORDS, type) || (checkClassifier(textAfterNoun, NUMBER_POSITION_ROMANIZATION, type)))
			result = true;
		if (!result) { //check if noun is Classifier
			if (checkClassifier(textToCheck, CLASSIFIERWORDS, type) || (checkClassifier(textToCheck, NUMBER_POSITION_ROMANIZATION, type)))
				result = true;
		}

		DEBUG && console.groupEnd("infrontOfClassifierword test")

		return result;
	}

	function nextConsonantObject(text, type = TypeEnums.BGN) {
		console.log("type: " + type)
		let foundConsonant = { consonant: "", hasConsonantCombination: "", hasConsonantMultichar: "", length: 0 };
		if (text && text.length > 0) {
			const hasConsonantMultichar = text.match(REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR[type]);
			if (hasConsonantMultichar) {
				//foundConsonant.consonant = hasConsonantMultichar[0];
				foundConsonant.hasConsonantMultichar = hasConsonantMultichar[0];
				foundConsonant.length += hasConsonantMultichar[0].length;
			}
			else {
				const hasConsonant = text.match(REGEX_STARTSWITH_CONSONANT[type]);
				console.log(hasConsonant)
				if (hasConsonant) {
					foundConsonant.consonant = hasConsonant[0];
					foundConsonant.length += hasConsonant[0].length;
				}
			}

			let textAfterConsonant = "";
			if (foundConsonant.consonant != "")
				textAfterConsonant = text.slice(foundConsonant.consonant.length);
			if (foundConsonant.hasConsonantMultichar != "")
				textAfterConsonant = text.slice(foundConsonant.hasConsonantMultichar.length);
			//console.log(textAfterConsonant)
			const hasConsonantCombination = textAfterConsonant.match(REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATIONS[type]);
			console.log("hasConsonantCombination: " + hasConsonantCombination)
			if (hasConsonantCombination) {
				foundConsonant.hasConsonantCombination = hasConsonantCombination[0];
				foundConsonant.length += hasConsonantCombination[0].length;
			}
		}

		return foundConsonant;
	}

	function nextVowelObject(text, type = TypeEnums.BGN) {
		let result = { vowel: "", type: "" };
		const hasVowelDependentMatch = text.match(REGEX_STARTSWITH_VOWELDEPENDENT[type]);
		const hasVowelIndependentMatch = text.match(REGEX_STARTSWITH_VOWELINDEPENDENT[type]);

		if (hasVowelIndependentMatch) {
			result.vowel = hasVowelIndependentMatch[0];
			result.type = "Independent";
		}
		else if (hasVowelDependentMatch) {
			result.vowel = hasVowelDependentMatch[0];
			result.type = "Dependent";
		}
		return result;
	}
	function nextVowelDependentObject(text, type = TypeEnum.BGN) {
		let result = { vowel: "", type: "" };
		const hasVowelDependentMatch = text.match(REGEX_STARTSWITH_VOWELDEPENDENT[type]);

		if (hasVowelDependentMatch) {
			result.vowel = hasVowelDependentMatch[0];
			result.type = "Dependent";
		}
		return result;

	}
	function nextVowelIndependendObject(text, type = TypeEnum.BGN) {
		let result = { vowel: "", type: "" };
		const hasVowelIndependentMatch = text.match(REGEX_STARTSWITH_VOWELINDEPENDENT[type]);

		if (hasVowelIndependentMatch) {
			result.vowel = hasVowelIndependentMatch[0];
			result.type = "Independent";
		}
		return result;
	}

	function nextToneSentenceMark(text, type = TypeEnums.BGN) {
		let result = "";
		const hasToneSentenceMarks = text.match(REGEX_STARTSWITH_TONE_SENTENCE_MARK[type]);
		if (hasToneSentenceMarks) {
			result = hasToneSentenceMarks[0];
		}
		return result;
	}

	/**
	 *
	 *
	 * @param [{ consonant:string, vowel:string, toneSentencemark:string }] phonemArray
	 * @returns string phonem
	 */
	function phonemObjectArrayToArray(phonemArray) {
		let textArray = [];
		for (let i = 0; i < phonemArray.length; i++) {
			let text = "";
			text += phonemArray[i].consonant;
			text += phonemArray[i].vowel;
			text += phonemArray[i].toneSentenceMark;
			if (phonemArray[i].endOfSyllable)
				text += phonemArray[i].endOfSyllable;
			textArray.push(text)
		}
		return textArray;
	}
	/**
	 * 
	 * @param string myanmartext without EndOfSyllable
	 * @return [{ consonant, hasConsonantCombination, hasConsonantMultichar, vowel, toneSentencemark }] phonemObjectArray of (Consonant,Vowel, ToneSentencemark)
	 */
	function phonemObjectArray(text, endOfSyllableOnlyFinalCharacter = false, type = TypeEnums.BGN) {
		const DEBUG = true;
		console.group("phonemObjectArray: " + text);
		let restText = text;
		//let fullPhonem = "";
		let phonemArray = [];
		let syllableIndex = 0;
		while (syllableIndex < text.length) {
			let IndependentPhonem = { fullPhonem: "", consonant: "", hasConsonantCombination: "", hasConsonantMultichar: "", vowel: "", toneSentenceMark: "", endOfSyllable: "", hasToneSentenceMarkAfterEndOfSyllable: "" };
			console.groupCollapsed("phonemStart without endOfSyllable at syllableIndex: " + syllableIndex + ", restText: " + restText)

			const independentVowelObject = nextVowelIndependendObject(restText, type);

			const independentVowel = independentVowelObject.vowel;
			console.log("independentVowelObject: ");
			console.log(independentVowelObject)
			if (independentVowel.length > 0) {
				restText = restText.slice(independentVowel.length)
				console.log("add independent vowel: " + independentVowel);
				syllableIndex += independentVowel.length;
				IndependentPhonem.vowel = independentVowel;
				IndependentPhonem.vowelType = independentVowelObject.type; //"Independent"
				consoleLogDeepCopyObject(IndependentPhonem);
				phonemArray.push(IndependentPhonem);
			}
			else {
				let phonem = { fullPhonem: "", consonant: "", hasConsonantCombination: "", hasConsonantMultichar: "", vowel: "", toneSentenceMark: "", endOfSyllable: "", hasToneSentenceMarkAfterEndOfSyllable: "" };

				const consonantObject = nextConsonantObject(restText, type);

				const consonant = consonantObject.consonant;
				const consonantLength = consonantObject.length;
				console.log(consonantObject)
				console.log("consonant or consonantMultichar: " + consonant +
					",consonantObject.hasConsonantCombination: " + consonantObject.hasConsonantCombination +
					",consonantObject.hasConsonantMultichar: " + consonantObject.hasConsonantMultichar +
					", consonant.length: " + consonantLength);
				syllableIndex += consonantLength;
				restText = restText.slice(consonantLength)
				console.log("resttext after Consonant: " + restText);
				let vowelObjectException;
				let vowel, vowelObject;

				if (endOfSyllableOnlyFinalCharacter) {
					vowelObjectException = nextVowelDependentObject(restText + "်", type);

				}
				console.log(vowelObjectException)
				if (vowelObjectException && vowelObjectException.vowel && vowelObjectException.vowel.length > 0) {
					vowelObject = vowelObjectException;
				}
				else {
					vowelObject = nextVowelDependentObject(restText, type);
				}
				vowel = vowelObject.vowel;

				console.log("vowel after consonant '" + consonant + "', vowel: " + vowel + ", vowel.length: " + vowel.length + ", vowelType: " + vowelObject.type);
				syllableIndex += vowel.length;
				restText = restText.slice(vowel.length);
				console.log("resttext after Vowel: " + restText);

				const toneSentenceMark = nextToneSentenceMark(restText, type);
				console.log("toneSentenceMark after '" + (consonant + vowel) + "': " + toneSentenceMark + ", toneSentenceMark.length: " + toneSentenceMark.length);
				syllableIndex += toneSentenceMark.length;
				restText = restText.slice(toneSentenceMark.length);

				if ((consonant.length + vowel.length + toneSentenceMark.length) == 0) {
					const hasNextConsonant = nextConsonantObject(restText[0], type)
					console.log(hasNextConsonant);
					if (hasNextConsonant && (hasNextConsonant.consonant != "" || hasNextConsonant.hasConsonantMultichar != "")) {
						DEBUG && console.log("Continue with next Consonant character after currentConsonant"); // ဂျပန် okell multiConsonant ဂျ rest ပ
					}
					else {
						DEBUG && console.log("Undefined Myanmar/Pali character: " + restText[0]);
						syllableIndex++;
						restText = restText.slice(1);
					}

				}

				if (restText.length > 0)
					console.log("restText after phonem to process: " + restText);
				phonem.consonant = consonant;
				phonem.hasConsonantCombination = consonantObject.hasConsonantCombination;
				phonem.hasConsonantMultichar = consonantObject.hasConsonantMultichar;
				phonem.vowel = vowelObject.vowel;
				phonem.vowelType = vowelObject.type;
				phonem.toneSentenceMark = toneSentenceMark;
				phonemArray.push(phonem);
			}


			console.groupEnd("phonemStart without endOfSyllable")
		}
		console.groupEnd("phonemObjectArray");
		return phonemArray;
	}
	/**
	 * text presplit into [C+V+Final+ToneSentenceMark]
	 * split syllable C + V + EndOfsyllable + ToneSentencemark
	 * returns MyanmarPhonems
	 * @param {*} Array myanmar syllableArray
	 */
	function createPhonemObjectArray(syllableArray, type = TypeEnums.BGN) {
		const DEBUG = true;
		//Not empty [] for better code readability/function return value preview
		console.groupCollapsed("createPhonemObjectArray");
		let myanmarPhonemArray = [Object.assign({},emptySyllable) 
			//{ consonant: "", hasConsonantCombination: "", hasConsonantMultichar: "", vowel: "", endOfSyllable: "", toneSentenceMark: "", hasToneSentenceMarkAfterEndOfSyllable: "" }
		];
		myanmarPhonemArray = []; //preset and emptied again just for showing result type
		console.log(syllableArray);
		console.log("syllableArray.length: " + syllableArray.length)
		for (let i = 0; i < syllableArray.length; i++) {
			const syllable = syllableArray[i];
			DEBUG && console.group("process syllable: " + syllable);
			DEBUG && console.log(syllable + ", syllable length: " + syllable.length)
			/*example ချမိတ် လိုးကူး -> ချ မ " ိတ်" လ" ိုး" က "ူး"
				ချမိတ် ချ မ ိတ်
			*/
			//let syllableIndex = 0;
			let syllableEnd = syllable.length;

			const endOfSyllableMatch = syllable.match(REGEX_CONSONANT_END_OF_SYLLABLE[type]);
			DEBUG && console.log(endOfSyllableMatch);
			let endOfSyllable, hasToneSentenceMarkAfterEndOfSyllable;
			//process string until endOfSyllable or endOf string
			let restText = syllable;

			if (endOfSyllableMatch) {
				endOfSyllable = endOfSyllableMatch[0];
				syllableEnd = endOfSyllableMatch.index;

				restText = syllable.slice(0, syllableEnd);
				const textAfterEndOfSyllable = syllable.slice(syllableEnd + endOfSyllable.length, syllable.length);
				DEBUG && console.log("textAfterEndOfSyllable: " + textAfterEndOfSyllable)
				DEBUG && console.log("restText -syllableWithoutEndOfSyllable: " + restText +
					", endOfSyllable: " + endOfSyllable +
					", endOfSyllableLength: " + endOfSyllable.length +
					", endOfSyllableMatch.index: " + endOfSyllableMatch.index);
				//hasToneSentenceMarkAfterEndOfSyllable = nextToneSentenceMark(textAfterEndOfSyllable)
				const matchToneSentenceMarkAfterEndOfSyllable = textAfterEndOfSyllable.match(REGEX_STARTSWITH_TONE_SENTENCE_MARK[type]);
				if (matchToneSentenceMarkAfterEndOfSyllable)
					hasToneSentenceMarkAfterEndOfSyllable = matchToneSentenceMarkAfterEndOfSyllable[0];
				if (hasToneSentenceMarkAfterEndOfSyllable)
					DEBUG && console.log("hasToneSentenceMark AfterEndOfSyllable: " + hasToneSentenceMarkAfterEndOfSyllable);
			}

			restText = reverseUnicodeHack(restText);  //reverse to allow correct consonantCharacterCombination ရွှေ(shwe)[gold]
			console.log("restText: " + restText)

			const endOfSyllableOnlyFinalCharacter = endOfSyllable == "်"
			console.log(endOfSyllable + ", endOfSyllableOnlyFinalCharacter: " + endOfSyllableOnlyFinalCharacter)
			const myanmarPhonemSyllableArray = phonemObjectArray(restText, endOfSyllableOnlyFinalCharacter, type);
			DEBUG && console.log(myanmarPhonemSyllableArray);
			myanmarPhonemArray.push(...myanmarPhonemSyllableArray);
			if (endOfSyllable) {
				DEBUG && console.log("last syllable has final character in vowel?: ") //လိုခေါ်
				const lastSyllable = myanmarPhonemArray[myanmarPhonemArray.length - 1];
				DEBUG && console.log(lastSyllable)
				const lastVowelHasFinalCharacter = lastSyllable && lastSyllable.vowel.includes("်");
				DEBUG && console.log("endOfSyllable: " + endOfSyllable + ", myanmarPhonemArray.length: " + myanmarPhonemSyllableArray.length + ", lastVowelHasFinalCharacter: " + lastVowelHasFinalCharacter)
				if (lastVowelHasFinalCharacter) {
					endOfSyllable = "";
				}

				if (myanmarPhonemSyllableArray.length > 0) {//Consonant + (without vowel a) + EndOfSyllable
					//console.log(myanmarPhonemArray[myanmarPhonemArray.length-1]);
					myanmarPhonemSyllableArray[myanmarPhonemSyllableArray.length - 1].endOfSyllable = endOfSyllable;
					myanmarPhonemSyllableArray[myanmarPhonemSyllableArray.length - 1].hasToneSentenceMarkAfterEndOfSyllable = hasToneSentenceMarkAfterEndOfSyllable;
				}
				else { //no previous Consonant/IndependentVowel
					let myanmarPhonemEndOfSyllable = Object.assign({},emptySyllable)
					//{ consonant: "", hasConsonantCombination: "", hasConsonantMultichar: "", vowel: "", endOfSyllable: "", toneSentenceMark: "", hasToneSentenceMarkAfterEndOfSyllable: "" }
					myanmarPhonemEndOfSyllable.endOfSyllable = endOfSyllable;
					myanmarPhonemEndOfSyllable.hasToneSentenceMarkAfterEndOfSyllable = hasToneSentenceMarkAfterEndOfSyllable;
					myanmarPhonemArray.push(myanmarPhonemEndOfSyllable);
				}

			}

			DEBUG && console.log(myanmarPhonemArray);
			DEBUG && console.log("myanmarPhonemArray concatenated: " + phonemObjectArrayToArray(myanmarPhonemArray));

			/*
			nextConsonant
			ConsonantCombination
				nextVowel	//addVowel ("a"/independent/dependend/endOfSyllable)
				ToneSentencemark
			EndOfSyllable
			ToneSentencemark
			*/
			DEBUG && console.groupEnd("process syllable: " + syllable);
		}
		console.groupEnd("createPhonemObjectArray");
		return myanmarPhonemArray;
	}
	function insertConsonantMedial(latinConsonant, medial, hasConsonantMultichar, type) {
		const DEBUG = false;
		let newLatinConsonant = latinConsonant;
		DEBUG && console.group("insertConsonantMedial: " + medial)
		if (medial != "") {
			let romanConsonantCharacterCombination;
			romanConsonantCharacterCombination = CONSONANT_CHARACTER_COMBINATIONS[type][medial];
			DEBUG && console.log("romanConsonantCharacterCombination: " + romanConsonantCharacterCombination)

			let inputConsonantAtConsonantCombinationFoundIndex = romanConsonantCharacterCombination.indexOf("-");
			const romanConsonantCombinationWithoutHypen = romanConsonantCharacterCombination.replace("-", "");
			if (type == TypeEnums.IPA && medial == "ှ" && newLatinConsonant.length > 1) {//hasConsonantMultichar မွှ /m̥w/ /m̥j/ မျှ
				newLatinConsonant = newLatinConsonant[0] + romanConsonantCombinationWithoutHypen + newLatinConsonant[1];
			}
			else
				newLatinConsonant = romanConsonantCombinationWithoutHypen.slice(0, inputConsonantAtConsonantCombinationFoundIndex) + newLatinConsonant + romanConsonantCombinationWithoutHypen.slice(inputConsonantAtConsonantCombinationFoundIndex);

			DEBUG && console.log("romanConsonantCombinationWithoutHypen: " + romanConsonantCombinationWithoutHypen);
		}
		DEBUG && console.groupEnd("insertConsonantMedial: " + medial)
		return newLatinConsonant;
	}
	function insertConsonantMedialLoopAvailable(latinConsonant, medial, hasConsonantMultichar, type) {
		let newLatinConsonant = latinConsonant;
		console.group("insertConsonantMedial: " + medial)
		console.log("latinConsonant: " + latinConsonant);
		if (medial && medial != "") {
			let restMedial = medial;
			while (restMedial.length > 0) {
				let nextMedialMatch = restMedial.match(REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATION[type]);
				console.log(nextMedialMatch);
				if (nextMedialMatch) {
					const medialMatch = nextMedialMatch[0];
					console.log("medialMatch: " + medialMatch + ", medialMatch length: " + medial.length)
					restMedial = restMedial.slice(medialMatch.length);
					nextMedialMatch = restMedial.match(REGEX_STARTSWITH_CONSONANT_CHARACTER_COMBINATION[type]);
					newLatinConsonant = insertConsonantMedial(newLatinConsonant, medialMatch, hasConsonantMultichar, type);
				} else if (restMedial.length > 0) {
					restMedial = restMedial.slice(1); //in case of unknown character
				}

			}

		}
		console.groupEnd("insertConsonantMedial: " + medial)
		return newLatinConsonant;
	}

	/**
	 * merge latinConsonant with latinConsonantCombinator
	 *
	 * @param {*} myanmarPhonem
	 * @returns string latinConsonant
	 */
	function getLatinConsonant(myanmarPhonemArray, currentArrayPos, currentAndNextSyllableIsWord = false, nextIsCompoundWord = false, nextIsNasalized = false, hasCurrentVoicedStopConsonant = "", currentStackedException = false, nextConsonantIsStacked = false, previousNorNG = false, previousT = false, previousK = false, previousA = false, hasPreviousLatinVowel = false, previousHalfVowel = false, previousCharacterIndex = 0, stackedIndexes = [], nextIsNoun, type = TypeEnums.BGN, spoken = true, sandhi = true) {
		console.group("getLatinConsonant" + ", type: " + type + ", keyName: " + Object.keys(TypeEnums)[type])

		//{ consonant:"", hasConsonantCombination:"", hasConsonantMultichar:"", length:0};
		//Note3 
		/* At  the  beginning  of  a  word,  the  vowel-carrier  အ  should  not  be  romanized,  unless 
			followed by a consonant character that does not carry a vowel character or 
			an end-of-syllable mark, 
				in which case the character အ should be romanized a */
		//exception? endOfSyllable (wun)?
		//(hasEndOfSyllable.character == "ဝတ်") ||
		const previousMyanmarPhonem = myanmarPhonemArray[currentArrayPos - 1];
		const myanmarPhonem = myanmarPhonemArray[currentArrayPos];
		const nextMyanmarPhonem = myanmarPhonemArray[currentArrayPos + 1];
		//const nextMyanmarPhonem = myanmarPhonemArray[currentArrayPos + 1]
		let latinConsonant = "";

		//characterPos + hasPreviousVowel for stackedCharacters check
		console.log(Object.assign({}, myanmarPhonem));
		consoleLogDeepCopyObject(myanmarPhonem.consonant)
		//hack wg unicodehack für endOfSyllable -> ConsonantCombination seperated if it has combinator ွှ
		if (CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR[type][myanmarPhonem.consonant + myanmarPhonem.hasConsonantCombination]) {
			myanmarPhonem.hasConsonantMultichar = myanmarPhonem.consonant + myanmarPhonem.hasConsonantCombination;
			myanmarPhonem.consonant = "";
			myanmarPhonem.hasConsonantCombination = "";
		}
		console.log(//"previousLatinCharacter: " + previousLatinCharacter+
			", previousNorNG: " + previousNorNG +
			", previousT: " + previousT +
			", hasPreviousLatinVowel:" + hasPreviousLatinVowel);
		const hasPreviousLatinVowelNOrNG = hasPreviousLatinVowel || previousNorNG; //Note 5 exception not upperStackedCharacter example ဥက္ကဌ (ukkada) changed from BGN to ဥက်ကဌ (UetKaDa)


		if (myanmarPhonem.hasConsonantMultichar != "") {
			console.log("myanmarPhonem.hasConsonantMultichar: " + myanmarPhonem.hasConsonantMultichar)
			latinConsonant = CONSONANT_CHARACTER_COMBINATIONS_MULTICHAR[type][myanmarPhonem.hasConsonantMultichar];
			if (spoken && hasPreviousLatinVowelNOrNG && CONSONANT_AFTER_ROMAN_VOWEL_MULTICHAR[type][myanmarPhonem.hasConsonantMultichar]) {
				latinConsonant = CONSONANT_AFTER_ROMAN_VOWEL_MULTICHAR[type][myanmarPhonem.hasConsonantMultichar];
			}
			console.log(latinConsonant);
		}
		else
			if (myanmarPhonem.consonant != "") {
				//console.log("selected Type: " + type);
				latinConsonant = CONSONANT[type][myanmarPhonem.consonant];
				console.group("CONSONANT_MODIFICATION_YAPIN_YAYIT[type]")
				console.log(CONSONANT_MODIFICATION_YAPIN_YAYIT[type])
				const hasYapinYayitModification = CONSONANT_MODIFICATION_YAPIN_YAYIT[type][myanmarPhonem.consonant];
				if (hasYapinYayitModification &&
					(myanmarPhonem.hasConsonantCombination == "ျ" || myanmarPhonem.hasConsonantCombination == "ြ")) {
					latinConsonant = hasYapinYayitModification;
				}
				console.groupEnd("CONSONANT_MODIFICATION_YAPIN_YAYIT[type]")
				//CONSONANT_AFTER_ROMAN_VOWEL[type][myanmarPhonem.consonant] -> groupedStopConsonants[myanmarPhonem.consonant].voiced

				let ConsonantAfterVowel;


				console.group("CONSONANT after Vowel")
				if (spoken && hasCurrentVoicedStopConsonant != "") {//!(currentConsonantIsStacked && nextConsonantIsStacked) && 
					//const hasVoicedConsonant = groupedStopConsonants[myanmarPhonem.consonant].voiced;
					console.log("myanmarPhonem.consonant: " + myanmarPhonem.consonant)
					console.log("hasVoicedConsonant: " + hasCurrentVoicedStopConsonant)
					ConsonantAfterVowel = CONSONANT[type][hasCurrentVoicedStopConsonant];
					//	if(!ConsonantAfterVowel)
					//		ConsonantAfterVowel = CONSONANT_AFTER_ROMAN_VOWEL[type][myanmarPhonem.consonant]; //special cases? IPA သ t -> d
					console.log("ConsonantAfterVowel: " + ConsonantAfterVowel)
				}

				//အင်္ကျီ  
				//ခင်ဗျာ
				const isKinza = myanmarPhonem.endOfSyllable == "င်";
				const previousIsKinza = previousMyanmarPhonem && previousMyanmarPhonem.endOfSyllable == "င်";
				const previousIsOnlyKinza = previousIsKinza && previousMyanmarPhonem.consonant == "အ" &&
					previousMyanmarPhonem.vowel == "";
				console.log("previousIsOnlyKinza: " + previousIsOnlyKinza)
				console.log("isKinza: " + isKinza + ", (!isKinza||myanmarPhonem.endOfSyllable==): " + (!isKinza || myanmarPhonem.endOfSyllable == ""));
				console.log("hasPreviousLatinVowelNOrNG: " + hasPreviousLatinVowelNOrNG +
					", ConsonantAfterVowel: " + ConsonantAfterVowel)
				//&& !previousIsOnlyKinza && !isKinza
				console.group("set consonant after roman vowel or n or ng")
				//const currentSyllable = getFullPhonem(myanmarPhonem);

				if (!(currentStackedException && nextConsonantIsStacked) && hasPreviousLatinVowelNOrNG && ConsonantAfterVowel && spoken) {//&& !previousHalfVowel
					const endOfSyllable = myanmarPhonem.endOfSyllable;
					const hasNasalizedFinal = CONSONANT_END_OF_SYLLABLE_N_TYPE[endOfSyllable];

					//if(!hasNasalizedFinal)
					myanmarPhonem.consonant = hasCurrentVoicedStopConsonant; //todo to check 
					latinConsonant = ConsonantAfterVowel;		//test ကပျာကယာ

					if (previousMyanmarPhonem && !currentStackedException) // "promise": [ɡədḭ] ကတိ ← /ka̰/ + /tḭ/
					{
						const hasPreviousVoicedConsonant = phonemHasVoicedStopConsonant(previousMyanmarPhonem);
						if (hasPreviousVoicedConsonant != "") {
							console.log("hasPreviousVoicedConsonant: " + hasPreviousVoicedConsonant)
							const previousVoicedConsonant = CONSONANT[type][hasPreviousVoicedConsonant];
							console.log("previousVoicedConsonantAfterVowel: " + previousVoicedConsonant)
							previousMyanmarPhonem.consonant = previousVoicedConsonant; //how to change outputted string. defer string output possibility to show changed myanmarCharacters for second sweep
							consoleLogDeepCopyObject(previousMyanmarPhonem);
						}

					}
				}
				console.groupEnd("set consonant after roman vowel or n or ng")
				console.group("set NasalizedFinal Consonant")
				console.log("nextIsNasalized: " + nextIsNasalized + ", nextIsNoun: " + nextIsNoun + ", hasCurrentVoicedStopConsonant: " + hasCurrentVoicedStopConsonant)
				//else
				//&& hasCurrentVoicedStopConsonant != ""
				if (spoken && sandhi && nextIsNoun && myanmarPhonem.consonant != "အ" && nextIsNasalized) {
					console.group("adjustForNasalizedFinal")
					//const previousWord = checkPreviousForKnownWord(myanmarPhonemArray, currentArrayPos + 1);//to double check မင်္ဂလာပါ။  vs တနင်္ဂနွေနေ့
					//console.log("previousWord: " + previousWord.value)
					adjustForNasalizedFinal(myanmarPhonemArray, currentArrayPos, nextIsNasalized);//, previousWord, currentAndNextSyllableIsWord, nextIsNoun, previousCharacterIndex, stackedIndexes);
					//reassigne updated Consonant
					latinConsonant = CONSONANT[type][myanmarPhonem.consonant];
					console.groupEnd("adjustForNasalizedFinal")
				}
				console.groupEnd("set NasalizedFinal Consonant")
				console.log("latinConsonant: " + latinConsonant)
				console.groupEnd("CONSONANT after Vowel")

				if (myanmarPhonem.consonant == "အ") {
					console.log(myanmarPhonem)
					console.group("consonant အ processing")

					let hasEndOfSyllable = false;
					console.log(myanmarPhonem.endOfSyllable != undefined)
					console.log(myanmarPhonem.endOfSyllable != "")
					if (myanmarPhonem.endOfSyllable != undefined && myanmarPhonem.endOfSyllable != "") {
						hasEndOfSyllable = true;
					}
					console.log("hasEndOfSyllable: " + hasEndOfSyllable)
					//const notEmptyEndOfSyllable =  (hasEndOfSyllable && myanmarPhonem.endOfSyllable!="") //no property or filled property
					//console.log(notEmptyEndOfSyllable)
					const isFalseEndOfSyllable = falseEndOfSyllable.includes(myanmarPhonem.endOfSyllable);
					if (((myanmarPhonem.vowel != "" && myanmarPhonem.vowelType == "Dependent") || (hasEndOfSyllable && !isFalseEndOfSyllable))) {	//အင်္ကျီ (in-gyi) အဝတ်   (awut)/ အသီးအရွက် 
						console.log(myanmarPhonem)
						console.log("has vowel or endOfSyllable");

						latinConsonant = "";
					}

					console.groupEnd("consonant အ processing")
					if (currentArrayPos > 0) {
						latinConsonant = "-" + latinConsonant; // မအူ → ma-u, သီးပင်အိုင် → Thibin-aing
					}
				}
			}

		if (myanmarPhonem.hasConsonantCombination && myanmarPhonem.hasConsonantCombination != "") {
			latinConsonant = insertConsonantMedialLoopAvailable(latinConsonant, myanmarPhonem.hasConsonantCombination, myanmarPhonem.hasConsonantMultichar, type);
			/*
			let romanConsonantCharacterCombination;
			romanConsonantCharacterCombination = CONSONANT_CHARACTER_COMBINATIONS[type][myanmarPhonem.hasConsonantCombination];
			let inputConsonantAtConsonantCombinationFoundIndex = romanConsonantCharacterCombination.indexOf("-");
			const romanConsonantCombinationWithoutHypen = romanConsonantCharacterCombination.replace("-", "");
			latinConsonant = romanConsonantCombinationWithoutHypen.slice(0, inputConsonantAtConsonantCombinationFoundIndex) + latinConsonant + romanConsonantCombinationWithoutHypen.slice(inputConsonantAtConsonantCombinationFoundIndex);*/
		}

		if (currentArrayPos > 0 && myanmarPhonem.consonant != "အ" && latinConsonant) {
			const NGY = previousNorNG && (latinConsonant[0] == "g" || latinConsonant[0] == "y") //note6  digraphs ng, ny, ngy
			const T = previousT && (latinConsonant[0] == "h" || latinConsonant[0] == "w" || latinConsonant[0] == "y") //note6 T  - digraphs th, tw, T+combinator ty
			const AKTN = previousA && (latinConsonant[0] == "k" || latinConsonant[0] == "t" || latinConsonant[0] == "n") //MLCTS - digraphs ak -> a-k
			const KY = previousK && (latinConsonant[0] == "y") //akyan/a-kyan
			if (NGY || T || AKTN || KY)
				latinConsonant = "-" + latinConsonant;	//အင်းကွတ်  (in-gut)
			//ကွန်ရက် (kun-yet)
			//ဟက်ဟက်ပက်ပက်ရယ်  (Het-HetPetPet|Y|è)
			//ကတ်ဝက် (kat-wet)
		}
		console.log("latinConsonant: " + latinConsonant)
		console.groupEnd("getLatinConsonant");

		console.group("next only Vowel/EndOfSyllable")
		//example cut at endOfSyllable ကျ်ာ -> ကျ် + ာ =combineable with next vowel/endOfSyllable? put into currentSyllable remove next
		consoleLogDeepCopyObject(myanmarPhonem)
		if (nextMyanmarPhonem && myanmarPhonem.endOfSyllable == "") {
			const nextHasConsonant = nextMyanmarPhonem.consonant != "" || nextMyanmarPhonem.hasConsonantMultichar != ""

			//example ငါးဥ
			//myanmarPhonem.toneSentenceMark=="" && myanmarPhonem.hasToneSentenceMarkAfterEndOfSyllable==""
			if (!nextHasConsonant && nextMyanmarPhonem.vowelType != "Independent") {
				//const nextHasVowel = nextMyanmarPhonem.vowel!="";
				myanmarPhonem.vowel = nextMyanmarPhonem.vowel;
				myanmarPhonem.vowelType = nextMyanmarPhonem.vowelType;
				myanmarPhonem.endOfSyllable = nextMyanmarPhonem.endOfSyllable;
				myanmarPhonem.toneSentenceMark = nextMyanmarPhonem.toneSentenceMark;
				myanmarPhonem.hasToneSentenceMarkAfterEndOfSyllable = nextMyanmarPhonem.hasToneSentenceMarkAfterEndOfSyllable;
				myanmarPhonemArray.splice(currentArrayPos + 1, 1);
			}
		}
		if (myanmarPhonem.vowelType == "Independent") {
			let vowelIndependent = VOWEL_CHARACTERS[type][myanmarPhonem.vowelType][myanmarPhonem.vowel];
			if (myanmarPhonem.vowelType == "Independent" && (currentArrayPos > 0)) {
				vowelIndependent = "-" + vowelIndependent;		//ကြောင်ဥကျဉ် (kyaung-ugyin) / ကြေဧ (KyeE) //ဥက္ကဌ
			}

			latinConsonant += vowelIndependent
		}
		console.groupEnd("next only Vowel/EndOfSyllable")
		return latinConsonant;
	}

	function canBeCombinedWithNextSyllableForSimplifiedStacked(myanmarPhonemArray, currentPos, type = TypeEnums.BGN) {
		console.group("canBeCombinedWithNextSyllableForSimplifiedStacked");
		let result = false;
		const currentMyanmarPhonem = myanmarPhonemArray[currentPos];
		//const previousMyanmarPhonem = myanmarPhonemArray[currentPos - 1];
		const nextMyanmarPhonem = myanmarPhonemArray[currentPos + 1];
		console.log(Object.assign({}, currentMyanmarPhonem));
		console.log(Object.assign({}, nextMyanmarPhonem))
		let nextEndOfSyllableIsOnlyFinal = false;
		let nextCharacterCombinator = false; //ြ
		let currentMedialOfMultichar = "";//InCase of Consonant Multichar ကြ //IPA/Okell special case
		let nextMedialOfMultichar = "";//InCase of Consonant Multichar ကြ //IPA/Okell special case

		if (currentMyanmarPhonem.hasConsonantMultichar && currentMyanmarPhonem.hasConsonantMultichar.includes("ြ"))
			currentMedialOfMultichar = "ြ";

		if (nextMyanmarPhonem) //simplifiedStackedCheck
		{
			if (nextMyanmarPhonem.hasConsonantMultichar) {
				//hack quickfix
				if (nextMyanmarPhonem.hasConsonantMultichar.includes("ြ"))
					nextMedialOfMultichar = "ြ";
				else if (nextMyanmarPhonem.hasConsonantMultichar.includes("ျ"))
					nextMedialOfMultichar = "ျ";
				else if (nextMyanmarPhonem.hasConsonantMultichar.includes("ှ"))
					nextMedialOfMultichar = "ှ";
				else if (nextMyanmarPhonem.hasConsonantMultichar.includes("ွ"))
					nextMedialOfMultichar = "ွ";
			}

			nextEndOfSyllableIsOnlyFinal = (nextMyanmarPhonem.endOfSyllable == "်") || //ယောက်ျ → ယောက်ကျား (YaukKya)
				((nextMyanmarPhonem.hasConsonantCombination == "ြ" || nextMedialOfMultichar == "ြ") && nextMyanmarPhonem.endOfSyllable == "");	//သောကြာနေ့ -> သောက်ကြာနေ့   (ThaukKyaNe)
		}

		let newEndOfSyllable = "";
		//currentMyanmarPhonem.endOfSyllable


		const simplifiedFinalCheckCurrent = currentMyanmarPhonem.endOfSyllable == "" &&
			((currentMyanmarPhonem.consonant != "" && currentMyanmarPhonem.consonant != "အ") ||
				currentMyanmarPhonem.hasConsonantMultichar != "" && currentMyanmarPhonem.vowel != "" && !currentMyanmarPhonem.vowelType == "Independent")
		const simplifiedFinalCheckNext = nextMyanmarPhonem && nextEndOfSyllableIsOnlyFinal &&
			((nextMyanmarPhonem.vowel != "" && nextMyanmarPhonem.vowelType != "Independent") || nextMyanmarPhonem.hasConsonantCombination != "" || nextMedialOfMultichar != "");

		console.log("simplifiedFinalCheckCurrent: " + simplifiedFinalCheckCurrent + ", simplifiedFinalCheckNext: " + simplifiedFinalCheckNext)
		if (nextMyanmarPhonem)
			console.log("nextEndOfSyllableIsOnlyFinal: " + nextEndOfSyllableIsOnlyFinal +
				", nextMyanmarPhonem.vowel: " + nextMyanmarPhonem.vowel +
				", nextMyanmarPhonem.hasConsonantCombination: " + nextMyanmarPhonem.hasConsonantCombination + ", nextMedialOfMultichar: " + nextMedialOfMultichar)
		//example ယောက်ျ → ယောက်ကျား (YaukKya)
		//nextFinal=="်"
		//simplifiedStackedCheck
		if (simplifiedFinalCheckNext && simplifiedFinalCheckCurrent) {
			let currentPhonemString = "";
			//if(previousMyanmarPhonem.consonant !="")
			//	previousPhonemString += previousMyanmarPhonem.consonant;
			//if(previousMyanmarPhonem.hasConsonantMultichar !="")
			//	previousPhonemString += previousMyanmarPhonem.hasConsonantMultichar;
			if (currentMyanmarPhonem.hasConsonantCombination != "") {
				//TODO to check if unicode hack needs to input
				currentPhonemString += currentMyanmarPhonem.hasConsonantCombination;
			}
			if (currentMedialOfMultichar != "")
				currentPhonemString += currentMedialOfMultichar;

			if (currentMyanmarPhonem.vowel != "")
				currentPhonemString += currentMyanmarPhonem.vowel;
			//can be a "Simplified Stacked Characters" test ယောက်ျ → ယောက်ကျား

			console.log("currentPhonemVowelString: " + currentPhonemString);
			const currentPhonemStringWithEndOfSyllable = currentPhonemString + "က်";
			console.log("currentPhonemVowelStringWithEndOfSyllable 'က်' added to: " + currentPhonemStringWithEndOfSyllable);
			const matchEndOfSyllable = currentPhonemStringWithEndOfSyllable.match(REGEX_STARTSWITH_CONSONANT_END_OF_SYLLABLE[type]);
			console.log(matchEndOfSyllable);

			if (matchEndOfSyllable && matchEndOfSyllable[0] && (currentPhonemStringWithEndOfSyllable != "က်" || nextEndOfSyllableIsOnlyFinal) && CONSONANT_END_OF_SYLLABLE[type][matchEndOfSyllable[0]]) {
				newEndOfSyllable = matchEndOfSyllable[0];
				console.log("newEndOfSyllable: " + newEndOfSyllable)

				currentMyanmarPhonem.vowel = "";
				currentMyanmarPhonem.endOfSyllable = newEndOfSyllable;
				nextMyanmarPhonem.endOfSyllable = "";
				result = true;
			}
		}
		//const nextEndOfSyllableExceptionTest = (nextMyanmarPhonem && nextMyanmarPhonem.endOfSyllable);
		let nextIsEndOfSyllableException = falseEndOfSyllable.includes(currentMyanmarPhonem.endOfSyllable);
		const doubleFinal = currentMyanmarPhonem.endOfSyllable != "" && nextMyanmarPhonem && nextMyanmarPhonem.endOfSyllable && nextMyanmarPhonem.endOfSyllable != "" && nextMyanmarPhonem.endOfSyllable.length == 2;
		//example ကျွန်ုပ် -> ကျွန်နုပ်(KyunNôk)
		//ဘတ်စ်ကား (BatSKa)
		//ဘဏ်တိုက် not doublefinal
		if (nextMyanmarPhonem)
			console.log("nextMyanmarPhonem.endOfSyllable: " + nextMyanmarPhonem.endOfSyllable + ", nextIsEndOfSyllableException: " + nextIsEndOfSyllableException)

		const nextOnlyEndOfSyllable = nextMyanmarPhonem && nextMyanmarPhonem.consonant == "" && nextMyanmarPhonem.hasConsonantMultichar == "" && nextMyanmarPhonem.vowel == "" && nextMyanmarPhonem.endOfSyllable != "";
		const isDoubleConsonant = currentMyanmarPhonem.endOfSyllable != "" && nextOnlyEndOfSyllable;
		console.log("doubleFinal: " + doubleFinal + ", isDoubleConsonant: " + isDoubleConsonant + ", nextOnlyEndOfSyllable: " + nextOnlyEndOfSyllable + ", currentMyanmarPhonem.endOfSyllable: " + currentMyanmarPhonem.endOfSyllable)

		//simplifiedDoubleConsonant	
		if (nextMyanmarPhonem &&
			(!doubleFinal) &&
			(nextMyanmarPhonem.consonant == "" && nextMyanmarPhonem.hasConsonantMultichar == "" && nextMyanmarPhonem.endOfSyllable != "" && !nextIsEndOfSyllableException)) {
			//double consonant "stacking"
			const lastConsonantOrFirstIndex = (currentMyanmarPhonem.endOfSyllable.length - 2) >= 0 ? currentMyanmarPhonem.endOfSyllable.length - 2 : 0;
			const newConsonant = currentMyanmarPhonem.endOfSyllable.slice(lastConsonantOrFirstIndex, currentMyanmarPhonem.endOfSyllable.length - 1);
			console.log("newConsonant to insert Into nextPhonem: " + newConsonant);
			console.log("currentMyanmarPhonem.endOfSyllable: " + currentMyanmarPhonem.endOfSyllable);
			if (nextMyanmarPhonem)
				console.log("nextMyanmarPhonem.endOfSyllable: " + nextMyanmarPhonem.endOfSyllable)
			if (newConsonant != "" && !doubleFinal) { // && simplifiedFinalCheckCurrent && simplifiedFinalCheckCurrent
				nextMyanmarPhonem.consonant = newConsonant;
				result = true;
			}

		}
		console.groupEnd("canBeCombinedWithNextSyllableForSimplifiedStacked");
		console.log("canBeCombinedWithNextSyllableForSimplifiedStacked: " + result)
		return result;
	}

	function nextIsStackedCharacter(myannmarUnchangedArray, currentPos = 0) {
		let result = false;
		const nextSyllable = myannmarUnchangedArray[currentPos + 1];

		if (nextSyllable) {
			if (nextSyllable.consonant == "ဿ") {

			}
		}
		return result;
	}
	function canMergeToNewEndOfSyllable(syllable, consonantWithFinal, type = TypeEnums.BGN) {
		let result = false;
		let syllableHasMulticharCombinator = "";
		if (syllable.hasConsonantMultichar.length > 1)
			syllableHasMulticharCombinator = syllable.hasConsonantMultichar.slice(1);
		const newEndOfSyllable = syllable.hasConsonantCombination + syllableHasMulticharCombinator + syllable.vowel + syllable.endOfSyllable + syllable.hasToneSentenceMarkAfterEndOfSyllable + consonantWithFinal;
		console.log("newEndOfSyllable: " + newEndOfSyllable)
		const matchEndOfSyllable = newEndOfSyllable.match(REGEX_STARTSWITH_CONSONANT_END_OF_SYLLABLE[type]);
		console.log(matchEndOfSyllable);
		if (matchEndOfSyllable && matchEndOfSyllable[0] && CONSONANT_END_OF_SYLLABLE[type][matchEndOfSyllable[0]]) {
			result = true;
		}
		/*
			let currentPhonemString = "";
					//if(previousMyanmarPhonem.consonant !="")
					//	previousPhonemString += previousMyanmarPhonem.consonant;
					//if(previousMyanmarPhonem.hasConsonantMultichar !="")
					//	previousPhonemString += previousMyanmarPhonem.hasConsonantMultichar;
					if (currentMyanmarPhonem.hasConsonantCombination != "") {
						//TODO to check if unicode hack needs to input
						currentPhonemString += currentMyanmarPhonem.hasConsonantCombination;
					}
					if (currentMedialOfMultichar != "")
						currentPhonemString += currentMedialOfMultichar;
		
					if (currentMyanmarPhonem.vowel != "")
						currentPhonemString += currentMyanmarPhonem.vowel;
					//can be a "Simplified Stacked Characters" test ယောက်ျ → ယောက်ကျား
		
					console.log("currentPhonemVowelString: " + currentPhonemString);
					const currentPhonemStringWithEndOfSyllable = currentPhonemString + "က်";
					console.log("currentPhonemVowelStringWithEndOfSyllable 'က်' added to: " + currentPhonemStringWithEndOfSyllable);
					const matchEndOfSyllable = currentPhonemStringWithEndOfSyllable.match(REGEX_STARTSWITH_CONSONANT_END_OF_SYLLABLE[type]);
					console.log(matchEndOfSyllable);
		
					if (matchEndOfSyllable && matchEndOfSyllable[0] && (currentPhonemStringWithEndOfSyllable != "က်" || nextEndOfSyllableIsOnlyFinal) && CONSONANT_END_OF_SYLLABLE[type][matchEndOfSyllable[0]]) {
						newEndOfSyllable = matchEndOfSyllable[0];
						console.log("newEndOfSyllable: " + newEndOfSyllable)
		
						currentMyanmarPhonem.vowel = "";
						currentMyanmarPhonem.endOfSyllable = newEndOfSyllable;
						nextMyanmarPhonem.endOfSyllable = "";
						result = true;
					}
		*/
		return result;
	}

	function insertSpecialStackedCharacter(myanmarPhonemArray, currentPos, unchangedSyllableArray, unchangedSyllablePos, type = TypeEnums.BGN) {
		let result = false;
		const currentMyanmarPhonem = myanmarPhonemArray[currentPos];
		const nextMyanmarPhonem = myanmarPhonemArray[currentPos + 1];
		console.group("insertSpecialStackedCharacter check ဿ ss to သ်သ") //ပိဿာ
		if (nextMyanmarPhonem && nextMyanmarPhonem.consonant == "ဿ") {
			const endsWithSpecialStackedCharacter = checkEndsWithUnchangedArrayElement(unchangedSyllableArray, unchangedSyllablePos + 1, compoundWordTest, type);
			console.log("endsWithSpecialStackedCharacter")
			console.log(endsWithSpecialStackedCharacter)
			let mergeEndSyllable = false;
			if (endsWithSpecialStackedCharacter.result) {
				//	mergeEndSyllable = canMergeToNewEndOfSyllable(currentMyanmarPhonem,"သ်", type) //insert new doubleConsonant into array syllable
				let newSyllable = Object.assign({},
					emptySyllable,{consonant:"သ", endOfSyllable:"်"})
				//newSyllable = Object.assign(newSyllable, {consonant:"သ", endOfSyllable:"်"})
				//{ fullPhonem: "", consonant: "သ", hasConsonantMultichar: "", vowel: "", endOfSyllable: "်" }
				console.log(newSyllable)
				let nextSyllable = Object.assign({}, nextMyanmarPhonem);
				nextSyllable.consonant = "သ"
				consoleLogDeepCopyObject(myanmarPhonemArray);
				myanmarPhonemArray.splice(currentPos + 1, 1, nextSyllable);
				myanmarPhonemArray.splice(currentPos + 1, 0, newSyllable);
				consoleLogDeepCopyObject(myanmarPhonemArray);
				result = true;
			}
			console.log("mergeEndSyllable: " + mergeEndSyllable)
		}
		console.groupEnd("insertSpecialStackedCharacter check ဿ ss to သ်သ")
		return result;
	}
	//simplifiedStackedCheck
	function nyaSimplifiedStackedCheck(myanmarPhonemArray, currentPos, unchangedSyllableArray, unchangedSyllablePos, type = TypeEnums.BGN) {
		/* 
		currently only found this example 
		pyin2-nya2 | ပညာ - knowledge. /pyiɴɲa/ 
		spoken ပြဉ်ဉာ
		need more different examples to have a better inferred rule
		*/
		let result = false;
		const currentMyanmarPhonem = myanmarPhonemArray[currentPos];
		const nextMyanmarPhonem = myanmarPhonemArray[currentPos + 1];
		let nextIsNya = false;
		console.group("nyaSimplifiedStackedCheck");
		console.log(nextMyanmarPhonem)
		if (nextMyanmarPhonem && nextMyanmarPhonem.consonant == "ည" && nextMyanmarPhonem.endOfSyllable == "" && nextMyanmarPhonem.vowel == "ာ")
			nextIsNya = true;
		console.log("nextIsNya: " + nextIsNya)
		if (nextIsNya && ((currentMyanmarPhonem.consonant != "" && currentMyanmarPhonem.consonant != "အ") || currentMyanmarPhonem.hasConsonantMultichar != "") && (currentMyanmarPhonem.hasConsonantCombination == "") &&
			currentMyanmarPhonem.vowel == "" && currentMyanmarPhonem.endOfSyllable == "") {
			console.log("change nya phonem values")

			currentMyanmarPhonem.endOfSyllable = "ဉ်";
			//currentMyanmarPhonem.hasConsonantCombination = "ြ";
			nextMyanmarPhonem.consonant = "ဉ";
			result = true;
		}
		console.groupEnd("nyaSimplifiedStackedCheck");


		return result;
	}
	function getFullPhonem(myanmarPhonem) {
		let phonem = "";
		phonem = myanmarPhonem.consonant + myanmarPhonem.hasConsonantMultichar + myanmarPhonem.hasConsonantCombination +
			myanmarPhonem.vowel + myanmarPhonem.toneSentenceMark +
			myanmarPhonem.endOfSyllable +
			myanmarPhonem.hasToneSentenceMarkAfterEndOfSyllable;
		return phonem;
	}

	function checkConsonantMergesWithPreviousSyllable(previousSyllable, currentSyllable, type) {
		let result = false;
		console.group("check is stacked and checkConsonantMergesWithPreviousSyllable")
		if (previousSyllable && currentSyllable && currentSyllable.vowel == "္") {//}.endsWith("္")) {			
			console.log(previousSyllable)
			console.log(currentSyllable)
			let consonantCombination = "";
			if (currentSyllable.hasConsonantMultichar.length > 0) {
				consonantCombination = currentSyllable.hasConsonantMultichar.slice(1);
			} else {
				consonantCombination = currentSyllable.consonantCombination;
			}

			const previousVowel = previousSyllable.vowel + previousSyllable.toneSentenceMark + previousSyllable.endOfSyllable + previousSyllable.hasToneSentenceMarkAfterEndOfSyllable;
			const endOfSyllablePartToCheck = currentSyllable.consonant + "်";
			//if (previousVowel.length > 0 )
			{
				let endOfSyllableToCheck = previousVowel + endOfSyllablePartToCheck;

				const endOfSyllableMergeWithStackedConsonant = CONSONANT_END_OF_SYLLABLE[type][endOfSyllableToCheck];
				if (endOfSyllableMergeWithStackedConsonant) //consonant builds new endOfSyllable
					result = true;
				console.log("checkConsonantMergesWithPreviousSyllable endOfSyllableToCheck: " + endOfSyllableToCheck + ", found EndOfSyllable: " + endOfSyllableMergeWithStackedConsonant + ", result: " + result)
			}
			/*
			else {
				console.log("hasStackedCharacter but no previous vowel to merge with. result: " + result)
			}
			*/


		}
		console.groupEnd("check is stacked and checkConsonantMergesWithPreviousSyllable")
		return result;
	}

	function checkEndsWithUnchangedArrayElement(unchangedSyllableArray, currentPos, ArrayToCheck = [], type = TypeEnums.BGN, checkFromPreviousSyllable = false) {
		//ခင်ဗျာ ခင် ဗျာ
		//လေယာဉ်ပျံ  
		//https://countwordsfree.com/comparetexts
		let result = { result: false, longestMatchingElement: "", syllableLength: 0 };
		const DEBUG = true;
		DEBUG && console.group("checkEndsWithUnchangedArrayElement upto index:  " + currentPos)
		let index = currentPos;
		let syllableLength = 0;
		let stackedCount = 0;
		let wordToCheck = "";

		console.log(" startPos index: " + index)
		if (index >= 0 && index < unchangedSyllableArray.length) {
			if (checkFromPreviousSyllable) {

				index--;
			}

			/*
			let lastSyllable = unchangedSyllableArray[index];
			let previousSyllable;
			
			let additionalSyllable = false; //merge endOfSyllable or abbreviation additional syllable
			if (index - 1 >= 0) {
				previousSyllable = unchangedSyllableArray[index - 1];
				if (lastSyllable.vowel == "္") {
					let consonantCombination = "";
					if (previousSyllable.hasConsonantMultichar != "") {
						consonantCombination = previousSyllable.hasConsonantMultichar.slice(1);
					} else {
						consonantCombination = previousSyllable.consonantCombination;
					}
					let endOfSyllableToCheck = previousSyllable.vowel + previousSyllable.toneSentenceMark + previousSyllable.endOfSyllable + previousSyllable.hasToneSentenceMarkAfterEndOfSyllable +
						lastSyllable.consonant;

					if (additionalSyllable) {
						stackedCount++;
					}
				}

			}
*/
			//DEBUG && console.log(lastSyllable)
			//wordToCheck = getFullPhonem(lastSyllable);
			//let REGEX_NOUNS_ENDSWITH_WORD = new RegExp("(" + wordToCheck + ")$");
			let matchEndsWith //= ArrayToCheck.find(item => item.endsWith(wordToCheck))//wordToCheck.match(REGEX_ENDSWITH_NOUN);
			let matchNext = true;

			//if("ယာဉ်"== wordToCheck)
			//	console.log("wordToCheck match")
			//const test="ယာဉ်"
			//	DEBUG && console.log("wordToCheck before while loop: " + wordToCheck)//+ ", should be matched with : ယာဉ်: " + (wordToCheck == test)
			//	DEBUG && console.log("first possible match endsWith wordToCheck: " + matchEndsWith)
			//console.log(wordToCheck +" - " + test)
			//console.log(wordToCheck.length +" - " + test.length)
			//console.log((wordToCheck == test) )
			//console.log(NOUNS.indexOf(wordToCheck))
			//console.log(NOUNS.indexOf(test))
			//console.log(NOUNS)
			wordToCheck = getFullPhonem(unchangedSyllableArray[index]); //first index
			syllableLength++;
			while (matchEndsWith || matchNext) {
				matchNext = false;
				//syllableLength++;
				const hasExactMatch = wordToCheck.length > 0 && matchWordVariations(wordToCheck, ArrayToCheck, 0); //ArrayToCheck.includes(wordToCheck) //about ~200 console output lines longer to check valid word with negation
				DEBUG && console.log("hasExactMatch: " + hasExactMatch + ", wordToCheck: " + wordToCheck)
				if (wordToCheck.length > 0 && hasExactMatch) {
					/*
					result.value = ArrayToCheck[hasArrayIndex];
					result.index = index;
					*/
					result.result = true;
					result.syllableLength = syllableLength - stackedCount;
					result.longestMatchingElement = wordToCheck;
					DEBUG && console.log("exact match: " + wordToCheck + ", hasExactMatch: " + hasExactMatch);
					//matchNext = false;
				}


				if (index > 0) {
					const currentUnchangedSyllable = unchangedSyllableArray[index];
					if (index > 0 && currentUnchangedSyllable) { //? which case undefined? example ဗြဟ္မစိုရ်တရားဗျား

						let previousLastSyllable;
						let previousSyllableString = "";
						const previousIndex = index - 1;
						let isAbbreviation = false
						if (previousIndex >= 0) {
							previousLastSyllable = unchangedSyllableArray[previousIndex];
							if (previousLastSyllable)
								previousSyllableString = getFullPhonem(previousLastSyllable);
							const stackedDiff = hasStackedDiff(unchangedSyllableArray, previousIndex, type);
							console.log("stackedDiff: " + stackedDiff)


							if (stackedDiff > 0) {//abbreviation 
								stackedCount++;
								console.log("backwards stackedCharacter count: " + stackedCount);
							} else {
								console.log(previousLastSyllable)
								console.log(currentUnchangedSyllable)
								if (previousLastSyllable.vowel == "္" && previousSyllableString.length > 1) { //သ္မီး = သ+္ +မီ
									let isMergedEndOfSyllable = false;
									if (previousIndex - 1 >= 0) {
										isMergedEndOfSyllable = checkConsonantMergesWithPreviousSyllable(unchangedSyllableArray[previousIndex - 1], previousLastSyllable, type)
									}
									if (!isMergedEndOfSyllable)
										isAbbreviation = true;
								}

							}
							if (isAbbreviation && stackedDiff == 0) {

							}
						}
						console.log("isAbbreviation: " + isAbbreviation)

						//const previousSyllable = unchangedSyllableArray[previousIndex];

						if (previousLastSyllable) {
							//previousSyllableString = getFullPhonem(previousLastSyllable);
							console.log("previousSyllableString: " + previousSyllableString)
							//if(previousSyllableString!="္")
							//	syllableLength++;	
							console.log("temp syllableLength: " + syllableLength)
							if (isAbbreviation) {
								//	syllableLength++;	
								console.log("isAbbreviation: " + previousSyllableString)
								previousSyllableString = previousSyllableString.slice(0, -1);
								console.log("isAbbreviation string without stacked character " + previousSyllableString)
							} else {

							}

						}

						DEBUG && console.log(currentUnchangedSyllable);
						/*
						let endOfSyllableMergeWithStackedConsonant = false; //merge endOfSyllable or abbreviation additional syllable
						endOfSyllableMergeWithStackedConsonant = checkConsonantMergesWithPreviousSyllable(previousLastSyllable, previousSyllable, type)
						
						if (!endOfSyllableMergeWithStackedConsonant) {//abbreviation 
							stackedCount++;
							console.log("backwards stackedCharacter count: " + stackedCount);
						}*/

						DEBUG && console.log(previousLastSyllable);
						DEBUG && console.log("previousSyllable to add to endsWith: " + previousSyllableString)
						if (previousSyllableString.length > 0) // && !isAbbreviation
							syllableLength++;
						wordToCheck = previousSyllableString + wordToCheck;
						//matchEndsWith = ArrayToCheck.find(item => item.endsWith(wordToCheck))//wordToCheck.match(REGEX_ENDSWITH_NOUN);
						matchEndsWith = matchWordVariations(wordToCheck, ArrayToCheck, -1);
						DEBUG && console.log("endswith wordToCheck: " + wordToCheck + ", matchEndsWith: " + matchEndsWith)

						if (matchEndsWith) {
							//syllableLength++
							DEBUG && console.log("endswith matchingWord: " + matchEndsWith)
						}
						if (!matchEndsWith) {
							DEBUG && console.log("no matching word endsWith: " + wordToCheck)
							DEBUG && console.log("LongestMatching Syllable is: " + result.longestMatchingElement)
							matchNext = false;
						}
					}
				}
				else {
					DEBUG && console.log("no more syllable infront of word. LongestMatching Syllable is: " + result.longestMatchingElement)
					matchEndsWith = false;
					matchNext = false;
				}

				index--;
				//syllableLength++;
			}
		}
		DEBUG && console.groupEnd("checkEndsWithUnchangedArrayElement upto index:  " + currentPos)
		DEBUG && console.log(result);
		return result;
	}

	function nextCanBeNasalized(myanmarPhonemArray, currentPos, previousWord = { value: "", index: -1 }, currentAndNextSyllableIsWord = false, nextIsNoun = false, previousCharacterIndex = 0, stackedIndexes = []) {
		let result = false;


		const DEBUG = true;
		DEBUG && console.group("nextCanBeNasalized")
		const currentMyanmarPhonem = myanmarPhonemArray[currentPos]
		const nextMyanmarPhonem = myanmarPhonemArray[currentPos + 1];
		const endOfSyllable = currentMyanmarPhonem.endOfSyllable;
		const hasNasalizedFinal = CONSONANT_END_OF_SYLLABLE_N_TYPE[endOfSyllable];
		DEBUG && console.log("endOfSyllable: " + endOfSyllable)
		DEBUG && console.log("hasNasalizedFinal: " + hasNasalizedFinal)
		//	const fullSyllable = getFullPhonem(currentMyanmarPhonem);
		consoleLogDeepCopyObject(currentMyanmarPhonem)
		console.log(nextMyanmarPhonem)
		console.log("nextIsNoun: " + nextIsNoun)
		//တင်ပါ ခင်ဗျား //not because of ? currentAndNextSyllableIsWord
		//ကျေးဇူးတင်ပါတယ်။  - တင် is not a noun but a verb since it follows after a possible verb with a tense form of ပါတယ်
		if (nextMyanmarPhonem && nextIsNoun) {
			consoleLogDeepCopyObject(nextMyanmarPhonem)
			//DEBUG && console.log(nextMyanmarPhonem);
			DEBUG && console.log("nextMyanmarPhonem.consonant: " + nextMyanmarPhonem.consonant)
			DEBUG && console.log("nextMyanmarPhonem.hasConsonantCombination: " + nextMyanmarPhonem.hasConsonantCombination)
			const currentConsonantIsStop = groupedStopConsonants[currentMyanmarPhonem.consonant];
			const nextConsonantIsStop = groupedStopConsonants[nextMyanmarPhonem.consonant];
			DEBUG && console.log(groupedStopConsonantsValue);

			const currentVoicedConsonantIsStopObject = groupedStopConsonantsValue.find(item => {
				return (item.voiced == currentMyanmarPhonem.consonant)
			}
			);
			let nextVoicedConsonantIsStopObject = groupedStopConsonantsValue.find(item => {
				return (item.voiced == nextMyanmarPhonem.consonant)
			}
			);

			/*
			negativ test?
			In some compound works, the phoneme /dʒ/, when following the nasalized final /ɰ̃/, can shift to a /j/ sound: 
			"blouse" (အင်္ကျီ angkyi): /èɪɰ̃dʒí/] → [èɪ̃jí].
			*/

			//if (nextMyanmarPhonem.hasConsonantCombination == "" )
			/* //change for previous Latin Vowel || N || ng
			if (nextConsonantIsStop) {
				DEBUG && console.log(nextConsonantIsStop)
				nextMyanmarPhonem.consonant = nextConsonantIsStop.voiced;
			}
			*/

			const nextConsonantVoiced = false;
			const currentSyllable = getFullPhonem(currentMyanmarPhonem);
			const characterIndexAfterSyllable = currentSyllable.length + previousCharacterIndex;
			const nextConsonantIsStacked = stackedIndexes.includes(characterIndexAfterSyllable);
			console.log("nextConsonantIsStacked: " + nextConsonantIsStacked + ", previousCharacterIndex: " + characterIndexAfterSyllable)
			console.log("stackedIndexes: " + stackedIndexes)
			//stacked test examples တနင်္ဂနွေနေ့ မင်္ဂလာပါ။ ခင်ဗျာ
			//ကံကြမ္မာ //nextMyanmarPhonem.hasConsonantCombination == ""

			const nextHasNoConsonantCombination = !(nextMyanmarPhonem.hasConsonantCombination != "" ||
				nextMyanmarPhonem.hasConsonantMultichar != "")
			console.log("previousWord.value" + previousWord.value + ", hasNasalizedFinal: " + hasNasalizedFinal + ", nextConsonantIsStacked: " + nextConsonantIsStacked)
			if (previousWord.value != "" && hasNasalizedFinal && !nextConsonantIsStacked)//has previous known noun //&& nextHasNoConsonantCombination
			{
				DEBUG && console.log(nextConsonantIsStop)
				DEBUG && console.log(nextVoicedConsonantIsStopObject)
				const voicedOrNasal = "nasal";//currentMyanmarPhonem.hasToneSentenceMarkAfterEndOfSyllable.includes("း")?"voiced":"nasal";
				console.log("voicedOrNasal: " + voicedOrNasal)
				//phonemes /p, pʰ, b, t, tʰ, d/, https://en.wikipedia.org/wiki/Burmese_phonology#Burmese_voicing_sandhi
				//not first and second row of each aspirated/unaspirated stopConsonant alternative make extra test for unvoiced, voiced consonant of 3-5th row of aspirated/unaspirated stopConsonants
				const isStopException = ["က", "စ", "ခ", "ဆ", "ဂ", "ဇ", "ဃ", "ဈ", "သ"] //တံခါး
				// additional သ		"သ": { voiced: "ဒ" }, //https://en.wikipedia.org/wiki/Burmese_phonology#Burmese_voicing_sandhi //, nasal: "န"  //?doublecheck has nasalise consonant exception?
				if ((nextConsonantIsStop || nextVoicedConsonantIsStopObject) && !isStopException.includes(nextMyanmarPhonem.consonant)) {//&& !currentAndNextSyllableIsWord 
					result = true;
				}
				console.log("if next nasalised set current to nasalised too. nextMyanmarPhonem.consonant: " + nextMyanmarPhonem.consonant)
				/*
				if (nextConsonantIsStop || nextVoicedConsonantIsStopObject) {
					DEBUG && console.log(currentConsonantIsStop)
					DEBUG && console.log(currentVoicedConsonantIsStopObject)
					console.log(currentMyanmarPhonem)
					if (currentConsonantIsStop ) { //&& currentMyanmarPhonem.endOfSyllable != "င်"
						currentMyanmarPhonem.consonant = currentConsonantIsStop["voiced"];
					}
					 else if (currentVoicedConsonantIsStopObject) {
						currentMyanmarPhonem.consonant = currentVoicedConsonantIsStopObject["voiced"];
					}
					DEBUG && console.log("new currentMyanmarPhonem.consonant: " + currentMyanmarPhonem.consonant)
				}
				*/
			}

		}
		DEBUG && console.groupEnd("nextCanBeNasalized")

		return result
	}


	function adjustForNasalizedFinal(myanmarPhonemArray, currentPos, nextIsNasalized = false) {//, previousWord = { value: "", index: -1 }, currentAndNextSyllableIsWord = false, nextIsNoun = false, previousCharacterIndex = 0, stackedIndexes = []) {
		const DEBUG = true;
		//const nextIsNasalized = nextCanBeNasalized(myanmarPhonemArray, currentPos, previousWord , currentAndNextSyllableIsWord , nextIsNoun , previousCharacterIndex , stackedIndexes )
		DEBUG && console.group("adjustForNasalizedFinal")

		console.log("nextIsNasalized: " + nextIsNasalized)
		const nextPosSmallerOrEqualArrayLength = (currentPos + 1) < myanmarPhonemArray.length;
		if (nextIsNasalized && nextPosSmallerOrEqualArrayLength) {
			const nextMyanmarPhonem = myanmarPhonemArray[currentPos + 1];
			if (nextMyanmarPhonem) {
				const nextConsonantIsStop = groupedStopConsonants[nextMyanmarPhonem.consonant];
				const nextVoicedConsonantIsStopObject = groupedStopConsonantsValue.find(item => {
					return (item.voiced == nextMyanmarPhonem.consonant)
				}
				);
				const voicedOrNasal = "nasal";//currentMyanmarPhonem.hasToneSentenceMarkAfterEndOfSyllable.includes("း")?"voiced":"nasal";
				if (nextConsonantIsStop) {//&& !currentAndNextSyllableIsWord
					nextMyanmarPhonem.consonant = nextConsonantIsStop[voicedOrNasal];
				}
				else if (nextVoicedConsonantIsStopObject) {
					nextMyanmarPhonem.consonant = nextVoicedConsonantIsStopObject[voicedOrNasal];
				}
			}

		}
		DEBUG && console.groupEnd("adjustForNasalizedFinal")
	}

	function hasStackedDiff(syllableArray, positionToCheck, type) {
		console.group("hasStackedDiff")
		let stackedDiff = 0;
		let currentSyllable = syllableArray[positionToCheck];
		const nextIndex = positionToCheck + 1;
		if (nextIndex < syllableArray.length) {

			let nextSyllable = syllableArray[positionToCheck + 1];
			let currentSyllableText = getFullPhonem(currentSyllable);
			let nextSyllableText;
			if (nextSyllable)
				nextSyllableText = getFullPhonem(nextSyllable);
			console.log(currentSyllable)
			console.log("currentSyllableText: " + currentSyllableText)


			const kinzaMergeIntoPrevious = currentSyllableText == "္";//|| nextSyllableOnlyStackedCharacter
			const currentSyllableEndsWithStackedCharacter = currentSyllableText.endsWith("္");
			/*
					if (!kinzaMergeIntoPrevious ) {  //abbreviation? သ္မီး
						stackedDiff++;
						//if(nextSyllableOnlyStackedCharacter)
						{
							console.log("skip empty syllable. only stacked symbol")
							//	stackedDiff++;
						}
					}
					else*/
			{

				if (nextSyllable) {
					const nextSyllableOnlyStackedCharacter = nextSyllableText == "္";  // မင်္ဂ 
					const syllableEndsWithStackedCharacter = nextSyllableText.endsWith("္");
					if (syllableEndsWithStackedCharacter) {
						console.group("check currentSyllable nextSyllable")
						console.log(currentSyllable)
						console.log(nextSyllable)
						const reducedConsonantAtStackedCharacter = checkConsonantMergesWithPreviousSyllable(currentSyllable, nextSyllable, type)
						console.log("reducedConsonantAtStackedCharacter: " + reducedConsonantAtStackedCharacter)
						console.groupEnd("check currentSyllable nextSyllable")

						if (reducedConsonantAtStackedCharacter)
							stackedDiff++; //abbreviation: not merged into previous endOfSyllable
					}
					if (nextSyllableOnlyStackedCharacter) {
						console.log("nextSyllable င် was already merged. Only empty stacked character syllable left")
						stackedDiff++;
					}

				}

			}
		}
		//const nextIndex=positionToCheck+1;
		//if(nextIndex< syllableArray.length && getFullPhonem(syllableArray[nextIndex]) == "္") stackedDiff++
		//if(unadjustdedIndexPosition+1< unchangedSyllableArray.length && getFullPhonem(unchangedSyllableArray[unadjustdedIndexPosition+1]) == "္") stackedDiff++;
		console.groupEnd("hasStackedDiff")
		return stackedDiff;
	}

	/**
	 *
	 *
	 * @param {*} [word=""]
	 * @param {*} [ARRAYELEMENT=[]]
	 * @param {number} [type=0] 0 exact, 1 from start, -1 from end
	 */
	function matchWord(wordToCheck = "", ARRAYELEMENT = [], type = 0) {
		let regex;
		let result = false;

		switch (type) {
			case 0: regex = new RegExp("^" + wordToCheck + "$");
				break;
			case -1: regex = new RegExp(wordToCheck + "$");
				break;
			case 1: regex = new RegExp("^" + wordToCheck);
				break;
		}

		result = ARRAYELEMENT.find(item => regex.test(item))

		return result;
	}
	/**
	 *
	 *
	 * @param {string} [wordToCheck=""]
	 * @param {*} [ARRAYELEMENT=[]]
	 * @param {number} [type=0] 0= exact; 1= startsWith; -1=endsWith
	 * @returns boolean
	 */
	function matchWordVariations(wordToCheck = "", ARRAYELEMENT = [], type = 0) {
		let result = false;
		const DEBUG = false;
		DEBUG && console.group("matchWordVariations")

		result = matchWord(wordToCheck, ARRAYELEMENT, type);//?true:false;
		DEBUG && console.log("exact matchWord: " + wordToCheck + ", result: " + result)
		if (!result) {
			const reSeperator = new RegExp("မ", "g");//Unicode char "္"

			let negationMatchIndexes = wordToCheck.matchAll(reSeperator); // /မ/g
			DEBUG && console.log("count seperator indexes : " + negationMatchIndexes)
			DEBUG && console.log(negationMatchIndexes)
			for (const match of negationMatchIndexes) {
				DEBUG && console.log(match)
				const index = match.index;
				const wordWithoutSeperator = wordToCheck.slice(0, index) + wordToCheck.slice(index + 1);
				DEBUG && console.log("wordToCheck: " + wordToCheck + ", index: " + index + ", wordWithoutSeperator: " + wordWithoutSeperator)
				result = matchWord(wordWithoutSeperator, ARRAYELEMENT, type)
				if (result) {
					//result = true;
					break;
				}
			}
		}
		DEBUG && console.groupEnd("matchWordVariations")
		return result
	}

	/**
	 *
	 *
	 * @param {*} unchangedSyllableArray
	 * @param {*} startPos
	 * @param {*} [ARRAYELEMENT=[]]
	 * @param {*} [type=TypeEnums.BGN]
	 * @param {boolean} [startFromNextElement=false]
	 * @returns
	 */
	function checkIsLongestArrayElement(unchangedSyllableArray, startPos, ARRAYELEMENT = [], type = TypeEnums.BGN, startFromNextElement = false) {
		let wordToCheck = "";
		let startsWithNoun = true;
		let isNoun = { result: false, longestElement: "", syllableLength: 0, syllableLengthUnchangedArray: 0, maNegation: false }; //longestMatchingNoun
		let positionToCheck = startPos;
		let syllableEndsWithStackedCharacter = false;
		//let currentIndexAfterSyllable = processedIndexAfterSyllable;
		let stackedDiff = 0;
		console.group("checkIsLongestArrayElement start from position : " + startPos + ", unchangedSyllableArray.length: " + unchangedSyllableArray.length)
		if (positionToCheck >= 0 && positionToCheck < unchangedSyllableArray.length) {
			if (startFromNextElement) {

				//stackedDiff += hasStackedDiff(unchangedSyllableArray, positionToCheck, type);
				console.log("stackedDiff: " + stackedDiff)
				positionToCheck++;

				//stackedDiff += hasStackedDiff(unchangedSyllableArray, positionToCheck, type);
				//	positionToCheck += stackedDiff;
				//	console.log("check from next syllable stackedDiff: " + stackedDiff);
			}
			if (positionToCheck + 1 < unchangedSyllableArray.length)
				if (getFullPhonem(unchangedSyllableArray[positionToCheck]) == "္") positionToCheck++; //skip emptyStackedCharacterSyllable

			console.log("check from next index?: " + positionToCheck + ", original startPos: " + startPos + ", startFromNextElement: " + startFromNextElement)
			while (startsWithNoun && positionToCheck < unchangedSyllableArray.length) {
				console.log("positionToCheck: " + positionToCheck)
				console.log(unchangedSyllableArray[positionToCheck])
				let syllableAtPosition = getFullPhonem(unchangedSyllableArray[positionToCheck]);
				console.log("currentSyllable: " + syllableAtPosition);

				const hasToneSentenceMark = syllableAtPosition.endsWith("း") ? "း" : "";
				let syllableWithoutMedial = syllableAtPosition.replace("ွ", "");
				syllableAtPosition = syllableAtPosition.replace("း", "");

				syllableEndsWithStackedCharacter = syllableAtPosition.endsWith("္") && syllableAtPosition != "္";

				console.log("currentSyllable: " + syllableAtPosition +
					", syllableEndsWithStackedCharacter: " + syllableEndsWithStackedCharacter)

				//currentIndexAfterSyllable += syllableAtPosition.length;
				console.log("wordToCheck before syllableAtPosition: " + wordToCheck)
				const wordToCheckWithoutMedial = wordToCheck + syllableWithoutMedial;
				wordToCheck += syllableAtPosition;

				if (syllableEndsWithStackedCharacter) {
					console.log("currentIsStacked for syllableAtPosition: " + syllableAtPosition)
					//currentIndexAfterSyllable++;
					const characterLengthWithoutFinalAndLastConsonant = wordToCheck.length - 1;
					//let lastCharacter = wordToCheck[characterLengthWithoutFinalAndLastConsonant]; 
					wordToCheck = wordToCheck.slice(0, characterLengthWithoutFinalAndLastConsonant);
					wordToCheckWithoutMedial = wordToCheckWithoutMedial.slice(wordToCheckWithoutMedial.length - 1);
					console.log("wordToCheck: " + wordToCheck)
				}

				/*
				const regexWordToneSentenceMark = new RegExp("^" + wordToCheck);//+ "[" + hasToneSentenceMark + "]?"
				const regexWordWithoutMedialToneSentenceMark = new RegExp("^" + wordToCheckWithoutMedial); //+ "[" + hasToneSentenceMark + "]?"
				const nounStartWithsWordToCheck = ARRAYELEMENT.find(item => regexWordToneSentenceMark.test(item));  //item.startsWith(wordToCheck)
				const nounStartWithsWordWithoutMedial = ARRAYELEMENT.find(item => regexWordWithoutMedialToneSentenceMark.test(item)); //item.startsWith(wordToCheckWithoutMedial)
				*/
				const nounStartWithsWordToCheck = matchWordVariations(wordToCheck + "[" + hasToneSentenceMark + "]?", ARRAYELEMENT, 1)
				const nounStartWithsWordWithoutMedial = matchWordVariations(wordToCheckWithoutMedial + "[" + hasToneSentenceMark + "]?", ARRAYELEMENT, 1)


				console.log("nounStartWithsWordToCheck: " + nounStartWithsWordToCheck + ", nounStartWithsWordWithoutMedial: " + nounStartWithsWordWithoutMedial)
				const nextStartsWithNoun = nounStartWithsWordToCheck || nounStartWithsWordWithoutMedial;
				console.log("wordToCheck: " + wordToCheck + ", hasToneSentenceMark: " + hasToneSentenceMark + ", nextStartsWithNoun: " + nextStartsWithNoun);
				if (nextStartsWithNoun && nextStartsWithNoun.length > 0) {
					console.group("exact noun matchTest for word: " + wordToCheck + ", positionToCheck: " + positionToCheck)
					/*	const regexExactWordToneSentenceMark = new RegExp("^" + wordToCheck + "[" + hasToneSentenceMark + "]?$");
						const regexExactWordWithoutMedialToneSentenceMark = new RegExp("^" + wordToCheckWithoutMedial + "[" + hasToneSentenceMark + "]?$");
						const nounExactWordToCheck = ARRAYELEMENT.find(item => regexExactWordToneSentenceMark.test(item))
						const nounStartWithsWordWithoutMedial = ARRAYELEMENT.find(item => regexExactWordWithoutMedialToneSentenceMark.test(item)) //item.startsWith(wordToCheckWithoutMedial)
						*/

					const nounExactWordToCheck = matchWordVariations(wordToCheck + "[" + hasToneSentenceMark + "]?", ARRAYELEMENT, 0)
					const nounStartWithsWordWithoutMedial = matchWordVariations(wordToCheckWithoutMedial + "[" + hasToneSentenceMark + "]?", ARRAYELEMENT, 0)


					const hasNoun = nounExactWordToCheck;////NOUNS.indexOf(wordToCheck) >= 0;
					const hasNounWithoutMedial = nounStartWithsWordWithoutMedial;// NOUNS.indexOf(wordToCheckWithoutMedial) >= 0;
					console.log(hasNoun)
					console.log("ARRAYELEMENT.includes(wordToCheck): " + ARRAYELEMENT.includes(wordToCheck));
					console.log("hasNoun: " + hasNoun + ", hasNounWithoutMedial: " + hasNounWithoutMedial + ", positionToCheck: " + positionToCheck + ", startPos: " + startPos)
					if (hasNoun || hasNounWithoutMedial) {
						isNoun.result = true;
						isNoun.syllableLengthUnchangedArray = positionToCheck - (startPos) + 1;
						if (startFromNextElement)
							isNoun.syllableLengthUnchangedArray--;
						isNoun.syllableLength = isNoun.syllableLengthUnchangedArray - stackedDiff;


						if (hasNoun)
							isNoun.longestElement = hasNoun;
						if (nounStartWithsWordWithoutMedial)
							isNoun.longestElement = nounStartWithsWordWithoutMedial;


						console.log("has found noun - hasNoun: " + ARRAYELEMENT[ARRAYELEMENT.indexOf(wordToCheck)] + ", nounSyllables count: " + isNoun.syllableLength);
					}
					console.groupEnd("exact noun matchTest")
				}
				else {
					startsWithNoun = false;
				}



				positionToCheck++;
				console.log(" positionToCheck < unchangedSyllableArray.length: " + (positionToCheck < unchangedSyllableArray.length))

			}
		}

		console.groupEnd("checkIsLongestArrayElement start from position : " + startPos)
		console.log(isNoun);
		return isNoun;
	}


	function halfVowelCharacter(type = TypeEnums.BGN) {
		let vowel = "";
		if (type == TypeEnums.IPA)
			vowel = "ə"	//Minor syllables schwa (ə)
		else //bgn/okell
			vowel = "ă";
		return vowel;
	}

	function getHalfVowel(myanmarPhonemArray, currentPos, nextIsNoun = false, nextIsNasalized = false, type = TypeEnums.BGN, spoken = true) {
		console.group("HalfVowel")
		let vowel = "";
		const currentMyanmarPhonem = myanmarPhonemArray[currentPos];
		const nextMyanmarPhonem = myanmarPhonemArray[currentPos + 1];
		const typeHasMinorSyllableChange = (type == TypeEnums.IPA || type == TypeEnums.BGN || type == TypeEnums.BGNPCGN || type == TypeEnums.Okell)

		const endOfSyllable = currentMyanmarPhonem.endOfSyllable;
		let hasFinalMark = false;
		let nextEndOfSyllable = "";
		if (nextMyanmarPhonem)
			nextEndOfSyllable = nextMyanmarPhonem.endOfSyllable;
		let hasEndOfSyllable = false;

		if (endOfSyllable)
			hasEndOfSyllable = (endOfSyllable && endOfSyllable != "");
		const doubleConsonant = (endOfSyllable == "်");

		console.log("typeHasMinorSyllableChange: " + typeHasMinorSyllableChange);
		const consonantNotA = (currentMyanmarPhonem.consonant != "" && currentMyanmarPhonem.consonant != "အ") || currentMyanmarPhonem.hasConsonantMultichar != "";
		const consonantMultichar_hasConsonantCombination = currentMyanmarPhonem.hasConsonantMultichar.slice(1)
		console.log("consonantMultichar_hasConsonantCombination: " + consonantMultichar_hasConsonantCombination);
		const syllableAfterConsonant = consonantMultichar_hasConsonantCombination + currentMyanmarPhonem.hasConsonantCombination + currentMyanmarPhonem.vowel + currentMyanmarPhonem.toneSentenceMark
		//currentMyanmarPhonem.endOfSyllable + currentMyanmarPhonem.hasToneSentenceMarkAfterEndOfSyllable;
		//? openSyllable only vowel dependend?
		//?const syllableAfterConsonant = currentMyanmarPhonem.vowel + currentMyanmarPhonem.toneSentenceMark
		console.log("for current syllable after Consonant to test " + syllableAfterConsonant)
		const isOpenSyllable = openSyllableWithoutGlideList.includes(syllableAfterConsonant) || syllableAfterConsonant == "" //? လက်ဖက်ရည် //openSyllableList openSyllableWithoutGlideList
		console.log("is openSyllable: " + isOpenSyllable)
		//ခင်ဗျာ
		//Sandhi pronounciation
		if (spoken && typeHasMinorSyllableChange && nextMyanmarPhonem) {
			console.group("MinorSyllableHalfVowel")

			//Minor Syllables check
			//		if (nextMyanmarPhonem)
			//			console.log("next endOfSyllable: " + nextMyanmarPhonem.endOfSyllable)		
			//console.log(nextMyanmarPhonem);
			//console.log('endOfSyllable == "်": ' + doubleConsonant)
			if (!doubleConsonant &&
				consonantNotA) {

				const currentNoEndOfSyllable = !hasEndOfSyllable;//currentMyanmarPhonem.endOfSyllable == "";
				const nextEndOfSyllable = nextMyanmarPhonem.endOfSyllable != "";

				const isMinorUtoA = nextMyanmarPhonem &&
					currentNoEndOfSyllable &&
					(currentMyanmarPhonem.vowel == "ု" ||
						currentMyanmarPhonem.vowel == "ူ" ||
						currentMyanmarPhonem.vowel == "");
				const nextEndSyllableWithoutVowel = (nextEndOfSyllable && nextMyanmarPhonem.vowel == "" && nextMyanmarPhonem.hasConsonantCombination == "");
				const nextVowelOrEndOfSyllable = nextMyanmarPhonem &&
					nextEndSyllableWithoutVowel ||
					(!nextEndOfSyllable && nextMyanmarPhonem.vowel != "");
				const minorAndCL = (isMinorUtoA || (currentMyanmarPhonem.toneSentenceMark == "း")) && nextVowelOrEndOfSyllable;

				const currentMyanmarPhonemIsOnlyKinza = ((currentMyanmarPhonem.endOfSyllable == "") ||
					(currentMyanmarPhonem.endOfSyllable != "" && currentMyanmarPhonem.endOfSyllable == "င်")) &&
					currentMyanmarPhonem.vowel == "" && currentMyanmarPhonem.hasConsonantCombination == "" && consonantMultichar_hasConsonantCombination == "" //ခင်ဗျား KimBya -> KaMya
				const nextPhonemToneMark = nextMyanmarPhonem && nextMyanmarPhonem.toneSentenceMark == "း";
				const currentKinzaNextToneSentenceMark = currentMyanmarPhonemIsOnlyKinza && nextPhonemToneMark


				const nextSyllableAfterConsonant = nextMyanmarPhonem.hasConsonantCombination + nextMyanmarPhonem.vowel + nextMyanmarPhonem.toneSentenceMark //+
				//nextMyanmarPhonem.endOfSyllable + nextMyanmarPhonem.hasToneSentenceMarkAfterEndOfSyllable;
				const nextIsOpenSyllable = openSyllableWithoutGlideList.includes(nextSyllableAfterConsonant) || nextSyllableAfterConsonant == ""; //|| nextSyllableAfterConsonant=="က်" //openSyllableList openSyllableWithoutGlideList
				const nextExceptions = (nextMyanmarPhonem.endOfSyllable == "") &&
					((nextMyanmarPhonem.hasConsonantCombination.includes("ွ") ||
						nextMyanmarPhonem.hasConsonantCombination.includes("ျ") ||
						nextMyanmarPhonem.vowel.includes("း")  //vowel tone high
						//nextMyanmarPhonem.toneSentenceMark.includes("း") //စကား /səɡà ~ -zəɡà/ vs ကိုကိုး /kokò/
					))
				let nextDoubleConsonant = false;
				const nextMuted = (nextMyanmarPhonem.consonant == "လ" && nextMyanmarPhonem.endOfSyllable == "်")
				if (nextMyanmarPhonem.consonant != "" && nextMyanmarPhonem.endOfSyllable == "်" && !nextMuted)//လ် mute
					nextDoubleConsonant = true;
				const glides = ["ွ", "ျ", "ြ"] //Onset clusters only when the second consonant is a glide /w/ or /j/
				const nextHasGlides = nextMyanmarPhonem.hasConsonantCombination != "";//glides.some(exception => nextMyanmarPhonem.hasConsonantCombination.includes(exception));
				const exceptions = ["ါ", "ွ", "ှ"];//not in syllable rhyme
				const hasEndSyllableException = exceptions.some(exception => nextMyanmarPhonem.endOfSyllable.includes(exception));

				//Syllable rhymes -> nextFinalWithoutExceptions
				const nextFinalWithoutExceptions = nextMyanmarPhonem.endOfSyllable != "" &&
					(nextMyanmarPhonem.endOfSyllable != "င်") &&
					!hasEndSyllableException &&
					(nextMyanmarPhonem.hasConsonantCombination == "");
				//isOpenSyllable || // not properly working together with  ခုနှစ် KuHnit 7 ဆယ်  vs သူငယ်(ThäNge)[friend]
				//(isOpenSyllable && nextIsOpenSyllable) ||
				console.log("currentMyanmarPhonem")
				consoleLogDeepCopyObject(currentMyanmarPhonem);
				console.log("Minor Syllables check")
				consoleLogDeepCopyObject(nextMyanmarPhonem);
				console.log("isOpenSyllable: " + isOpenSyllable +
					", currentMyanmarPhonemIsOnlyKinza: " + currentMyanmarPhonemIsOnlyKinza +
					", hasEndOfSyllable: " + hasEndOfSyllable
				)
				console.log("nextMyanmarPhonem.endOfSyllable: " + nextMyanmarPhonem.endOfSyllable +
					", hasEndSyllableException: " + hasEndSyllableException +
					//	", nextEndSyllableWithoutVowel: " + nextEndSyllableWithoutVowel +
					", nextMyanmarPhonem.hasConsonantCombination: " + nextMyanmarPhonem.hasConsonantCombination
				)

				//isOpenSyllable || 
				const currentSyllable = getFullPhonem(currentMyanmarPhonem);
				console.log("currentSyllable: " + currentSyllable)
				//console.log("NOUNS.length: " + NOUNS.length)
				const nounIndex = compoundWordTest.indexOf(currentSyllable);
				const hasNoun = nounIndex >= 0;
				console.log("hasNoun: " + hasNoun + ", nounIndex: " + nounIndex + ", endOfSyllable: " + endOfSyllable)
				//(((isOpenSyllable && hasEndOfSyllable == "") || currentMyanmarPhonemIsOnlyKinza)) 
				//const isEndOfSyllableWithoutTone =  endOfSyllableWithoutTone.includes(currentMyanmarPhonem.endOfSyllable)
				console.log("nextIsOpenSyllable: " + nextIsOpenSyllable)
				const firstSyllableTest = ((isOpenSyllable) && (hasEndOfSyllable == "" || endOfSyllable.endsWith("က်"))) || currentMyanmarPhonemIsOnlyKinza || nextIsNasalized
				const secondSyllableTest = (nextIsNoun) || nextIsOpenSyllable || nextDoubleConsonant;//nextHasGlides || nextIsNasalized && (currentMyanmarPhonem.hasConsonantCombination != "" || consonantMultichar_hasConsonantCombination != "")
				const firstMinorTest = firstSyllableTest && secondSyllableTest
				/* https://en.wikipedia.org/wiki/Burmese_phonology#Syllable_structure
				A minor syllable has some restrictions:
				
				It contains /ə/ as its only vowel
				It must be an open syllable (no coda consonant)
				It cannot bear tone  //meaning? tone sentencemarker? ့ း
				It has only a simple (C) onset (no glide after the consonant)
				It must not be the final syllable of the word
				*/

				const secondMajorTest = (nextFinalWithoutExceptions || nextExceptions);
				console.log("firstSyllableTest: " + firstSyllableTest + ", secondSyllableTest: " + secondSyllableTest)
				console.log("firstMinorTest- isOpenSyllable: " + isOpenSyllable +
					",  currentMyanmarPhonem.endOfSyllable==က် " + (currentMyanmarPhonem.endOfSyllable == "က်") +
					", currentMyanmarPhonemIsOnlyKinza: " + currentMyanmarPhonemIsOnlyKinza +
					"+ !hasNoun: " + !hasNoun + ", nextHasGlides: " + nextHasGlides +
					", currentMyanmarPhonem.hasConsonantCombination: " + currentMyanmarPhonem.hasConsonantCombination +
					", consonantMultichar_hasConsonantCombination: " + consonantMultichar_hasConsonantCombination)
				console.log("firstMinorTest: " + firstMinorTest + ", secondMajorTest: " + secondMajorTest + ", nextIsNoun: " + nextIsNoun)
				console.log("secondMajorTest: , nextFinalWithoutExceptions: " + nextFinalWithoutExceptions +
					", nextExceptions: " + nextExceptions)

				if (firstMinorTest && nextMyanmarPhonem.consonant != "အ" && !nextMuted) //&& secondMajorTest
				/*(isOpenSyllable && nextExceptions && !hasEndOfSyllable  && !nextEndSyllableWithoutVowel) ||
					(isOpenSyllable && !hasEndOfSyllable && nextEndSyllableWithoutVowel) ||
					isOpenSyllable && currentMyanmarPhonemIsOnlyKinza && nextEndSyllableWithoutVowel) */ {

					//console.log("nextMyanmarPhonem.toneSentenceMark: " + nextMyanmarPhonem.toneSentenceMark)
					//console.log("nextMyanmarPhonem.endOfSyllable: " + nextMyanmarPhonem.endOfSyllable)
					//console.log("nextMyanmarPhonem.hasToneSentenceMarkAfterEndOfSyllable: " + nextMyanmarPhonem.hasToneSentenceMarkAfterEndOfSyllable)


					const currentConsonantAndNextVowelNoFinal = (currentMyanmarPhonem.vowel == "" || isOpenSyllable);

					console.log("nextSyllableAfterConsonant: " + nextSyllableAfterConsonant + ", nextIsOpenSyllable: " + nextIsOpenSyllable)
					const currentMinorNextFinal = currentConsonantAndNextVowelNoFinal ||
						//nextMyanmarPhonem.toneSentenceMark.includes("း") ||
						/*
							//nextMyanmarPhonem.vowel.includes("ါ") ||
							nextMyanmarPhonem.vowel.includes("း") ||
							nextMyanmarPhonem.hasConsonantCombination.includes("ွ") ||*/
						//nextMyanmarPhonem.toneSentenceMark.includes("့") ||
						nextEndOfSyllable ||
						nextMyanmarPhonem.hasToneSentenceMarkAfterEndOfSyllable.includes("း")

					console.log("currentConsonantAndNextVowelNoFinal: " + currentConsonantAndNextVowelNoFinal +
						", currentMinorNextFinal: " + currentMinorNextFinal);

					//nextIsNoun
					if (spoken && secondSyllableTest) {
						console.log("should have half vowel schwa ə")
						vowel = halfVowelCharacter(type);
					}
				}
			}
			console.groupEnd("MinorSyllableHalfVowel")
		}
		console.groupEnd("HalfVowel")
		return vowel;
	}

	function getLatinVowel(myanmarPhonemArray, currentPos, hasSimplifiedStackedCharacter = false, hasClassifier = false, nextConsonantIsStacked = false, type = TypeEnums.BGN, spoken = true) {
		console.group("getLatinVowel");
		/*2. Except when accompanied by a 
			(not) (dependent vowel character or an end-of-syllable mark), 
			a  Burmese  (consonant  character  or  a  consonant  character  combination)  should  be 
			romanized with a following vowel letter a
			*/
		/* 4.
			The  independent  vowel  characters  should  be  romanized  
			without  a  hyphen  at  the beginning of words and 
			with a hyphen at the beginning of medial and final syllables
		
			ဧဏီ (ENi)
			*/
		const previousMyanmarPhonem = myanmarPhonemArray[currentPos - 1];
		const currentMyanmarPhonem = myanmarPhonemArray[currentPos];
		const nextMyanmarPhonem = myanmarPhonemArray[currentPos + 1];
		const secondNextMyanmarPhonem = myanmarPhonemArray[currentPos + 2];//Number Position 8 contains two consonants and vowels
		consoleLogDeepCopyObject(currentMyanmarPhonem);
		consoleLogDeepCopyObject(nextMyanmarPhonem);
		consoleLogDeepCopyObject(secondNextMyanmarPhonem);
		console.log(", typeValue: " + type)
		let vowel = "";
		const hasIndependentVowel = (currentMyanmarPhonem.vowel != "" && currentMyanmarPhonem.vowelType == "Independent")
		const endOfSyllable = currentMyanmarPhonem.endOfSyllable;
		let hasFinalMark = false;
		let nextEndOfSyllable = "";
		let nextOnlyEndOfSyllable = false;
		if (nextMyanmarPhonem) {
			nextEndOfSyllable = nextMyanmarPhonem.endOfSyllable;
			nextOnlyEndOfSyllable = nextMyanmarPhonem.consonant == "" && nextMyanmarPhonem.hasConsonantMultichar == "" && nextMyanmarPhonem.vowel == "" && nextEndOfSyllable
		}


		let hasEndOfSyllable = false;

		if (endOfSyllable)
			hasEndOfSyllable = (endOfSyllable && endOfSyllable != "");
		const doubleConsonant = (endOfSyllable == "်");
		let endOfSyllableFinalException = false;
		let isEndOfSyllableException = falseEndOfSyllable.includes(endOfSyllable);

		/*
		let hasClassifier = isNextSyllableCL;
		let isCL = false;
		if (nextMyanmarPhonem) {
			let nextClassifierToTest = getFullPhonem(nextMyanmarPhonem);
			if (secondNextMyanmarPhonem)
				nextClassifierToTest += getFullPhonem(secondNextMyanmarPhonem);
	
			console.log("nextClassifierToTest: " + nextClassifierToTest)
			isCL = infrontOfClassifierword(nextClassifierToTest, type);// myanmarPhonemArray, currentPos
			hasClassifier = (isNextSyllableCL || isCL);
		}
		console.log("hasClassifier: " + hasClassifier + ", isNextSyllableCL: " + isNextSyllableCL + ", isCL: " + isCL)*/

		console.log("currentMyanmarPhonem.endOfSyllable: " + currentMyanmarPhonem.endOfSyllable)
		console.log("hasEndOfSyllable: " + hasEndOfSyllable)
		if (hasEndOfSyllable) {
			consoleLogDeepCopyObject(nextMyanmarPhonem);
			console.log("hasEndOfSyllable currentMyanmarPhonem.endOfSyllable: " + currentMyanmarPhonem.endOfSyllable)

			//console.log("type: " + type);
			//console.log( CONSONANT_END_OF_SYLLABLE[type]);
			hasEndOfSyllable = CONSONANT_END_OF_SYLLABLE[type][currentMyanmarPhonem.endOfSyllable]; //default endOfSyllable

			//change EndOfSyllable to "schwa" for IsOne||IsTwo && hasCL
			if (spoken && hasClassifier) {
				const tempSyllable = currentMyanmarPhonem.consonant + currentMyanmarPhonem.hasConsonantCombination + currentMyanmarPhonem.vowel + currentMyanmarPhonem.endOfSyllable;
				const hasNumberException = END_OF_SYLLABLE_NUMBER_EXCEPTIONS[currentMyanmarPhonem.endOfSyllable] ? halfVowelCharacter(type) : null;
				const isOne = tempSyllable == "တစ်" || tempSyllable == "ဒစ်"; //second test  consonant changed to voiced consonant after vowel
				const isTwo = tempSyllable == "နှစ်";
				console.log("tempSyllable: " + tempSyllable +
					", isOne: " + isOne +
					", isTwo: " + isTwo +
					", hasClassifier: " + hasClassifier)
				//", isCL: " + isCL)
				if ((isOne || isTwo)) //hasNumberException &&
					hasEndOfSyllable = halfVowelCharacter(type)//hasNumberException
			}
			console.log("hasEndOfSyllable: " + hasEndOfSyllable);
			console.log("currentMyanmarPhonem.endOfSyllable: " + currentMyanmarPhonem.endOfSyllable)
		}
		console.log("hasEndOfSyllable: " + hasEndOfSyllable)
		const finalMark = "်";

		if (hasEndOfSyllable)
			hasFinalMark = endOfSyllable.indexOf(finalMark); //what is performanter indexOf or includes?
		console.log("hasFinalMark: " + hasFinalMark)
		console.log("hasEndOfSyllable: " + hasEndOfSyllable + ', endOfSyllable=="်": ' + doubleConsonant);


		/* examples
		စကြဝဠာ  spoken(စက်ကြဝဠာ) (/sɛʔʨawəla/ )[universe]
		သောကြာနေ့  spoken (သောက် + ကြာနေ့) (ThaukKyaNe.)[Friday]
		*/
		/*
			let hasSimplifiedStackedCharacter = false;
		const simplifiedStackedCheck = (currentMyanmarPhonem.endOfSyllable == "") &&
			((currentMyanmarPhonem.consonant != "" && currentMyanmarPhonem.consonant != "အ") ||
				currentMyanmarPhonem.hasConsonantMultichar != "");//&& currentMyanmarPhonem.vowel!="";
		const simplifiedDoubleConsonant = nextMyanmarPhonem &&
			(((nextMyanmarPhonem.consonant == "") && (nextMyanmarPhonem.hasConsonantMultichar == "")) &&
				(nextMyanmarPhonem.endOfSyllable != ""))
		consoleLogDeepCopyObject(currentMyanmarPhonem)
		consoleLogDeepCopyObject(nextMyanmarPhonem)
		console.log("simplifiedStackedCheck: " + simplifiedStackedCheck);//abbreviation change
		console.log("simplifiedDoubleConsonant: " + simplifiedDoubleConsonant);
		*/
		//TODO seperate simplifiedStackedCheck + simplifiedDoubleConsonant processing
		console.group("simplifiedStackedCheck|simplifiedDoubleConsonant")
		//	if (simplifiedStackedCheck || simplifiedDoubleConsonant) {

		//next can be a "Simplified Stacked Characters" test ယောက်ျ → ယောက်ကျား
		//hasSimplifiedStackedCharacter = canBeCombinedWithNextEndOfSyllable(myanmarPhonemArray, currentPos, type);

		console.log("hasSimplifiedStackedCharacter: " + hasSimplifiedStackedCharacter);
		console.log("after hasSimplifiedStackedCharacter currentMyanmarPhonem.endOfSyllable: " + currentMyanmarPhonem.endOfSyllable)
		if (hasSimplifiedStackedCharacter)
			hasEndOfSyllable = CONSONANT_END_OF_SYLLABLE[type][currentMyanmarPhonem.endOfSyllable];
		console.log("new EndOfSyllable: " + hasEndOfSyllable)
		//	}
		console.groupEnd("simplifiedStackedCheck|simplifiedDoubleConsonant")

		const noConsonant = currentMyanmarPhonem.consonant == "" && currentMyanmarPhonem.hasConsonantMultichar == "" && !hasIndependentVowel;
		const doubleFinal = previousMyanmarPhonem && previousMyanmarPhonem.endOfSyllable != "" && currentMyanmarPhonem.endOfSyllable != "" && currentMyanmarPhonem.endOfSyllable.length == 2;
		const isDoubleConsonant = !hasSimplifiedStackedCharacter &&
			noConsonant &&
			(endOfSyllable || endOfSyllable == "်")
		console.log("hasFinalMark: " + hasFinalMark + ", isDoubleConsonant: " + isDoubleConsonant + ", nextConsonantIsStacked:" + nextConsonantIsStacked + ", doubleFinal: " + doubleFinal)
		console.log("hasIndependentVowel: " + hasIndependentVowel + ", nextOnlyEndOfSyllable: " + nextOnlyEndOfSyllable)

		//doubleFinal||
		console.group("doubleConsonant processing")
		if (hasFinalMark >= 0) {
			if (isDoubleConsonant || (hasIndependentVowel) || nextConsonantIsStacked && hasIndependentVowel) {
				//သမီး → သ္မီး  ?
				//exception ဂတ်စ်အိုး (GatS-O) //ဝက်ဝံ (WetWun)
				//ဥက္ကဌ ဘတ်စ်ကား

				/* EndOfSyllable exceptions: "ဝံ":	"wun","ံ":	"an","ိံ":	"ein","ုံ":	"ôn" */
				//let characterToTest = endOfSyllable;
				const testLegalMyanmarConsonant = endOfSyllable.slice(0, endOfSyllable.length - 1);
				console.log("endOfSyllable: " + endOfSyllable)
				console.log("testLegalMyanmarConsonant: " + testLegalMyanmarConsonant + ", LegalLatinConsonant: " + CONSONANT[type][testLegalMyanmarConsonant]);


				consoleLogDeepCopyObject(currentMyanmarPhonem);


				//const validEndOfSyllable =CONSONANT_END_OF_SYLLABLE[type][currentMyanmarPhonem.endOfSyllable][currentMyanmarPhonem.vowel + endOfSyllable];

				let hasCompoundEndOfSyllable;
				const noVowelInfrontOfAddedEndOfSyllable = hasIndependentVowel || (currentMyanmarPhonem.vowel == "")
				if (!noVowelInfrontOfAddedEndOfSyllable) {
					hasCompoundEndOfSyllable = CONSONANT_END_OF_SYLLABLE[type][currentMyanmarPhonem.vowel + endOfSyllable];
				}

				console.log("noVowelInfrontOfAddedEndOfSyllable: " + noVowelInfrontOfAddedEndOfSyllable)
				console.log("hasEndOfSyllable: " + hasEndOfSyllable + ", hasCompoundEndOfSyllable: " + hasCompoundEndOfSyllable)

				if ((!hasIndependentVowel || (nextConsonantIsStacked && hasIndependentVowel)) && CONSONANT[type][testLegalMyanmarConsonant]) {
					// ယောက်ျ   က်ျ 
					endOfSyllableFinalException = true;
					hasEndOfSyllable = CONSONANT[type][testLegalMyanmarConsonant];
					console.log("new EndOfSyllable endOfSyllableFinalException: " + hasEndOfSyllable)
				}
				console.log("new EndOfSyllable after doubleConsonant processing: " + hasEndOfSyllable)
			}
			/*else { //processed in preprocessing
				if (!simplifiedStackedCheck && !simplifiedDoubleConsonant) {
					//abbreviation 
					//currentMyanmarPhonem.endOfSyllable = "";
				}
			}*/
		}

		console.groupEnd("doubleConsonant processing")
		//spoken = false; // needs to be adjusted //ဟိုတယ်မှ (Ho-teh-hma)  



		if (currentMyanmarPhonem.vowel != "" && currentMyanmarPhonem.vowelType == "Dependent") {
			vowel = VOWEL_CHARACTERS[type][currentMyanmarPhonem.vowelType][currentMyanmarPhonem.vowel];

		}
		else {
			consoleLogDeepCopyObject(currentMyanmarPhonem)
			//console.log(myanmarPhonem.endOfSyllable)
			//console.log(myanmarPhonem.endOfSyllable=="")
			//endOfSyllable property does not exists or is empty
			console.log((!currentMyanmarPhonem.endOfSyllable + ', currentMyanmarPhonem.endOfSyllable==""' + currentMyanmarPhonem.endOfSyllable == "") + ", if case: " + (!currentMyanmarPhonem.endOfSyllable || currentMyanmarPhonem.endOfSyllable == ""))


			const endSyllableCheck = (hasEndOfSyllable != "" && isEndOfSyllableException)
			console.log("endSyllableCheck: " + endSyllableCheck +
				", hasEndOfSyllable: " + hasEndOfSyllable +
				", isEndOfSyllableException: " + isEndOfSyllableException)
			console.group("standard syllable a")
			if (
				((currentMyanmarPhonem.vowel == "" || currentMyanmarPhonem.vowelType == "Independent") &&
					(endSyllableCheck || hasEndOfSyllable == "")))	//လွှ   
			{
				const currentConsonantNotEmptyOrA = currentMyanmarPhonem.consonant != "" && currentMyanmarPhonem.consonant != "အ";
				const currentHasConsonant = (currentConsonantNotEmptyOrA || currentMyanmarPhonem.hasConsonantMultichar != "");
				console.log("currentHasConsonant: " + currentHasConsonant
					+ ", endOfSyllableFinalException: " + endOfSyllableFinalException +
					", endOfSyllable != '်': " + (endOfSyllable != "်"))
				console.log("endOfSyllable: " + endOfSyllable +
					"currentMyanmarPhonem.endOfSyllable: " + currentMyanmarPhonem.endOfSyllable)
				if (currentHasConsonant
					&& !endOfSyllableFinalException && endOfSyllable != "်") {//currentMyanmarPhonem.endOfSyllable
					console.log("type: " + type)
					console.log("TypeEnums.IPA: " + TypeEnums.IPA)
					/*
					switch (type) { //why doesn't this work?
						case TypeEnums.Okell:
							vowel = "á"; //OKELL
							break;
						case TypeEnums.IPA:
							vowel = "a̰";
							break;
						case TypeEnums.BGN:
							vowel = "ă";
							break;
						default:
							vowel = "a";
					}*/

					if (type == TypeEnums.Okell)
						vowel = "á"; //OKELL
					else if (type == TypeEnums.BGN)
						vowel = "a"; //PCGN
					else if (type == TypeEnums.IPA)
						vowel = "a̰";
					else {
						vowel = "a";
					}


				}

			}
			console.groupEnd("standard syllable a")
		}



		console.log("vowel step: " + vowel);
		let hasToneSentenceMarkAfterVowel = "";
		if (currentMyanmarPhonem.toneSentenceMark && currentMyanmarPhonem.toneSentenceMark.length > 0)
			for (let t = 0; t < currentMyanmarPhonem.toneSentenceMark.length; t++) {
				if (TONE_SENTENCE_MARK[type][currentMyanmarPhonem.toneSentenceMark[t]])
					hasToneSentenceMarkAfterVowel += TONE_SENTENCE_MARK[type][currentMyanmarPhonem.toneSentenceMark[t]];
			}
		//const hasToneSentenceMarkAfterVowel = TONE_SENTENCE_MARK[currentMyanmarPhonem.toneSentenceMark]
		if (hasToneSentenceMarkAfterVowel)
			vowel += hasToneSentenceMarkAfterVowel;

		if (hasEndOfSyllable) {
			const isPyitException = currentMyanmarPhonem.endOfSyllable == "စ်" && currentMyanmarPhonem.consonant == "ပ" && currentMyanmarPhonem.vowel == "" && currentMyanmarPhonem.hasConsonantCombination == ""
			if (spoken && isPyitException) {
				//currentMyanmarPhonem.hasConsonantCombination="ျ"
				const latinMedial = insertConsonantMedial("", "ျ", "", type)
				vowel += latinMedial;//CONSONANT_CHARACTER_COMBINATIONS[type]["ျ"];
			}
			vowel += hasEndOfSyllable;
		}


		//TODO alternative process toneSentenceMark into vowel
		let hasToneSentenceMarkAfterEndOfSyllable = "";
		if (currentMyanmarPhonem.hasToneSentenceMarkAfterEndOfSyllable && currentMyanmarPhonem.hasToneSentenceMarkAfterEndOfSyllable.length > 0) {
			//console.log("currentMyanmarPhonem.hasToneSentenceMarkAfterEndOfSyllable: " + currentMyanmarPhonem.hasToneSentenceMarkAfterEndOfSyllable)
			for (let t = 0; t < currentMyanmarPhonem.hasToneSentenceMarkAfterEndOfSyllable.length; t++) {
				//console.log(currentMyanmarPhonem.hasToneSentenceMarkAfterEndOfSyllable[t]);
				if (TONE_SENTENCE_MARK[type][currentMyanmarPhonem.hasToneSentenceMarkAfterEndOfSyllable[t]])
					hasToneSentenceMarkAfterEndOfSyllable += TONE_SENTENCE_MARK[type][currentMyanmarPhonem.hasToneSentenceMarkAfterEndOfSyllable[t]];
			}
		}

		//const hasToneSentenceMarkAfterEndOfSyllable = TONE_SENTENCE_MARK[currentMyanmarPhonem.hasToneSentenceMarkAfterEndOfSyllable];
		if (hasToneSentenceMarkAfterEndOfSyllable)
			vowel += hasToneSentenceMarkAfterEndOfSyllable;

		console.log("vowel: " + vowel);


		console.groupEnd("getLatinVowel");
		return vowel;
	}
	function consoleLogDeepCopyObject(object) {
		if (object)//not undefined
			console.log(JSON.parse(JSON.stringify(object)));
	}


	function phonemHasVoicedStopConsonant(myanmarPhonemObject) {
		let hasVoicedStopConsonant = "";
		console.group("stopConsonant check")
		if (myanmarPhonemObject) {
			const hasCurrentStopConsonant = groupedStopConsonants[myanmarPhonemObject.consonant];
			if (hasCurrentStopConsonant) {
				const hasVoicedConsonant = groupedStopConsonants[myanmarPhonemObject.consonant].voiced;
				console.log("myanmarPhonemObject.consonant: " + myanmarPhonemObject.consonant)
				console.log("hasVoicedConsonant: " + hasVoicedConsonant)
				if (hasVoicedConsonant)
					hasVoicedStopConsonant = hasVoicedConsonant
				//ConsonantAfterVowel = CONSONANT[type][hasVoicedConsonant];
				//console.log("ConsonantAfterVowel: " + ConsonantAfterVowel)
			}
			else {
				const hasVoicedConsonant = groupedStopConsonantsValue.find(item => {
					return (item.voiced == myanmarPhonemObject.consonant)
				})
				if (hasVoicedConsonant)
					hasVoicedStopConsonant = myanmarPhonemObject.consonant;
				//	console.log(myanmarPhonemObject.consonant + " is voiced consonant")
			}
		}

		console.groupEnd("stopConsonantCheck")
		return hasVoicedStopConsonant;
	}
	function checkIsCLAtNextIndex(syllableArray, index, type) {
		const nextStartsWith_NUMBER_POSITION_ROMANIZATION_VALUES = checkIsLongestArrayElement(syllableArray, index, NUMBER_POSITION_ROMANIZATION_VALUES, type, true);
		const nextStartsWith_CLASSIFIERWORDS = checkIsLongestArrayElement(syllableArray, index, CLASSIFIERWORDS, type, true);
		const isCL = nextStartsWith_NUMBER_POSITION_ROMANIZATION_VALUES.result || nextStartsWith_CLASSIFIERWORDS.result
		return isCL;
	}
	function romanizeSyllableRegex(syllableArray, unchangedSyllables, isNextSyllableCL = false, stackedIndexes = [], type = TypeEnums.BGN, spoken = true, sandhi = true, hasPreviousNumberText = "") {
		const DEBUG = true;

		let romanizedArray = [];
		DEBUG && console.group("romanizeSyllableRegex: " + syllableArray + ', isSpoken: ' + spoken + ", hasPreviousNumberText: " + hasPreviousNumberText);
		(DEBUG || DEBUGNUMERAL) && console.log(syllableArray);
		const myanmarPhonemArray = createPhonemObjectArray(syllableArray, type);
		const unchangedSyllableArray = createPhonemObjectArray(unchangedSyllables, type);
		//const romanVowel = ["a", "e", "i", "o", "u", "è", "è", "ô"];
		//let characterPos = 0;
		console.log("myanmarPhonemArray to process");
		consoleLogDeepCopyObject(myanmarPhonemArray);
		consoleLogDeepCopyObject(unchangedSyllableArray);
		//myanmarRuleAdjustment(myanmarPhonemArray,  isNextSyllableCL , stackedIndexes , type, spoken );
		let processedCharacters = 0; //to check for match stackedIndexes position
		let processedStackedCharacter = 0;
		let previousHalfVowel = "";
		let p = 0;
		//for (let p = 0; p < myanmarPhonemArray.length; p++) {
		while (p < myanmarPhonemArray.length) {
			let hasInsertedStackedCharacter = false;
			if (spoken) {
				hasInsertedStackedCharacter = insertSpecialStackedCharacter(myanmarPhonemArray, p, unchangedSyllableArray, p + processedStackedCharacter, type);
				console.log("hasInsertedStackedCharacter: " + hasInsertedStackedCharacter)

			}

			console.groupCollapsed("romanizeSyllableRegex process syllable position " + p)
			//နက္ခတ္တဗေဒ /nəkʰətá.bedá/
			const currentCharacterIndexAtSyllable = processedCharacters; //currentSyllable.length + without -> current consonant

			let unadjustdedIndexPosition = p + processedStackedCharacter;
			const currentConsonantIsStacked = stackedIndexes.length > 0 && stackedIndexes.includes(currentCharacterIndexAtSyllable);



			const currentStackedOrPreviousKinza = (currentConsonantIsStacked || previousEndOfSyllableIsKinza);
			let fullSyllable = getFullPhonem(myanmarPhonemArray[p]);
			let nextSyllable = "";

			if (currentConsonantIsStacked) {
				console.log("currentConsonantIsStacked at index: " + currentConsonantIsStacked + " add stackedCharacterSymbol into processedCharacters count to match prefilled stackedIndexes array")
				processedCharacters++;
			}

			console.log("processedCharacters: " + processedCharacters + ", characterIndexAfterSyllable: " + characterIndexAfterSyllable)

			console.log("previousHalfVowel: " + previousHalfVowel)
			consoleLogDeepCopyObject(myanmarPhonemArray[p]);
			console.group("check previous roman character, n/ng and simmilar combination")
			let previousRomanChar = getPreviousRomanCharactersRegex(romanizedArray, type);
			if (hasPreviousNumberText.length > 0) {
				console.log("hasPreviousNumberText: " + hasPreviousNumberText)
				previousRomanChar = getPreviousRomanCharactersRegex(romanizedArray, type, hasPreviousNumberText);
			}
			let latinSyllable = "";
			console.log("previousRomanChar: " + previousRomanChar)
			//hasPreviousNorNG(romanizedArray); //Note 6
			const previousNorNG = hasPreviousChar(previousRomanChar, ["n", "ng", "ɴ"], type);  //"ɴ" ipa
			const previousT = hasPreviousChar(previousRomanChar, ["t"], type);
			const previousK = hasPreviousChar(previousRomanChar, ["k"], type);
			const previousA = hasPreviousChar(previousRomanChar, ["a"], type); //for MLCTS အာကျန် a-kyan
			const hasPreviousRomanVowel = hasPreviousRomanVowelCharacterRegEx(previousRomanChar, type);
			const hasPreviousLatinVowel = hasPreviousRomanVowel || previousNorNG;
			console.groupEnd("check previous roman character, n/ng and simmilar combination")
			console.log("hasPreviousRomanVowel: " + hasPreviousRomanVowel);
			//rebuild getPreviousRomanCharacter with regex version
			const previousMyanmarPhonemObject = myanmarPhonemArray[p - 1];
			const currentMyanmarPhonemObject = myanmarPhonemArray[p];
			const nextMyanmarPhonemObject = myanmarPhonemArray[p + 1];
			const secondNextMyanmarPhonem = myanmarPhonemArray[p + 2];//Number Position 8 contains two consonants and vowels

			const previousEndOfSyllableIsKinza = (previousMyanmarPhonemObject && previousMyanmarPhonemObject.endOfSyllable == "င်");
			const currentEndOfSyllableIsKinza = (currentMyanmarPhonemObject && currentMyanmarPhonemObject.endOfSyllable == "င်") && currentMyanmarPhonemObject.hasToneSentenceMarkAfterEndOfSyllable == "";


			console.log("currentConsonantIsStacked: " + currentConsonantIsStacked + ", currentCharacterIndexAtSyllable: " + currentCharacterIndexAtSyllable +
				", processedStackedCharacter of word: " + processedStackedCharacter + ", index p: " + p + ", unadjusted indexPosition: " + unadjustdedIndexPosition)
			console.log("activeSyllable to process: ")
			consoleLogDeepCopyObject(unchangedSyllableArray[unadjustdedIndexPosition]);
			console.log("stackedIndexes: " + stackedIndexes)
			consoleLogDeepCopyObject(currentMyanmarPhonemObject)

			console.group("currentDoubleConsonant")
			let currentDoubleConsonant = false;
			consoleLogDeepCopyObject(currentMyanmarPhonemObject)
			if (currentMyanmarPhonemObject) {
				if (currentMyanmarPhonemObject.consonant != "" && currentMyanmarPhonemObject.endOfSyllable == "်" || fullSyllable == currentMyanmarPhonemObject.endOfSyllable)//လ် mute
					currentDoubleConsonant = true;
			}
			console.groupEnd("currentDoubleConsonant")
			console.log("currentDoubleConsonant: " + currentDoubleConsonant)

			//https://r12a.github.io/scripts/myanmar/#voicing
			//"voicing when they appear in non-initial syllables of a word or in particle suffixes, unless they follow a syllable with stopped tone or follow the အ"
			//stopped tone (ie. a syllable ending in a plosive consonant), တစ်ဆယ် ten is pronounced təʔsʰɛ̀ not təʔzɛ̀.
			const stoppedTone = [
				//endOfsyllable
				"က်",
				"စ်",
				"တ်",
				"ပ်",
				//consonant
				"အ",
				"ဃ", //"ဃ": "g?",
			]

			let previousHasStoppedTone = false;
			if (previousMyanmarPhonemObject) {
				const previousSyllable = getFullPhonem(previousMyanmarPhonemObject);
				stoppedTone.forEach(element => {
					if (previousSyllable.endsWith(element)) {
						previousHasStoppedTone = true;
					}
				});
			}
			let currentHasVoicedStopConsonant = ""
			if (!previousHasStoppedTone)
				currentHasVoicedStopConsonant = phonemHasVoicedStopConsonant(currentMyanmarPhonemObject)

			let currentHasStoppedTone = false;
			if (currentMyanmarPhonemObject) {
				const syllable = getFullPhonem(currentMyanmarPhonemObject);
				stoppedTone.forEach(element => {
					if (syllable.endsWith(element)) {
						currentHasStoppedTone = true;
					}
				});
			}
			console.log(nextMyanmarPhonemObject)
			let nextHasVoicedStopConsonant = "";
			if (!currentHasStoppedTone)
				nextHasVoicedStopConsonant = phonemHasVoicedStopConsonant(nextMyanmarPhonemObject)
			console.log("currentHasVoicedStopConsonant: " + currentHasVoicedStopConsonant + ", nextHasVoicedStopConsonant: " + nextHasVoicedStopConsonant)
			let phonemLatinConsonant = "";
			let phonemLatinVowel = "";
			let previousLatinCharacter = "";
			const hasPreviousLatinCharacter = romanizedArray[p - 1];

			if (hasPreviousLatinCharacter)
				previousLatinCharacter = romanizedArray[p - 1][romanizedArray[p - 1].length - 1];
			console.log(romanizedArray)
			consoleLogDeepCopyObject(currentMyanmarPhonemObject)
			//console.log(currentMyanmarPhonemObject)
			console.log("previousLatinCharacter?: " + (p - 1))
			console.log(romanizedArray[p - 1] + ", lastCharacter: " + previousLatinCharacter + ", hasPrevious LatinVowel or NorNG: " + hasPreviousLatinVowel)

			let currentIsVoicedStopConsonant;
			if (currentMyanmarPhonemObject) {
				currentIsVoicedStopConsonant = groupedStopConsonantsValue.find(item => {
					return (item.voiced == currentMyanmarPhonemObject.consonant) //|| item.nasal == nextMyanmarPhonemObject.consonant
				});
			}

			let nextIsVoicedStopConsonant;
			if (nextMyanmarPhonemObject) {
				nextIsVoicedStopConsonant = groupedStopConsonantsValue.find(item => {
					return (item.voiced == nextMyanmarPhonemObject.consonant) //|| item.nasal == nextMyanmarPhonemObject.consonant
				});
			}


			let characterIndexAfterSyllable = processedCharacters + fullSyllable.length;
			let nextConsonantIsStacked = stackedIndexes.includes(characterIndexAfterSyllable);
			console.group("check unchanged index same as adjustedSyllables")
			consoleLogDeepCopyObject(unchangedSyllableArray[p + 1])
			consoleLogDeepCopyObject(myanmarPhonemArray[p + 1])
			console.groupEnd("check unchanged index same as adjustedSyllables")
			let nextIsNounObject, nextCouldBeVerbObject, currentIsNounObject;
			let verbTenseSyllableCount = 0;
			let isWordObject;
			let nextNounSyllableLengthUnchangedArray, currentNounSyllableLengthUnchangedArray;
			if (spoken) {
				console.group("check next Noun")
				console.log("check next Noun at index of unchangedSyllableArray: " + unadjustdedIndexPosition + ", processedStackedCharacter: " + processedStackedCharacter)
				console.log(unchangedSyllableArray[unadjustdedIndexPosition])
				nextIsNounObject = checkIsLongestArrayElement(unchangedSyllableArray, unadjustdedIndexPosition, compoundWordTest, type, true); //nextIsNoun or noun/verb?
				nextNounSyllableLengthUnchangedArray = nextIsNounObject.syllableLengthUnchangedArray;
				console.groupEnd("check next Noun")
				console.log(nextIsNounObject)
				nextCouldBeVerbObject = checkIsLongestArrayElement(unchangedSyllableArray, unadjustdedIndexPosition, VERBS, type, true); //nextIsNoun or noun/verb?
				if (nextCouldBeVerbObject.syllableLengthUnchangedArray > 0) {
					nextCouldBeVerbObject
					//if Noun + verb + has verb tense form?
					//example နဲ့ would be no tense after verb but a "postpositional marker" [with] for a noun without ma negation
					//nextCouldBeVerbObject.maNegation

				}
				/*
				console.group("check current noun")
				currentIsNounObject = checkIsLongestArrayElement(unchangedSyllableArray, unadjustdedIndexPosition, compoundWordTest, type, false); //nextIsNoun or noun/verb?
				currentNounSyllableLengthUnchangedArray = currentIsNounObject.syllableLengthUnchangedArray;
				console.groupEnd("check current noun")
				*/

			}
			const nextIsNoun = nextNounSyllableLengthUnchangedArray > 0;
			const currentIsNoun = currentNounSyllableLengthUnchangedArray > 0;
			//const prevIsNoun = checkPreviousIsNoun(myanmarPhonemArray, p -1 );			
			console.log("nextIsNoun: " + nextIsNoun);
			console.log("currentIsNoun: " + currentIsNoun)
			if (currentIsNoun) {
				//	previousHasStoppedTone = false;
				//	currentHasStoppedTone = false;
				//	currentHasVoicedStopConsonant="";
				//	nextHasVoicedStopConsonant="";
			}


			/*
			const simplifiedStackedCheck = currentMyanmarPhonemObject.endOfSyllable == "" &&
				(currentMyanmarPhonemObject.consonant != "" || currentMyanmarPhonemObject.hasConsonantMultichar != "");
				*/
			let nyaChanged = false;
			let hasSimplifiedStackedCharacter = false;

			const simplifiedStackedCheck = (currentMyanmarPhonemObject.endOfSyllable == "") &&
				((currentMyanmarPhonemObject.consonant != "" && currentMyanmarPhonemObject.consonant != "အ") ||
					currentMyanmarPhonemObject.hasConsonantMultichar != "");//&& currentMyanmarPhonem.vowel!="";
			const simplifiedDoubleConsonant = nextMyanmarPhonemObject &&
				(((nextMyanmarPhonemObject.consonant == "") && (nextMyanmarPhonemObject.hasConsonantMultichar == "")) &&
					(nextMyanmarPhonemObject.endOfSyllable != ""))



			let hasClassifier = isNextSyllableCL;
			let isCL = false;
			let tempSyllable;
			if (currentMyanmarPhonemObject)
				tempSyllable = currentMyanmarPhonemObject.consonant + currentMyanmarPhonemObject.hasConsonantCombination + currentMyanmarPhonemObject.vowel + currentMyanmarPhonemObject.endOfSyllable;
			let hasNumberException;

			const isOne = tempSyllable == "တစ်";
			const isTwo = tempSyllable == "နှစ်";
			console.log("tempSyllable: " + tempSyllable +
				", hasNumberException: " + hasNumberException +
				", isOne: " + isOne +
				", isTwo: " + isTwo +
				", hasClassifier: " + hasClassifier)
			//", isCL: " + isCL)
			if (isOne || isTwo)
				hasNumberException = END_OF_SYLLABLE_NUMBER_EXCEPTIONS[currentMyanmarPhonemObject.endOfSyllable] ? halfVowelCharacter(type) : null;
			//hasEndOfSyllable = hasNumberException
			let nextIsNumber = false;
			console.group("check nextIsNumber")
			//check before RomanAfterVowel Change in getLatinConsonant
			if (nextMyanmarPhonemObject) {
				let nextStartsWith_NUMBER_ROMANIZATION_VALUES = checkIsLongestArrayElement(unchangedSyllableArray, p, NUMBER_ROMANIZATION_VALUES, type, true);
				nextIsNumber = nextStartsWith_NUMBER_ROMANIZATION_VALUES.result;
			}
			console.groupEnd("check nextIsNumber")

			let nextNounIsCL = false;
			if (nextMyanmarPhonemObject) {
				nextSyllable = getFullPhonem(nextMyanmarPhonemObject);
				console.group("check hasClassifier")
				let nextClassifierToTest = nextSyllable;
				if (secondNextMyanmarPhonem)
					nextClassifierToTest += getFullPhonem(secondNextMyanmarPhonem);
				//const endsAsNumber = checkEndsWithArrayElement(myanmarPhonemArray, p, NUMBER_ROMANIZATION_VALUES)

				console.log("nextClassifierToTest: " + nextClassifierToTest)
				//isCL = infrontOfClassifierword(nextClassifierToTest, type);// myanmarPhonemArray, currentPos(
				let CLIndexToCheck = p;
				if (nextIsNoun)
					CLIndexToCheck = p + nextNounSyllableLengthUnchangedArray;
				const unchangedArrayPosAfterNoun = CLIndexToCheck + processedStackedCharacter;

				//check classifier after noun
				/*
				let nextStartsWith_NUMBER_POSITION_ROMANIZATION_VALUES = checkIsLongestArrayElement(unchangedSyllableArray, unchangedArrayPosAfterNoun, NUMBER_POSITION_ROMANIZATION_VALUES, type, true);
				let nextStartsWith_CLASSIFIERWORDS = checkIsLongestArrayElement(unchangedSyllableArray, unchangedArrayPosAfterNoun, CLASSIFIERWORDS, type, true);
				isCL = nextStartsWith_NUMBER_POSITION_ROMANIZATION_VALUES.result || nextStartsWith_CLASSIFIERWORDS.result*/
				isCL = checkIsCLAtNextIndex(unchangedSyllableArray, unchangedArrayPosAfterNoun, type);
				console.log(unchangedSyllableArray[unchangedArrayPosAfterNoun]);
				console.log("CLIndexToCheck: " + CLIndexToCheck + ", nextNounSyllableLengthUnchangedArray: " + nextNounSyllableLengthUnchangedArray + ", arrayindex p: " + p + ", isCL after Noun: " + isCL +
					", unchangedArrayPosAfterNoun: " + unchangedArrayPosAfterNoun + ", characterIndexAfterSyllable: " + characterIndexAfterSyllable)
				if (!isCL && nextIsNoun) //noun == Classifier
				{
					/*
					nextStartsWith_NUMBER_POSITION_ROMANIZATION_VALUES = checkIsLongestArrayElement(unchangedSyllableArray, p, NUMBER_POSITION_ROMANIZATION_VALUES, type, true);
					nextStartsWith_CLASSIFIERWORDS = checkIsLongestArrayElement(unchangedSyllableArray, p, CLASSIFIERWORDS, type, true);
					isCL = nextStartsWith_NUMBER_POSITION_ROMANIZATION_VALUES.result || nextStartsWith_CLASSIFIERWORDS.result*/
					isCL = checkIsCLAtNextIndex(unchangedSyllableArray, p, type);
					if (isCL)
						nextNounIsCL = true;
				}

				hasClassifier = (isNextSyllableCL || isCL); //&& !nextIsNumber CL not part of compound noun //!TODO nextIsNoun p length + isCL
				console.groupEnd("check hasClassifier")
			}
			console.log("hasClassifier: " + hasClassifier + ", isNextSyllableCL: " + isNextSyllableCL + ", isCL: " + isCL + ", nextIsNoun: " + nextIsNoun)



			let endsAsCL = false;
			//if (previousMyanmarPhonemObject) { 
			console.group("check endsAsCL");
			//endsAsCL = checkEndsWithArrayElement(myanmarPhonemArray, p, CLASSIFIERWORDS) || checkEndsWithArrayElement(myanmarPhonemArray, p, NUMBER_POSITION_ROMANIZATION_VALUES)
			endsAsCL = checkEndsWithUnchangedArrayElement(unchangedSyllableArray, p + processedStackedCharacter, CLASSIFIERWORDS, type) || checkEndsWithUnchangedArrayElement(unchangedSyllableArray, p + processedStackedCharacter, NUMBER_POSITION_ROMANIZATION_VALUES, type)
			console.groupEnd("check endsAsCL");
			//}
			console.log("endsAsCL: " + endsAsCL);
			console.log("hasSimplifiedStackedCharacter: " + hasSimplifiedStackedCharacter)
			console.group("endsAsNumber")
			let endsAsNumber = false;
			//endsAsNumber = checkEndsWithArrayElement(myanmarPhonemArray, p, NUMBER_ROMANIZATION_VALUES)
			endsAsNumber = checkEndsWithUnchangedArrayElement(unchangedSyllableArray, p + processedStackedCharacter, NUMBER_ROMANIZATION_VALUES, type)
			console.groupEnd("endsAsNumber")
			console.log("endsAsNumber: " + endsAsNumber);

			let syllableIsWord = false;
			let currentAndNextSyllableIsWord = false;
			let endsAsWord = false;
			console.group("check adjective has previousWord")
			const endsAsAdjectiveUnchangedArray = checkEndsWithUnchangedArrayElement(unchangedSyllableArray, p + processedStackedCharacter, ADJECTIVES, type);
			console.log(endsAsAdjectiveUnchangedArray)
			const hasWordInfrontOfAdjective = checkEndsWithUnchangedArrayElement(unchangedSyllableArray, p + processedStackedCharacter - endsAsAdjectiveUnchangedArray.syllableLength, compoundWordTest, type);

			console.groupEnd("check adjective has previousWord")
			console.log(hasWordInfrontOfAdjective)
			console.group("endsAsWordUnchangedArray")
			//endsAsWord = checkEndsWithArrayElement(myanmarPhonemArray, p, compoundWordTest);
			console.log("index p: " + p + ", processed stacked characterIn word: " + processedStackedCharacter + ", unchangedIndex: " + (p + processedStackedCharacter));
			console.log(unchangedSyllableArray[p + processedStackedCharacter])
			let endsAsWordUnchangedArray;
			let currentIsAdjective = false;
			if (endsAsAdjectiveUnchangedArray.result && hasWordInfrontOfAdjective && hasWordInfrontOfAdjective.result) {
				endsAsWordUnchangedArray = { result: false, longestMatchingElement: "", syllableLength: 0 };
				currentIsAdjective = true;
			}
			else
				endsAsWordUnchangedArray = checkEndsWithUnchangedArrayElement(unchangedSyllableArray, p + processedStackedCharacter, compoundWordTest, type);
			endsAsWord = endsAsWordUnchangedArray.result;
			console.groupEnd("endsAsWordUnchangedArray")
			console.log(endsAsWordUnchangedArray)
			console.log("endsAsWord: " + endsAsWord)
			/*
			console.group("nextEndsAsWordUnchangedArray")
			const nextEndsAsWordUnchangedArray = checkEndsWithUnchangedArrayElement(unchangedSyllableArray, p + processedStackedCharacter+1, compoundWordTest, type);
			const nextEndsAsWord = nextEndsAsWordUnchangedArray.result;
			console.groupEnd("nextEndsAsWordUnchangedArray")
			*/
			console.log("fullSyllable: " + fullSyllable) //?endsAsWordUnchangedArray.longestMatchingElement
			if (compoundWordTest.includes(fullSyllable)) {
				syllableIsWord = true;
				const compoundWordToTest = fullSyllable + nextSyllable;
				console.log("compoundWordToTest: " + compoundWordToTest)
				if (compoundWordTest.includes(compoundWordToTest))
					currentAndNextSyllableIsWord = true;
			}


			if (spoken && !currentIsAdjective && (simplifiedStackedCheck || simplifiedDoubleConsonant)) {
				//next can be a "Simplified Stacked Characters" test ယောက်ျ → ယောက်ကျား
				hasSimplifiedStackedCharacter = canBeCombinedWithNextSyllableForSimplifiedStacked(myanmarPhonemArray, p, type);
			}
			if (simplifiedStackedCheck) { //ma ... bu မကြာသေးပြဘူး။
				//nyaChanged = nyaSimplifiedStackedCheck(myanmarPhonemArray, p, type);
				nyaChanged = nyaSimplifiedStackedCheck(myanmarPhonemArray, p, unchangedSyllableArray, p + processedStackedCharacter, type);
				console.log("nyaChanged: " + nyaChanged);
				console.log(currentMyanmarPhonemObject);
				//	if (nyaChanged)
				//		hasEndOfSyllable = CONSONANT_END_OF_SYLLABLE[currentMyanmarPhonem.endOfSyllable];
			}
			//const (endsAsWordUnchangedArray.syllableLength>1 && !currentAndNextSyllableIsWord)
			const nextIsCompoundWord = endsAsWord && nextIsNoun;
			const nextIsNasalized = nextCanBeNasalized(myanmarPhonemArray, p, endsAsWord, currentAndNextSyllableIsWord, nextIsNoun, processedCharacters, stackedIndexes)
			console.log("nextCanBeNasalized: " + nextIsNasalized);
			fullSyllable = getFullPhonem(myanmarPhonemArray[p]);

			if (!(fullSyllable == "လ်" && spoken)) {
				/* Colloquial Burmese p.178(p.156)
	The ending -လ် does not affect the pronunciation in any way. For example သို and သိုလ် are both pronounced as (to); ဗို and ဗိုလ် are both pronounced as (bo)
	*/
				phonemLatinConsonant = getLatinConsonant(myanmarPhonemArray, p, currentAndNextSyllableIsWord, nextIsCompoundWord, nextIsNasalized, currentHasVoicedStopConsonant, currentStackedOrPreviousKinza, nextConsonantIsStacked, previousNorNG, previousT, previousK, previousA, hasPreviousLatinVowel, previousHalfVowel, processedCharacters, stackedIndexes, nextIsNoun, type, spoken, sandhi)
				phonemLatinVowel = getLatinVowel(myanmarPhonemArray, p, hasSimplifiedStackedCharacter, hasClassifier, nextConsonantIsStacked, type, spoken);
			}

			/*
			if (currentConsonantIsStacked) {
				console.log("hasSimplifiedStackedCharacter: " + hasSimplifiedStackedCharacter)
	
				let previousUnadjustedSyllable;
				if(unadjustdedIndexPosition-1>=0){
					previousUnadjustedSyllable = unchangedSyllableArray[unadjustdedIndexPosition-1];
					console.log(previousUnadjustedSyllable)
				
				//	processedStackedCharacter++;
				}				
			}*/
			let previousUnadjustedIndexPosition = unadjustdedIndexPosition - 1;
			const currentUnchangedSyllable = unchangedSyllableArray[unadjustdedIndexPosition];
			console.log("unadjustdedIndexPosition: " + unadjustdedIndexPosition + ", previousUnadjustedIndexPosition: " + previousUnadjustedIndexPosition + ", unchangedSyllableArray.length: " + unchangedSyllableArray.length)
			let stackedDiff = hasStackedDiff(unchangedSyllableArray, unadjustdedIndexPosition, type);
			//if(unadjustdedIndexPosition+1< unchangedSyllableArray.length && getFullPhonem(unchangedSyllableArray[unadjustdedIndexPosition+1]) == "္") stackedDiff++;
			console.log("current index p: " + p + " has stackedDiff: " + stackedDiff)
			processedStackedCharacter += stackedDiff;// hasStackedDiff(unchangedSyllableArray, unadjustdedIndexPosition - 1, type);

			// nextSyllableOnlyStackedCharacter = nextSyllableText == "္";
			if (previousUnadjustedIndexPosition >= 0) { //currentUnchangedSyllable.vowel == "္" &&


				let previousUnadjustedSyllable;
				previousUnadjustedSyllable = unchangedSyllableArray[unadjustdedIndexPosition - 1];
				console.log(previousUnadjustedSyllable)
				console.log(currentUnchangedSyllable)
				const previousCompressesWithStackedConsonant = checkConsonantMergesWithPreviousSyllable(previousUnadjustedSyllable, currentUnchangedSyllable, type);

				console.log("previousCompressesWithStackedConsonant: " + previousCompressesWithStackedConsonant + ", stackedDiff: " + stackedDiff)

				if (previousCompressesWithStackedConsonant) { //true reduce stacked character / false no consonant count change
					//	processedStackedCharacter++;
				}

				//processedStackedCharacter += stackedDiff;// hasStackedDiff(unchangedSyllableArray, unadjustdedIndexPosition - 1, type);

			}
			let endOfSyllable;
			if (currentMyanmarPhonemObject)
				endOfSyllable = currentMyanmarPhonemObject.endOfSyllable;
			let hasNasalizedFinal;
			console.log("CONSONANT_END_OF_SYLLABLE_N_TYPE - endOfSyllable: " + endOfSyllable)
			if (endOfSyllable) {

				hasNasalizedFinal = CONSONANT_END_OF_SYLLABLE_N_TYPE[endOfSyllable];
			}
			const currentIsKinza = currentMyanmarPhonemObject.endOfSyllable == "င်" && currentMyanmarPhonemObject.hasToneSentenceMarkAfterEndOfSyllable == ""



			console.group("check nextConsonant stacked for halfVowel")
			//update stackedCheck in case of syllable combination change

			processedCharacters += fullSyllable.length;
			characterIndexAfterSyllable = processedCharacters;
			nextConsonantIsStacked = stackedIndexes.includes(characterIndexAfterSyllable);
			console.log("nextConsonantIsStacked: " + nextConsonantIsStacked + ", previousCharacterIndex: " + characterIndexAfterSyllable)
			console.log("stackedIndexes: " + stackedIndexes)
			console.groupEnd("check nextConsonant stacked")




			console.group("adjustVoicedStopConsonant for halfVowel")

			//currently depending on changes of getLatinVowel
			let phonemHasHalfVowel = getHalfVowel(myanmarPhonemArray, p, nextIsNoun, nextIsNasalized, type, spoken);

			console.log("sandhi: " + sandhi + ", currentConsonantIsStacked: " + currentConsonantIsStacked)
			console.group("sandhi halfVowel")
			/* example test cases
			? stacked character consonant vs lowerConsonant  
			? သ္မီး သမီး vs ပိန္နဲသီး spoken ပိန်နဲသီ
			? အင်္ကျီ
			? သင်္ဘော
			? အန္တိမဘဝ
			? သင်္ဘော
			? မင်္ဂလာပါ။
			? ခင်ဗျာ vs ပိန္နဲသီ
			TODO needs simplification
			&& nextConsonantIsStacked
			*/
			const followingIsNumberNotNoun = endsAsCL && nextIsNumber;
			console.log("currentConsonantIsStacked: " + currentConsonantIsStacked + ", currentEndOfSyllableIsKinza: " + currentEndOfSyllableIsKinza)
			console.log("nextConsonantIsStacked: " + nextConsonantIsStacked + ",  !(nextConsonantIsStacked && currentEndOfSyllableIsKinza): " + !(nextConsonantIsStacked && currentEndOfSyllableIsKinza));

			console.log("endsAsCL: " + endsAsCL + ", nextIsNumber: " + nextIsNumber + ", nextNounIsCL: " + nextNounIsCL)
			console.log("hasNumberException: " + hasNumberException + ", could be phonemHasHalfVowel: " + phonemHasHalfVowel)

			if (spoken && sandhi) {// && (phonemHasHalfVowel != "" || (hasNumberException && hasClassifier))
				//&& !endsAsCL //CL true if number infront
				// Reduction cannot occur in the final syllable of a word
				const currentUnchangedSyllableString = getFullPhonem(currentUnchangedSyllable)
				console.log(endsAsWordUnchangedArray)
				const matchLastSyllable = endsAsWordUnchangedArray.longestMatchingElement.endsWith(currentUnchangedSyllableString);
				console.log("endsAsWordUnchangedArray endsWith currentSyllable: " + matchLastSyllable + ", currentUnchangedSyllableString: " + currentUnchangedSyllableString)
				const isMultiSyllableWord = endsAsWordUnchangedArray.syllableLength > 1
				const isFinalSyllableOfMultiSyllableWord = isMultiSyllableWord && matchLastSyllable;// 0 part of a word, >1 lastpart of word || currentEndOfSyllableIsKinza
				const lastSyllableIsOnStackedCharacter = (nextConsonantIsStacked)
				const isSingleSyllableWord = (endsAsWordUnchangedArray.syllableLength <= 1)//0 no match, 1= single syllable word

				console.log("endsAsWordUnchangedArray.syllableLength: " + endsAsWordUnchangedArray.syllableLength +
					", isFinalSyllableOfMultiSyllableWord: " + isFinalSyllableOfMultiSyllableWord +
					", currentEndOfSyllableIsKinza: " + currentEndOfSyllableIsKinza + ", isSingleSyllableWord: " + isSingleSyllableWord)
				console.log("lastSyllableIsOnStackedCharacter: " + lastSyllableIsOnStackedCharacter + ", currentConsonantIsStacked: " + currentConsonantIsStacked + ", nextConsonantIsStacked: " + nextConsonantIsStacked)

				/* https://en.wikipedia.org/wiki/Burmese_phonology#Syllable_structure
				A minor syllable has some restrictions:
				
				It contains /ə/ as its only vowel
				It must be an open syllable (no coda consonant)
				It cannot bear tone
				It has only a simple (C) onset (no glide after the consonant)
				It must not be the final syllable of the word
				*/
				//const isEndOfSyllableWithoutTone =  endOfSyllableWithoutTone.includes(currentMyanmarPhonem.endOfSyllable)
				//not working ကျွန်တော် / negativ test ပိန္နဲသီး

				const currentOnlyKinza = currentUnchangedSyllable.vowel == "" && currentEndOfSyllableIsKinza;


				const currentAfterConsonant = currentUnchangedSyllableString.slice(1);
				const currentOpenSyllable = openSyllableWithoutGlideList.includes(currentAfterConsonant)
				const currentOnlyConsonantOrKinza = currentOnlyKinza || currentAfterConsonant == "" || currentOpenSyllable || currentUnchangedSyllable.endOfSyllable.endsWith("က်");
				const consonantMultichar_hasConsonantCombination = currentUnchangedSyllable.hasConsonantMultichar.slice(1)

				const singleSyllableHasSchwa = isSingleSyllableWord && (consonantMultichar_hasConsonantCombination.includes("ွ") || currentUnchangedSyllable.hasConsonantCombination.includes("ွ") || currentUnchangedSyllable.endOfSyllable.includes("ွ"))
				const singleSyllableTest = (isSingleSyllableWord && (currentOnlyConsonantOrKinza || singleSyllableHasSchwa))
				const notFinalOrSingleOpenSyllable = ((isMultiSyllableWord && !isFinalSyllableOfMultiSyllableWord) || singleSyllableTest) && !nextConsonantIsStacked // && currentConsonantIsStacked 
				const isHalfVowel = phonemHasHalfVowel != "" &&
					!nyaChanged && !hasSimplifiedStackedCharacter &&
					notFinalOrSingleOpenSyllable  //&& !currentConsonantIsStacked//not final syllable without knowing word endOfWord is on nextConsonantIsStacked
				/*
				
					(nextIsNoun && !followingIsNumberNotNoun) &&	//ဘုရား vs ခင်ဗျား  //&& (nextNounIsCL && hasClassifier)
					
					*/
				//((!currentConsonantIsStacked && !nextConsonantIsStacked) || ((isNotFinalSyllableOfWordOrIsSingleSyllable || currentConsonantIsStacked) && previousEndOfSyllableIsKinza))
				console.log("currentMyanmarPhonemObject")
				console.log(currentMyanmarPhonemObject)
				console.log("notFinalOrSingleOpenSyllable: " + notFinalOrSingleOpenSyllable +
					", nextConsonantIsStacked: " + nextConsonantIsStacked +
					", isSingleSyllableWord: " + isSingleSyllableWord +
					", singleSyllableTest: " + singleSyllableTest +
					", singleSyllableHasSchwa: " + singleSyllableHasSchwa + ", isFinalSyllableOfMultiSyllableWord: " + isFinalSyllableOfMultiSyllableWord + ", isMultiSyllableWord: " + isMultiSyllableWord)
				console.log("isHalfVowel: " + isHalfVowel + ", phonemHasHalfVowel: " + phonemHasHalfVowel)
				const tmpVowel = phonemLatinVowel; //if hasNasalizedFinal use normal vowel
				if (isHalfVowel || (hasNumberException && hasClassifier)) //
					phonemLatinVowel = halfVowelCharacter(type);
				//check current and next both  StopConsonant - both consonant voiced
				//check nextConsonant stacked? //မင်္ဂလာပါ။

				const hasStackedExpandedCharacterChange = currentConsonantIsStacked && !currentEndOfSyllableIsKinza && !currentStackedOrPreviousKinza;
				console.log("currentHasVoicedStopConsonant: " + currentHasVoicedStopConsonant + ", currentStackedOrPreviousKinza: " + currentStackedOrPreviousKinza + ", hasStackedExpandedCharacterChange: " + hasStackedExpandedCharacterChange)
				console.log("phonemHasHalfVowel true and nextIsVoicedStopConsonant: " + nextIsVoicedStopConsonant + ", nextHasVoicedStopConsonant: " + nextHasVoicedStopConsonant);

				console.group("nasalise in compoundWord")
				console.log("endsAsWord: " + endsAsWord + ", currentAndNextSyllableIsWord: " + currentAndNextSyllableIsWord + ", hasNasalizedFinal: " + hasNasalizedFinal)
				if (!hasStackedExpandedCharacterChange && !nextConsonantIsStacked && !hasNumberException) {

					if (endsAsWord && hasNasalizedFinal && nextMyanmarPhonemObject) {//&& currentAndNextSyllableIsWord ခလုတ် vs
						const nextNasalised = groupedStopConsonantsValue.find(item => {
							return (item.nasal == nextMyanmarPhonemObject.consonant)
						}
						);
						console.log(currentMyanmarPhonemObject)
						console.log(currentMyanmarPhonemObject.consonant)
						const currentNasalised = groupedStopConsonantsValue.find(item => {
							return (item.nasal == currentMyanmarPhonemObject.consonant)
						}
						);
						console.log("currentNasalised: " + currentNasalised + ", nextNasalised: " + nextNasalised + ", bothNasal: " + bothNasal + ", nextIsNasalized: " + nextIsNasalized)
						const bothNasal = currentNasalised && nextIsNasalized;
						if (!isHalfVowel && (nextIsNasalized && !currentIsKinza))//   //&& !(currentIsVoicedStopConsonant||currentHasVoicedStopConsonant)
						{
							phonemLatinVowel = tmpVowel;
							phonemHasHalfVowel = "";
						}

					} //else
					{
						//currentAndNextSyllableIsWord  && !nextIsNasalized //currentConsonantIsStacked&& !currentEndOfSyllableIsKinza
						const voicedConsonantException = "သ"

						if (isHalfVowel &&
							!(currentConsonantIsStacked && !currentEndOfSyllableIsKinza) && !hasSimplifiedStackedCharacter &&
							currentHasVoicedStopConsonant != "" && currentUnchangedSyllable.consonant != voicedConsonantException && (nextHasVoicedStopConsonant != "" || nextIsVoicedStopConsonant) && !nextIsNasalized) { //!nextIsNasalized  only change for Voiced/After Vowel change if next is/can be Voiced
							currentMyanmarPhonemObject.consonant = currentHasVoicedStopConsonant;//တက္ကသိုလ်
							consoleLogDeepCopyObject(currentMyanmarPhonemObject)
							//if(nextIsVoicedStopConsonant) //only change previous to voiced if next is voiced  // "promise": [ɡədḭ] ကတိ ← /ka̰/ + /tḭ/
							phonemLatinConsonant = CONSONANT[type][currentMyanmarPhonemObject.consonant];
							console.log("sandhi voiced consonant pair. Change current consonant to : " + phonemLatinConsonant)
							let nextConsonantAdjustment = "";
							//if (!nextIsVoicedStopConsonant && nextHasVoicedStopConsonant != "") nextConsonantAdjustment = phonemHasVoicedStopConsonant(nextMyanmarPhonemObject);
							//nextMyanmarPhonemObject = nextConsonantAdjustment;
						}
					}
				}


				console.groupEnd("nasalise in compoundWord")

				if (isHalfVowel || (hasNumberException && hasClassifier))
					previousHalfVowel = phonemHasHalfVowel;
			}
			else {
				previousHalfVowel = "";
			}
			console.groupEnd("sandhi halfVowel")


			console.groupEnd("adjustVoicedStopConsonant for halfVowel")
			console.log("fullSyllable: " + fullSyllable)
			console.log("processedCharacters after current syllable: " + processedCharacters + ", fullSyllable length: " + fullSyllable.length)
			console.log("stackedIndexes: " + stackedIndexes);
			console.log("phonemLatinConsonant: " + phonemLatinConsonant);
			console.log("phonemLatinVowel: " + phonemLatinVowel);

			latinSyllable += phonemLatinConsonant + phonemLatinVowel;
			//const hasToneSentenceMark = TONE_SENTENCE_MARK[currentMyanmarPhonemObject.toneSentenceMark];
			//console.log(currentMyanmarPhonemObject.toneSentenceMark)
			/*console.log("hasToneSentenceMark: " + hasToneSentenceMark);
			if(hasToneSentenceMark)
			{
				latinSyllable += hasToneSentenceMark;
			}*/
			let upperCase = latinSyllable;
			const currentMyanmarPhonem = myanmarPhonemArray[p]
			consoleLogDeepCopyObject(currentMyanmarPhonem)
			//const testLegalMyanmarConsonant = endOfSyllable.slice(0, endOfSyllable.length - 1);
			//ဒေါ်လာ DawLa
			/*
			if (currentMyanmarPhonem.endOfSyllable != "်" ||
				(currentMyanmarPhonem.endOfSyllable == "်" && currentMyanmarPhonem.vowel != ""))//vowel endOfSyllable
			{
				console.log("currentMyanmarPhonem: ")
				consoleLogDeepCopyObject(currentMyanmarPhonem);
				console.log("latinSyllable: " + latinSyllable);				
			}*/
			if (!currentDoubleConsonant)
				upperCase = firstCharacterToUpperCase(latinSyllable);

			romanizedArray.push(upperCase);
			console.groupEnd("romanizeSyllableRegex process syllable position " + p)
			p++;
			if (hasInsertedStackedCharacter)
				processedStackedCharacter--;
		}
		console.log(romanizedArray);

		DEBUG && console.groupEnd("romanizeSyllableRegex: " + syllableArray + ', isSpoken: ' + spoken);
		return romanizedArray.join("");
	}

	let _private = {}
	
	if(process.env.NODE_ENV == "development")
	{
		_private = {
			halfVowelCharacter:halfVowelCharacter,
			preprocessStackedCharacters:preprocessStackedCharacters,
			pronounceMyanmarNumberArray:pronounceMyanmarNumberArray,
			returnStackedCharacterMatches:returnStackedCharacterMatches,
			segment:segment,
			seperateIntoSyllablesRegex:seperateIntoSyllablesRegex,
			createPhonemObjectArray:createPhonemObjectArray,
			phonemObjectArray:phonemObjectArray,
			replaceMistakenInputCharacters:replaceMistakenInputCharacters,
			canMergeToNewEndOfSyllable:canMergeToNewEndOfSyllable,
			canBeCombinedWithNextSyllableForSimplifiedStacked:canBeCombinedWithNextSyllableForSimplifiedStacked,
			insertSpecialStackedCharacter:insertSpecialStackedCharacter,
		}
	}

	return {
		//public accessable functions how to test private functions?
		processTextBGN: processTextBGN,
		_private: _private
	 };
	 
} )

function mypron(text, type = TypeEnums.BGN, spoken = true, sandhi = true) {
	let DEBUG = false;
	//return "";//memory leak test; not the cause?
	DEBUG && console.group("mypron executing: -" + text)

	let result = "";
	//function processTextBGN(text, isNextWordCL = false, type = TypeEnums.BGN, spoken=true) {
	if (text && text.length > 0) {
		DEBUG && console.log("text.length: " + text.length)
		result = myPronApp().processTextBGN(text, false, type, spoken, sandhi);
	}

	//processTextBGN(text, isNumber = false, isNextWordCL = false, debug = false, type = TypeEnums.BGN) 
	DEBUG && console.groupEnd();
	return result;
}

export  {  mypron, myPronApp }
//exports.myPronApp  = myPronApp();
/*
export function mypron(text, type = TypeEnums.BGN, spoken = true, sandhi = true) {
	let DEBUG = false;
	//return "";//memory leak test; not the cause?
	DEBUG && console.group("mypron executing: -" + text)

	let result = "";
	//function processTextBGN(text, isNextWordCL = false, type = TypeEnums.BGN, spoken=true) {
	if (text && text.length > 0) {
		DEBUG && console.log("text.length: " + text.length)
		result = myPronApp().processTextBGN(text, false, type, spoken, sandhi);
	}

	//processTextBGN(text, isNumber = false, isNextWordCL = false, debug = false, type = TypeEnums.BGN) 
	DEBUG && console.groupEnd();
	return result;
}*/

/*
expose({ mypron(text) {
	const DEBUG = false;
	DEBUG && console.log("mypron [expose] executing: -" + text)
	let result = "";
	if(text && text.length>0)
		result = processTextBGN(text);

	return result;
}})
*/

/*
syllables [] each note  transformation then concatenating
	!first check consonant indexof() "special multi char cases" consonantCombination "ရှ": "sh", "ခြ": "ch", "ချ": "ch",
	!split consonant vowel
   !note2	consonant not followed by vowel Consonant+a
   !note3	အ -> not vocalized if followed by (CONSONANT_END_OF_SYLLABLE)
	   !vocalized when before consonant character that does not carry a vowel (note2?)
	   !when consonant set after CONSONANT_END_OF_SYLLABLE add infront "-" //second loop check with previousSyllableArray adjust romanizeArray
   !note4  independent  vowel  characters  should  be  romanized with additional "-" when following after CONSONANT_END_OF_SYLLABLE
		!ဧဏီ → eni, ကြေဧ → kye-e
   !note5 stacked character deconstruct (unicode already in correct order?)
   !customNote exchange CONSONANT with CONSONANT_AFTER_VOWEL if(following n or ng)
   !note6 when CONSONANT_END_OF_SYLLABLE
	   ! n before "g" or "y" add "-" before  "g/y" -> "-g/-y"
	   ! t before "g" add "-" before  "h" -> "-h"
   !note7	difference to pdf	add marker for tone marks "း", "့" in romanization
   !note8 င်္ vowel mark indicates  a  change  in  the  romanization  of  the  preceding  syllable from a to in

   !note9 Although of infrequent occurrence, a number of character ligatures and abbreviations are found in Burmese writing.
	   In the event that a character not shown in the tables is encountered, a reference source should be consulted.
   !note10 ည် is romanized i, in or e, depending on pronunciation.  A reference source should be consulted in case of uncertainty.
   !note11 The  Romanization  columns  show  only  lowercase  forms  but,  when  romanizing, uppercase and lowercase Roman letters as appropriate should be used
*/