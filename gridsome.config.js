// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`
const tailwindcss = require("tailwindcss")

module.exports = {
  siteName: 'Myanmar To Latin',
  //siteUrl: 'https://MyanmarToLatin.netlify.app',
  plugins: [{
    use: '@gridsome/plugin-google-analytics',
    options: {
      id: process.env.GA_TRACKING_ID,
      disabled: process.env.NODE_ENV !== 'production',
      debug: {
        sendHitTask: process.env.NODE_ENV === 'production',
      }
    }
  },
  {
    use: 'klaro-gridsome',
    options: {
      privacyPolicy: '/about#privacy',
      cookieName: 'consents_myanmartolatin',
      // How Klaro should store the user's preferences. It can be either 'cookie'
      // (the default) or 'localStorage'.
      storageMethod: 'localStorage',
      // You can also set a custom expiration time for the Klaro cookie.
      // By default, it will expire after 120 days.
      cookieExpiresAfterDays: 7,
      // replace "decline" with cookie manager modal
      hideDeclineAll: true,
      translations: {
        en: {
          consentModal: {
            description: 'Here you can see and customize the information that we collect about you.',
          },
          googleAnalytics: {
            description: 'Website analytics powered by Google Analytics, allowing us to see how visitors use our website.'
          },
          myanmarToLatinType: {
            description: 'Myanmar To Latin website settings'
          },
          purposes: {
            analytics: 'Analytics',
            sitesettings: 'Website Settings'
          },
        },
      },
      apps: [
        {
          name: 'googleAnalytics',
          default: true,
          title: 'Google Analytics',
          purposes: ['analytics'],
          cookies: [
            '_ga',
            '_gcl_au',
            '_gid',
            '_gat'
          ],
          required: false,
          optOut: true,
          /*
           If 'onlyOnce' is set to 'true', the app will only be executed once regardless
           how often the user toggles it on and off. This is relevant e.g. for tracking
           scripts that would generate new page view events every time Klaro disables and
           re-enables them due to a consent change by the user.
           */
          onlyOnce: true
        },
        {
          name: 'myanmarToLatinType',
          default: true,
          title: 'Myanmar To Latin Settings',
          purposes: ['sitesettings'],
          cookies: [
            '_myanmarToLatinSettings',
          ],
          required: false,
          optOut: true,
          onlyOnce: true
        }
      ]

    }
  }],
  css: {
    loaderOptions: {
      postcss: {
        plugins: [
          tailwindcss
        ],
      },
    },
  }
}
