# Myanmar to Latin [![Project Status: WIP – Initial development is in progress, but there has not yet been a stable, usable release suitable for the public.](https://www.repostatus.org/badges/latest/wip.svg)](https://www.repostatus.org/#wip)

## Here is the sourcecode of the site https://MyanmarToLatin.Netlify.app
On the webpage i am describing in words how i try to transliterate and read myanmar characters.

## Intro

If you just want to view the (currently chaotic) script for transliterating myanmar(Unicode) words into latin words you can look for the file my-pron.js in the subfolder assets/js on the gitlab page.

For now only transliterating without sandhi changes is properly functioning.<br />
My script is still in development and needs to be cleaned up and optimized, but should cover basic transliteration.<br />
It is seperated into these 3 levels

- Written: Which character is exactly written + stacked character expansion (without simplified stacked words)
- Simple Spoken changes.
  - Consonant change after vowel and
  - 1,2,7 Number weakening infront of Classifier or noun+Classifier (Noun dictionary usage)
  - simplified stacked word expansion
- Extended Spoken (Sandhi) (Noun+Verb dictionary usage)
  - Half vowel
  - voiced consonant pair matching after half vowel and following voiced consonant after vowel or nasal finals (n/ng)
  - Nasalized Consonant changes

## About
I'm trying to learn myanmar vocabulary and there are a lot of different transliteration styles for converting myanmar words used. For ease of learning i searched for a script to standardize conversion from myanmar to latin pronounciation for myself. The best one i found was <a href="https://github.com/myanmaropenwordnet/mya2rom">myanmaropenwordnet / mya2rom</a> but it does not support stacked characters and myanmar numbers.<br />
Since i haven't found a script that can transliterate stacked characters and sandhi changes i'm beginning this script which may be helpful for more people.<br/>
The script is still in its infancy state but has progressed enough and is beginning to be useful.<br />
So now i am making this public.<br/>

## Used resources
Since there is no need for a complex webpage with backend interactions i decided to try out a static site generator.<br />
For ease of programming the website is build with Gridsome and
TailwindCSS. The code is hosted on gitlab and served over netlify.

Website:
- <a href="https://gridsome.org">Gridsome</a>
- <a href="https://tailwindcss.com/">Tailwind CSS</a>
- <a href="https://ktquez.github.io/vue-disqus">Vue Disqus</a>
- <a href="https://gridsome.org/plugins/klaro-gridsome">Klaro gridsome Consent manager</a>
- <a href="https://analytics.google.com">Google Analytics</a>
- <a href="https://gitlab.com">Gitlab</a>
- <a href="https://www.netlify.com/">netlify</a>

Transliteration script
- <a href="https://github.com/dzcpy/transliteration">dzcpy / transliteration </a>(Non myanmar transliteration)
- <a href="https://github.com/myanmarlinguistics/myanmar-words/">myanmarlinguistics / myanmar-words</a> (Myanmar nouns and verb list)

## Features
- myanmar unicode romanization/transliteration (BGN,MLCTS,IPA, Okell) <br/>I am using BGN myself. The other transliteration types still needs to be double checked
- stacked character "decompression"/"expansion" (additional inferred rules for "Stacked Character", "Simplified Double Stacked Words" and "Double Consonants/Modern Closing Consonants") Consonant after stacked not changed to voiced Consonant except previous EndOfSyllable is Kinza
- number pronounciation up to a length of 8 digits. <br />
With weakening (1,2,7) if followed by known classifier  <a href="https://en.wikipedia.org/wiki/Burmese_numerical_classifiers">Wikipedia - Burmese_numerical_classifiers</a> or noun directly followed by classifier
- Spoken Pyit exception (Found in Myanmar Script Learning Guide PDF on  <a href="https://www.asiapearltravels.com/language/lesson33.php">Asiapearltravel - Lesson 33: Learn to read Burmese Script</a><br/>
ပစ် Exception Spoken (Pyit) not (Pit)
- additional infered rule for ပညာ (pyin2-nya2) //needs to be further checked

## current limitations, errors, questions:
- variable pronounciations. Following variations are always using the first variant
  - ည် (i|e|in)
  - ရ (ya|ra) displayed as |Y|
  - လျှ (sha|hlya) displayed as |Sh| -at least one known exception လျှပ်စစ် [electricity](hlyatsit) not (shatsit)
- needs exact characters.<br />
similar looking characters for example ၀(ThuNya)[0-Zero] / ဝ(wa) [shiftW german keyboard layout] will result in "wrong" romanization
- မဇ္ဈိမ [center, middle] (/myiʔzímá/) //Anki - different decompression needed? (need to further lookup. Idea maybe in https://en.wikipedia.org/wiki/MLC_Transcription_System) any rules for "oddballs"? နနွင်း [turmeric] (sa-nwin)//https://viss.wordpress.com/2009/01/04/10-things-i-hate-about-the-burmese-language/
- missing detailed rules for speaking sandhi.
  - still figuring out minor syllables
  - Nasal Consonant changes and weakening to half vowel inaccurate without bigger wordlist
  - Noun/Verb list မြန်မာဘာသာဗေဒ (Myanmar Linguistics) - မြန်မာစကားလုံးများ (Myanmar Words / Burmese Words) too small. Hasn't every common word. For example the compound word "အသံတိတ်" is missing in which case the halfVowel result is inaccurate? (Example "အသံတိတ်ရုပ်ရှင်")<br />
  Is there a bigger list or a list with extended common words available? Are there "rules" for valid compound words?
  - valid word/verb detection limitation:<br />
  valid word search extended for မ negation. Needed for extended negativ compound verb for example နားမလည်. Without extension next valid word classification for halfVowel not working since လည် would be seen as single syllable word<br />
  In verb dictionary only နားလည် available without marker for negativ မ seperator placement. Are there more seperator words that can fall inbetween compound words?

## TODO:

- further checking if my inferred rules are correct
- BGN needs to be double check for cross interferences of other transliteration system changes. Other transliteration systems needs to be double checked.
- simplify currently used rules
- code refactoring (clean up / redundant/unused code removal / optimisations)
  - exchange manual syllable segmentation with adjusted regex of <a href="https://github.com/myanmaropenwordnet/mya2rom/blob/master/MYA%20Syllable%20Regex.txt" >myanmaropenwordnet / mya2rom / MYA Syllable Regex.txt</a> to better fit my usage.
  - readjust stacked character processing
- extended Spoken (Sandhi): further checking sandhi (Minor Syllable Consonant/Half Vowel Changes and Nasalized Consonant changes)
  - improving compound word check (word = noun+verb?)

## maybe sometime in the future (to look into):

- improving number to word preprocessing. <br />
  The python regex examples of <a href="https://github.com/hpbyte/Myanmar_Number_to_Words">hpbyte / Myanmar_Number_to_Words</a> seems like a good starting point.
- Myanmar OCR Photo/LiveView as quick reading/pronounciation helper:<br/>
  Looks promising to include <a href="https://tesseract.projectnaptha.com/">https://tesseract.projectnaptha.com</a>

## far in the future

- Text2Speech
- Translation
